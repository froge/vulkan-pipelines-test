#pragma once

#include "flecs.h"
#include "common.h"

#ifdef PHASES_H_DECLARE
#define PHASES_EXTERN
#else
#define PHASES_EXTERN extern
#endif

PHASES_EXTERN ECS_ENTITY_DECLARE(PInput);
PHASES_EXTERN ECS_ENTITY_DECLARE(PUser);
PHASES_EXTERN ECS_ENTITY_DECLARE(PPhysics);
PHASES_EXTERN ECS_ENTITY_DECLARE(PPostPhysics);
PHASES_EXTERN ECS_ENTITY_DECLARE(POutputs);
PHASES_EXTERN ECS_ENTITY_DECLARE(PPresent);

void PhasesImport(ecs_world_t *world);
