#pragma once

#include "flecs.h"
// #include "cglm/call.h"
#include "cglm/struct.h"
#include "common.h"
#include "rapier.h"

#ifdef PHYSICS_H_DECLARE
#define PHYSICS_EXTERN
#else
#define PHYSICS_EXTERN extern
#endif

typedef vec3s Vector3;
typedef vec4s Vector4;
typedef mat4s Matrix4;

typedef versors    Quaternion;

typedef struct Transform3D {
    Vector3    pos;
    Vector3    scale;
    Quaternion rotation;
    u32 modified;
}Transform;

struct Transform3D default_transform();

typedef Transform WorldTransform;

// typedef Vector3    Position;
// typedef Vector3    Scale;
// typedef Quaternion Rotation;

// typedef Vector3    WorldPosition;
// typedef Vector3    WorldScale;
// typedef Quaternion WorldRotation;

typedef Vector3    Velocity;

typedef struct {
    u32 verts;
    Vector3 *data;
}CollisionMesh;

typedef struct {
    Vector3 origin;
    Vector3 direction;
}Ray;

typedef struct {
    ecs_entity_t action;
}Selectable;

typedef struct {
    RigidBodyHandle handle;
}PhysicsBody;

typedef struct {
    PhysicsState *state;
    PhysicsPipeline *pipeline;
    RigidBodySet *rset;
    ColliderSet *cset;
    u64 last_update;
}PhysicsSystem;

void PhysicsImport(ecs_world_t *world);

Ray ray_from_mouse_pos(ecs_world_t *world, vec2 mouse_pos);
int intersect_mesh(Ray ray, CollisionMesh *mesh, mat4s model, float *dist);
void physics_set_timescale(ecs_world_t *world, f32 scale);

PHYSICS_EXTERN ECS_COMPONENT_DECLARE(Transform);
// PHYSICS_EXTERN ECS_COMPONENT_DECLARE(Scale);
// PHYSICS_EXTERN ECS_COMPONENT_DECLARE(Rotation);
PHYSICS_EXTERN ECS_COMPONENT_DECLARE(Matrix4);

PHYSICS_EXTERN ECS_COMPONENT_DECLARE(WorldTransform);
// PHYSICS_EXTERN ECS_COMPONENT_DECLARE(WorldScale);
// PHYSICS_EXTERN ECS_COMPONENT_DECLARE(WorldRotation);

PHYSICS_EXTERN ECS_COMPONENT_DECLARE(Velocity);
PHYSICS_EXTERN ECS_COMPONENT_DECLARE(CollisionMesh);
PHYSICS_EXTERN ECS_COMPONENT_DECLARE(Selectable);

PHYSICS_EXTERN ECS_COMPONENT_DECLARE(PhysicsSystem);
PHYSICS_EXTERN ECS_COMPONENT_DECLARE(PhysicsBody);

PHYSICS_EXTERN ECS_TAG_DECLARE(Static);
PHYSICS_EXTERN ECS_TAG_DECLARE(Dynamic);
PHYSICS_EXTERN ECS_TAG_DECLARE(Kinematic);
