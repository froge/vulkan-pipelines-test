#version 460

layout (location = 0) out vec2 tex_coord_out;

vec2 fart[] = {
    vec2( 1.0,  1.0),
    vec2(-1.0,  1.0),
    vec2(-1.0, -1.0),
    vec2( 1.0,  1.0),
    vec2(-1.0, -1.0),
    vec2( 1.0, -1.0),
};

vec2 tex[] = {
    vec2(1.0, 0.0),
    vec2(0.0, 0.0),
    vec2(0.0, 1.0),
    vec2(1.0, 0.0),
    vec2(0.0, 1.0),
    vec2(1.0, 1.0),
};

void main() {
    gl_Position = vec4(fart[gl_VertexIndex], 0.0, 1.0);
    tex_coord_out = tex[gl_VertexIndex];
}
