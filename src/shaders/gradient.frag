#version 460

layout (location = 0) out vec4 color;
layout (location = 1) out vec4 normal;
layout (location = 2) out vec4 pos;
layout (location = 3) out uvec2 mat;

layout (location = 0) in vec2 tex_coord;

void main () {
    gl_FragDepth = 1.0;
    // color.rgb = mix(vec3(0.0, 0.0, 0.0), vec3(1.0, 1.0, 1.0), 1.0-tex_coord.y);
    // color.rgb = pow(vec3(0.57, 0.72, 1.0), vec3(2.2));
    color.rg = tex_coord;
    color.a = 1.0;
    normal = vec4(0.0);
    pos = vec4(0.0);
    mat.r = 2;
    mat.g = 0;
}
