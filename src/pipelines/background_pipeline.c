#include <vulkan/vulkan.h>
#include <flecs.h>

VkPipeline create_background_pipeline(ecs_world_t *world, VkPipelineLayout pipeline_layout, VkRenderPass render_pass) {
    ecs_entity_t s3dv_e = load_shader(world, "shaders/fullscreen.vert.spv", VK_SHADER_STAGE_VERTEX_BIT);
    ecs_entity_t s3df_e = load_shader(world, "shaders/gradient.frag.spv", VK_SHADER_STAGE_FRAGMENT_BIT);

    const Shader *s3dv = ecs_get(world, s3dv_e, Shader);
    const Shader *s3df = ecs_get(world, s3df_e, Shader);
    
    VkPipelineShaderStageCreateInfo shader_info0 = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = s3dv->stage,
        .module = s3dv->mod,
        .pName = "main",
    };

    VkPipelineShaderStageCreateInfo shader_info1 = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .stage = s3df->stage,
        .module = s3df->mod,
        .pName = "main",
    };


    VkPipelineVertexInputStateCreateInfo vinput_info = {
        VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = 0,
        .pVertexBindingDescriptions = &(VkVertexInputBindingDescription){0, 16, VK_VERTEX_INPUT_RATE_VERTEX},
        .vertexAttributeDescriptionCount = 0,
        .pVertexAttributeDescriptions = (VkVertexInputAttributeDescription[]) {
            { 0, 0, VK_FORMAT_R32G32_SFLOAT, 0 },
            { 1, 0, VK_FORMAT_R32G32_SFLOAT, 8 },
        },
    };

    VkPipelineInputAssemblyStateCreateInfo inputassembly = {
        VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        .primitiveRestartEnable = VK_FALSE,
    };

    VkPipelineViewportStateCreateInfo viewport_state = {
        VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .viewportCount = 1,
        .scissorCount = 1,
    };

    VkPipelineRasterizationStateCreateInfo raster_state = {
        VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .cullMode = VK_CULL_MODE_NONE,
        .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
        .lineWidth = 1.0,
    };

    VkPipelineMultisampleStateCreateInfo multisample_state = {
        VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
        .sampleShadingEnable = VK_FALSE,
        .minSampleShading = 1.0,
        .pSampleMask = &(VkSampleMask){0x1},
        .alphaToCoverageEnable = VK_FALSE,
        .alphaToOneEnable = VK_FALSE,
    };

    VkStencilOpState stencil_state = {
        .failOp = VK_STENCIL_OP_KEEP,
        .passOp = VK_STENCIL_OP_REPLACE,
        .depthFailOp = VK_STENCIL_OP_KEEP,
        .compareOp = VK_COMPARE_OP_EQUAL,
        .compareMask = 0xff,
        .writeMask = 0xff,
        .reference = 1,
    };

    VkPipelineDepthStencilStateCreateInfo ds_state = {
        VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = VK_FALSE,
        .depthWriteEnable = VK_FALSE,
        .depthCompareOp = VK_COMPARE_OP_GREATER,
        .depthBoundsTestEnable = VK_FALSE,
        .stencilTestEnable = VK_FALSE,
        .front = {},
        .back = {},
        .minDepthBounds = 0,
        .maxDepthBounds = 1.0,
    };

    VkPipelineColorBlendStateCreateInfo blend_state = {
        VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .logicOpEnable = VK_FALSE,
        .logicOp = VK_LOGIC_OP_AND,
        .attachmentCount = 4,
        .pAttachments = (VkPipelineColorBlendAttachmentState[]){
            {
                .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
                .blendEnable = VK_FALSE,
            },
            {
                .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
                .blendEnable = VK_FALSE,
            },
            {
                .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
                .blendEnable = VK_FALSE,
            },
            {
                .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
                .blendEnable = VK_FALSE,
            },
        },
    };

    VkPipelineDynamicStateCreateInfo dyn_state = {
        VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .dynamicStateCount = 2,
        .pDynamicStates = (VkDynamicState[]) { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR },
    };

    const Swapchain *swapchain = ecs_singleton_get(world, Swapchain);
    
    // VkPipelineRenderingCreateInfoKHR pipeline_rendering_create_info = {
    //     VK_STRUCTURE_TYPE_PIPELINE_RENDERING_CREATE_INFO,
    //     .colorAttachmentCount = 1,
    //     .pColorAttachmentFormats = &swapchain->info.imageFormat,
    //     .depthAttachmentFormat = VK_FORMAT_D32_SFLOAT_S8_UINT,
    //     .stencilAttachmentFormat = VK_FORMAT_D32_SFLOAT_S8_UINT,
    // };

    VkGraphicsPipelineCreateInfo pipeline_info = {0};
    pipeline_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipeline_info.pNext = NULL;
    pipeline_info.stageCount = 2;
    pipeline_info.pStages = (VkPipelineShaderStageCreateInfo[]){shader_info0, shader_info1};
    pipeline_info.pVertexInputState = &vinput_info;
    pipeline_info.pInputAssemblyState = &inputassembly;
    pipeline_info.pTessellationState = NULL;
    pipeline_info.pViewportState = &viewport_state;
    pipeline_info.pRasterizationState = &raster_state;
    pipeline_info.pMultisampleState = &multisample_state;
    pipeline_info.pDepthStencilState = &ds_state;
    pipeline_info.pColorBlendState = &blend_state;
    pipeline_info.pDynamicState = &dyn_state;
    pipeline_info.layout = pipeline_layout;
    pipeline_info.renderPass = render_pass;
    pipeline_info.subpass = 0;
    pipeline_info.basePipelineHandle = VK_NULL_HANDLE;
    // pipeline_info.pNext = &pipeline_rendering_create_info;
    
    ///


    VkDevice dev = *ecs_singleton_get_mut(world, VkDevice);

    VkPipeline pipeline;
    VkResult res = vkCreateGraphicsPipelines(dev, VK_NULL_HANDLE, 1, &pipeline_info, NULL, &pipeline);
    if (res != VK_SUCCESS) {
        printf("pipeline creation failed with error code %i\n", res);
        exit(1);
    }

    ecs_singleton_modified(world, VkDevice);

    return pipeline;
}

