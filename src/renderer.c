#include "cglm/box.h"
#include "cglm/struct/mat4.h"
#include "cglm/struct/quat.h"
#include "common.h"
#include <vulkan/vulkan.h>
#include "vk_mem_alloc.h"
#include "convars.h"
#include "vulkan.h"
#include "window.h"

#define RENDERER_H_DECLARE
#include "renderer.h"

#include "phases.h"
#include "physics.h"

void matrix_from_transform(ecs_iter_t *it) {
    const Transform *transforms = ecs_field(it, Transform,  1);
    Matrix4 *matrices = ecs_field(it, Matrix4, 2);
    const WorldTransform *wtransforms = ecs_field(it, WorldTransform, 3);

    ecs_world_t *world = it->world;

    for (int i = 0; i < it->count; i++) {
        ecs_entity_t entity = it->entities[i];

        if (transforms) {
            Transform transform = transforms[i];
            // if (transform.modified || (wtransforms && wtransforms[i].modified)) {
            // } else {
            //     continue;
            // }
            if (wtransforms) {
                const Transform *wtransform = &wtransforms[i];
                transform.pos = glms_vec3_mul(transform.pos, wtransform->scale);
                transform.pos = glms_vec3_add(transform.pos, wtransform->pos);
                transform.rotation = glms_quat_mul(transform.rotation, wtransform->rotation);
                transform.scale = glms_vec3_mul(transform.scale, wtransform->scale);
            }
            ecs_set_ptr(world, entity, WorldTransform, &transform);
            if (matrices) {
                Matrix4 *matrix;
                matrix = &matrices[i];
                *matrix = glms_mat4_identity();
                *matrix = glms_translate(*matrix, transform.pos);
                *matrix = glms_quat_rotate(*matrix, transform.rotation);
                *matrix = glms_scale(*matrix, transform.scale);
            }
        }
    }
}

void check_visible(ecs_iter_t *it) {
    ecs_world_t *world = it->world;
    Camera *cam = ecs_singleton_get(world, Camera);
    Render3D *renders = ecs_field(it, Render3D, 1);
    Transform *mats = ecs_field(it, WorldTransform, 2);
    AABB *aabbs = ecs_field(it, AABB, 3);
    mat4s proj;
    glm_mat4_identity(proj.raw);
    const Window *window = ecs_singleton_get(world, Window);
    const Framebuffer *buf = ecs_get(world, window->framebuffer, Framebuffer);
    glm_perspective(glm_rad(90.0), (float)buf->x / (float)buf->y, 0.01, 100.0, proj.raw);
    mat4s view; 
    cam_view(world, &view.raw);
    mat4s view_proj = glms_mat4_mul(proj, view);
    vec4 frustum[6];
    glm_frustum_planes(view_proj.raw, frustum);

    for (int i = 0; i < it->count; i++) {
        Render3D *render = &renders[i];
        Transform *mat = &mats[i];
        AABB *aabb = &aabbs[i];
        AABB _aabb;
        glm_vec3_add(aabb->min.raw, mat->pos.raw, _aabb.min.raw);
        glm_vec3_add(aabb->max.raw, mat->pos.raw, _aabb.max.raw);
        // printf("%f, %f, %f\n", _aabb.min.x, _aabb.min.y, _aabb.min.z);
        render->visible = glm_aabb_frustum((vec3*)&_aabb, frustum);
        // render->visible = true;
        // if (render->visible) {
        //     printf("is visible\n");
        // } else {
        //     printf("is not visible\n");
        // }
    }
}

void RendererImport(ecs_world_t *world) {
    ECS_MODULE(world, Renderer);

    ECS_COMPONENT_DEFINE(world, Camera);
    ECS_COMPONENT_DEFINE(world, Material);
    ECS_COMPONENT_DEFINE(world, DeltaTime);
    ECS_COMPONENT_DEFINE(world, Render3D);
    ECS_COMPONENT_DEFINE(world, AABB);
    ECS_COMPONENT_DEFINE(world, Light);

    ECS_TAG_DEFINE(world, RenderOutline);

    ECS_SYSTEM(world, check_visible, PPhysics, Render3D, WorldTransform, AABB);

    ecs_system_init(world, &(ecs_system_desc_t){
        .entity = ecs_entity(world, {
            .name = "matrix_from_transform",
            .add = { ecs_dependson(PPostPhysics) },
        }),
        .query.filter.terms = {
            { .id = ecs_id(Transform), .inout = EcsIn },
            { .id = ecs_id(Matrix4),  .oper = EcsOptional, .inout = EcsOut },
            { .id = ecs_id(WorldTransform), .src.flags = EcsParent | EcsCascade, .oper = EcsOptional, .inout = EcsInOut },
        },
        .callback = matrix_from_transform,
    });
}
