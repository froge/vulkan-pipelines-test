#version 460

#extension GL_EXT_nonuniform_qualifier : require
#extension GL_EXT_scalar_block_layout : require

layout (location = 0) out vec4 color;
layout (location = 1) out vec4 frag_normal;
layout (location = 2) out vec4 frag_pos;
layout (location = 3) out uvec2 mat;

layout (location = 0) in vec2 tex_coord;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 pos;
layout (location = 3) in flat int draw_index;

struct MaterialInfo {
    bool lit;
    uint tex;
    uint metallic_roughness_tex;
    vec3 albedo;
    float metallic;
    float roughness;
};

layout (set = 0, binding = 2, scalar) readonly buffer fortnite {
    MaterialInfo materials[];
};

layout (set = 0, binding = 0) uniform sampler2D tex[];

struct ObjectInfo {
    mat4 model;
    mat3 inv_model;
    uint mesh;
    uint mat;
    uint pad;
};

layout (set = 1, binding = 0, scalar) readonly buffer fort {
    ObjectInfo objects[];
};

layout (set = 2, binding = 0, scalar) uniform ubo0 {
    mat4 proj;
    mat4 view;
    vec3 cam_pos;
};

// layout (push_constant) uniform pc {
//     layout (offset = 64) vec3 albedo;
//     uint source;
//     uint index;
// };

void main () {
    vec3 tex_col;
    // if (gl_FragCoord.y > 1440/2) {
    ObjectInfo object = objects[draw_index];
    // if (source != 0) {
    color.rg = tex_coord; 
    // color.b = float(object.mat) / 255.0;
    // color.rgb = texture(tex[materials[object.mat].tex], tex_coord).rgb * materials[object.mat].albedo;
    // } else {
    //     color.rgb = albedo;
    // }
    // } else {
        // tex_col = texture(tex[1], tex_coord).rgb; 
    // }
    // color.rgb = (tex_col * (0.1 + max(dot(normalize(vec3(-1, -1, -1)), (normal)), 0.0)));
    // color.rgb = (normal + 1.0) / 2.0;
    color.a = 1.0;
    frag_normal.rgb = normalize(normal);
    frag_normal.a = 1.0;
    frag_pos.rgb = pos - cam_pos;
    mat.r = object.mat;
    // mat.g = draw_index;
}
