#include "cglm/io.h"
#include "cglm/vec3.h"
#define CGLTF_IMPLEMENTATION
#include "cgltf.h"

#include "common.h"
#include "renderer.h"
#include "physics.h"
#include "vulkan/vulkan.h"
#include "vk_mem_alloc.h"
#include "convars.h"
#include "vulkan.h"

#include "stb_image.h"

ecs_entity_t load_gltf_mesh(ecs_world_t *world, cgltf_node *node, ecs_entity_t parent) {
  cgltf_mesh *mesh = node->mesh;

  ecs_entity_t subparent = ecs_new_id(world);
  ecs_set_name(world, subparent, node->name);



  Transform fortnite;

  // if (node->has_translation) {
    glm_vec3_copy(node->translation, fortnite.pos.raw);
    // ecs_set_ptr(world, entity, Position, &node->translation);
  // }
  // if (node->has_scale) {
    glm_vec3_copy(node->scale, fortnite.scale.raw);
    // ecs_set_ptr(world, entity, Scale, &node->scale);
  // }
  // if (node->has_rotation) {
  fortnite.rotation.x = node->rotation[0];
  fortnite.rotation.y = node->rotation[1];
  fortnite.rotation.z = node->rotation[2];
  fortnite.rotation.w = node->rotation[3];
    // glm_quat_copy(node->rotation, fortnite.rotation.raw);
    // ecs_set_ptr(world, entity, Rotation, &node->rotation);
  // }
  // glm_vec4_copy(node->rotation, transform.rotation.raw);
    ecs_add_pair(world, subparent, EcsChildOf, parent);



  for (int i = 0; i < mesh->primitives_count; i++) {
    ecs_entity_t entity = ecs_new_id(world);
    ecs_set_ptr(world, entity, Transform, &fortnite);
    ecs_add(world, entity, Matrix4);
    Mesh out_mesh;
    cgltf_primitive *primitive = &mesh->primitives[i];
    cgltf_accessor *indices = primitive->indices;

    out_mesh.verts = indices->count;

    struct Vertex *vertices = malloc(sizeof(struct Vertex) * primitive->attributes->data->count);
    u32 *indices_b = malloc(sizeof(u32) * indices->count);
    size_t ibase = (size_t)indices->buffer_view->buffer->data + (size_t)indices->buffer_view->offset + (size_t)indices->offset;
    for (int i = 0; i < indices->count; i++) {
      indices_b[i] = *(u16*)(ibase + (size_t)indices->stride * (size_t)i);
    }

    AABB bounds;
  
    for (int i = 0; i < primitive->attributes_count; i++) {
      cgltf_attribute *attr = &primitive->attributes[i];
      cgltf_accessor *accessor = attr->data;
      size_t base = (size_t)accessor->buffer_view->buffer->data + (size_t)accessor->buffer_view->offset + (size_t)accessor->offset;
      for (int v = 0; v < accessor->count; v++) {
        float *chud = (float*)(base + (size_t) accessor->stride * (size_t)v);
        switch (attr->type) {
          case cgltf_attribute_type_position:
            {
              memcpy(vertices[v].pos, chud, sizeof(vec3));
              for (int i = 0; i < 3; i++) {
                if (chud[i] < bounds.min.raw[i]) {
                  bounds.min.raw[i] = chud[i];
                }
                if (chud[i] > bounds.max.raw[i]) {
                  bounds.max.raw[i] = chud[i];
                }
              }
            }
            break;
          case cgltf_attribute_type_normal:
            {
              memcpy(vertices[v].norm, chud, sizeof(vec3));
            }
            break;
          case cgltf_attribute_type_texcoord:
            {
              memcpy(vertices[v].tex, chud, sizeof(vec2));
            }
            break;
        }
      }
    }

    ecs_set_ptr(world, entity, AABB, &bounds);

    Buffer buf = create_vertex_buffer(world, vertices, sizeof(struct Vertex) * primitive->attributes->data->count);
    Buffer ibuf = create_index_buffer(world, indices_b, sizeof(u32) * indices->count);
    CollisionMesh cmesh = {
        indices->count,
        malloc(cmesh.verts * sizeof(vec3)),
    };

    for (int i = 0; i < cmesh.verts; i++) {
        cmesh.data[i].x = vertices[indices_b[i]].pos[0];
        cmesh.data[i].y = vertices[indices_b[i]].pos[1];
        cmesh.data[i].z = vertices[indices_b[i]].pos[2];
    }

    MainSets *sets = ecs_singleton_get_mut(world, MainSets);
    Material mat = { };
    mat.texture = ecs_lookup(world, "white_tex");
    mat.metallic_roughness_texture = ecs_lookup(world, "white_tex");
    ecs_entity_t mat_e = ecs_lookup(world, primitive->material->name);
    if (!mat_e) {
      if (primitive->material->has_pbr_metallic_roughness) {
        mat.albedo.r = primitive->material->pbr_metallic_roughness.base_color_factor[0];
        mat.albedo.g = primitive->material->pbr_metallic_roughness.base_color_factor[1];
        mat.albedo.b = primitive->material->pbr_metallic_roughness.base_color_factor[2];
        // mat.albedo.r = pow(primitive->material->pbr_metallic_roughness.base_color_factor[0], 2.2);
        // mat.albedo.g = pow(primitive->material->pbr_metallic_roughness.base_color_factor[1], 2.2);
        // mat.albedo.b = pow(primitive->material->pbr_metallic_roughness.base_color_factor[2], 2.2);
        // mat.albedo.r = mat.albedo.r > 1.0 ? 1.0 : mat.albedo.r;
        // mat.albedo.g = mat.albedo.g > 1.0 ? 1.0 : mat.albedo.g;
        // mat.albedo.b = mat.albedo.b > 1.0 ? 1.0 : mat.albedo.b;
        // mat.roughness = pow(primitive->material->pbr_metallic_roughness.roughness_factor, 1.0 / 2);
        mat.roughness = primitive->material->pbr_metallic_roughness.roughness_factor;
        mat.metallic = primitive->material->pbr_metallic_roughness.metallic_factor;
        cgltf_texture *texture = primitive->material->pbr_metallic_roughness.base_color_texture.texture;
        cgltf_texture *metallic_roughness_texture = primitive->material->pbr_metallic_roughness.metallic_roughness_texture.texture;
        if (texture) {
          int x, y, channels = 0;
          stbi_uc *texture_data;
          if (!texture->image->buffer_view) {
            // char path[100] = "models/";
            char path[100] = "models/glTF/";
            strcat(path, texture->image->uri);
            texture_data = stbi_load(path, &x, &y, &channels, 4);
          } else {
            texture_data = stbi_load_from_memory((stbi_uc*)((u64)texture->image->buffer_view->buffer->data + texture->image->buffer_view->offset), texture->image->buffer_view->size, &x, &y, &channels, 4);
          }

          if (!texture_data) {
              printf("texture failed to load\n");
              exit(1);
          }

          VkSampler sampler = *ecs_singleton_get(world, VkSampler);

          ecs_entity_t tex_e = load_texture2d_from_data(world, texture_data, x, y, 4);
          stbi_image_free(texture_data);
          descriptor_load_image(world, tex_e, sampler, *ecs_get_mut(world, sets->global, VkDescriptorSet), sets->tex_ind);
          sets->tex_ind += 1;
          mat.texture = tex_e;
        }
        if (metallic_roughness_texture) {
          int x, y, channels = 0;
          stbi_uc *texture_data;
          if (!metallic_roughness_texture->image->buffer_view) {
            // char path[100] = "models/";
            char path[100] = "models/glTF/";
            strcat(path, metallic_roughness_texture->image->uri);
            texture_data = stbi_load(path, &x, &y, &channels, STBI_rgb_alpha);
          } else {
            texture_data = stbi_load_from_memory((stbi_uc*)((u64)metallic_roughness_texture->image->buffer_view->buffer->data + metallic_roughness_texture->image->buffer_view->offset), metallic_roughness_texture->image->buffer_view->size, &x, &y, &channels, STBI_rgb_alpha);
          }

          VkSampler sampler = *ecs_singleton_get(world, VkSampler);
          struct TextureConfig tex = {
              x,
              y,
              1,
              VK_FORMAT_R8G8B8A8_UNORM,
              1,
              VK_IMAGE_VIEW_TYPE_2D,
          };
          ecs_entity_t tex_e = load_texture_from_data(world, texture_data, &tex);
          stbi_image_free(texture_data);
          descriptor_load_image(world, tex_e, sampler, *ecs_get_mut(world, sets->global, VkDescriptorSet), sets->tex_ind);
          sets->tex_ind += 1;
          mat.metallic_roughness_texture = tex_e;
        }
      }
      mat_e = new_material(world, mat.albedo, mat.texture, mat.metallic_roughness_texture, 1, mat.metallic, mat.roughness, primitive->material->name);
    }
  
    free(vertices);
    free(indices_b);
    out_mesh.vert_buf = buf;
    out_mesh.ind_buf = ibuf;
    VkDevice dev = *ecs_singleton_get(world, VkDevice);

    out_mesh.ind = sets->mesh_ind;
    sets->mesh_ind += 1;

    ecs_entity_t meshed = ecs_set_ptr(world, 0, Mesh, &out_mesh);
    ecs_set_ptr(world, entity, CollisionMesh, &cmesh);
    ecs_set(world, entity, Render3D, { mat_e, meshed, 1 });

    struct MeshInfo *info = &sets->meshes[out_mesh.ind];

    VkBufferDeviceAddressInfo address_info = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO,
        .buffer = buf.buffer,
    };

    info->verts = out_mesh.verts;
    info->vert_buf = vkGetBufferDeviceAddress(dev, &address_info);
    address_info.buffer = ibuf.buffer;
    info->ind_buf = vkGetBufferDeviceAddress(dev, &address_info);

    ecs_add_pair(world, entity, EcsChildOf, subparent);
  }
  // ecs_add(world, entity, Selectable);
  // ecs_set(world, entity, Velocity, {});
  return 0;
}

void load_gltf_model(ecs_world_t *world, ecs_entity_t target, char *filename) {
  cgltf_options options = {};
  cgltf_data *data = NULL;
  if (cgltf_parse_file(&options, filename, &data) != cgltf_result_success) {
    printf("wtf\n");
  }
  // cgltf_load_buffers(&options, data, "models/");
  cgltf_load_buffers(&options, data, "models/glTF/");

  // ecs_entity_t scope = ecs_set_scope(world, target);
  for (int i = 0; i < data->nodes_count; i++) {
    if (data->nodes[i].mesh) {
      MainSets *sets = ecs_singleton_get(world, MainSets);
      ecs_entity_t this = load_gltf_mesh(world, &data->nodes[i], target);
    }
  }
  // ecs_set_scope(world, scope);
  cgltf_free(data);
}
