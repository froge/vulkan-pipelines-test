#include "SDL2/SDL.h"
#include "SDL_events.h"
#include "SDL_gamecontroller.h"
#include "SDL_haptic.h"
#include "cglm/vec3.h"
#include "common.h"

#include "phases.h"
#include "vulkan/vulkan.h"
#include "vk_mem_alloc.h"
#include "cglm/call.h"
#include "miniaudio.h"
#include "components.h"
#include "convars.h"
#include "physics.h"
#include "renderer.h"
#include "vulkan.h"
#include "window.h"
#include "networking.h"
#include "ui.h"

#define INPUT_H_DECLARE
#include "input.h"

void handle_input(ecs_iter_t *iter) {
    ecs_world_t *world = iter->world;

    // Host *_host = ecs_singleton_get_mut(world, Host);
    // if (!_host->server) {
        ecs_singleton_set(world, DeltaTime, { iter->delta_time });
    // }

    ConVar *textinput = ecs_get(world, CurrentTextInput, ConVar);

    ecs_query_t **action_queries = iter->ctx;

    // u64 delta_tick = SDL_GetTicks() - last_tick;
    // printf("%lu\n", last_tick);
    // bool should = false;
    // if (delta_tick > 20) {
    //     should = true;
    //     last_tick = (u64)SDL_GetTicks();
    // }

    SDL_Event event;

    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                {
                    // ecs_entity_t mousee = ecs_lookup(world, "quit");
                    // ConVar *mouse = ecs_get_mut(world, mousee, ConVar);
                    // mouse->i = !mouse->i;
                    // ecs_modified(world, mousee, ConVar);
                    ecs_quit(world);
                }
                break;
            case SDL_WINDOWEVENT:
                switch (event.window.event) {
                    case SDL_WINDOWEVENT_RESIZED:
                        {
                        ecs_entity_t mousee = ecs_lookup(world, "resized");
                        ConVar *mouse = ecs_get_mut(world, mousee, ConVar);
                        mouse->i = 1;
                        ecs_modified(world, mousee, ConVar);

                        // Window *window = ecs_singleton_get_mut(world, Window);
                        // VkDevice dev = *ecs_singleton_get_mut(world, VkDevice);
                        // VkPhysicalDevice physdev = *ecs_singleton_get_mut(world, VkPhysicalDevice);
                        // VmaAllocator allocator = *ecs_singleton_get_mut(world, VmaAllocator);
                        // Swapchain *chudchain = ecs_singleton_get_mut(world, Swapchain);
                        // VkSurfaceKHR surf = chudchain->info.surface;

                        // VkSurfaceCapabilitiesKHR caps;

                        // vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physdev, surf, &caps);

                        // Framebuffer *framebuffer = ecs_get_mut(world, window->framebuffer, Framebuffer);
                        // Texture *depth = ecs_get_mut(world, chudchain->depth, Texture);

                        // ecs_entity_t vsync_c = ecs_lookup(world, "vsync");
                        // const ConVar *vsync = ecs_get(world, vsync_c, ConVar);

                        // recreate_swapchain(dev, physdev, chudchain, framebuffer, caps.currentExtent.width, caps.currentExtent.height, vsync->i);

                        // delete_texture(allocator, dev, depth);
                        // ecs_modified(world, chudchain->depth, Texture);

                        // Texture new_texture = create_fb_texture_manual(world, framebuffer->x, framebuffer->y, FBTEX_DEPTH);

                        // ecs_set_ptr(world, chudchain->depth, Texture, &new_texture);

                        // VkRenderingAttachmentInfoKHR *info = (VkRenderingAttachmentInfoKHR*)framebuffer->render_info.pDepthAttachment;
                        // info->imageView = depth->view;

                        // ecs_modified(world, window->framebuffer, Framebuffer);
                        // ecs_singleton_modified(world, Swapchain);
                    }
                }
                break;
            case SDL_MOUSEBUTTONDOWN:
                {
                    ecs_iter_t it = ecs_query_iter(world, action_queries[4]);

                    while (ecs_query_next(&it)) {
                        InputAction *as = ecs_field(&it, InputAction, 1);
                        MouseButtonAction *kas = ecs_field(&it, MouseButtonAction, 2);
                        for (int i = 0; i < it.count; i++) {
                            MouseButtonAction *kaction = &kas[i];
                            InputAction *action = &as[i];
                            if (event.button.button == kaction->button) {
                                action->callback(world, action->data);
                            }
                        }
                    }
                    break;
                }
            case SDL_MOUSEBUTTONUP:
                {
                    ecs_iter_t it = ecs_query_iter(world, action_queries[3]);

                    while (ecs_query_next(&it)) {
                        InputAction *as = ecs_field(&it, InputAction, 1);
                        MouseButtonAction *kas = ecs_field(&it, MouseButtonAction, 2);
                        for (int i = 0; i < it.count; i++) {
                            MouseButtonAction *kaction = &kas[i];
                            InputAction *action = &as[i];
                            if (event.button.button == kaction->button) {
                                action->callback(world, action->data);
                            }
                        }
                    }
                    break;
                }
            case SDL_KEYDOWN:
                {
                    if (event.key.repeat && !textinput->e) {
                        continue;
                    }
                    ecs_iter_t it = ecs_query_iter(world, action_queries[1]);

                    while (ecs_query_next(&it)) {
                        InputAction *as = ecs_field(&it, InputAction, 1);
                        KeyboardAction *kas = ecs_field(&it, KeyboardAction, 2);
                        for (int i = 0; i < it.count; i++) {
                            KeyboardAction *kaction = &kas[i];
                            InputAction *action = &as[i];
                            if (event.key.keysym.scancode == kaction->key) {
                                action->callback(world, action->data);
                            }
                        }
                    }
                    break;
                }
            case SDL_KEYUP:
                {
                    ecs_iter_t it = ecs_query_iter(world, action_queries[0]);

                    while (ecs_query_next(&it)) {
                        InputAction *as = ecs_field(&it, InputAction, 1);
                        KeyboardAction *kas = ecs_field(&it, KeyboardAction, 2);
                        for (int i = 0; i < it.count; i++) {
                            InputAction *action = &as[i];
                            KeyboardAction *kaction = &kas[i];
                            if (event.key.keysym.scancode == kaction->key) {
                                action->callback(world, action->data);
                            }
                        }
                    }
                    break;
                }
            case SDL_CONTROLLERBUTTONUP:
                {
                    ecs_iter_t it = ecs_query_iter(world, action_queries[6]);

                    while (ecs_query_next(&it)) {
                        InputAction *as = ecs_field(&it, InputAction, 1);
                        GamepadAction *kas = ecs_field(&it, GamepadAction, 2);
                        for (int i = 0; i < it.count; i++) {
                            InputAction *action = &as[i];
                            GamepadAction *kaction = &kas[i];
                            if (event.cbutton.button == kaction->button) {
                                action->callback(world, action->data);
                            }
                        }
                    }
                    break;
                }
            case SDL_CONTROLLERBUTTONDOWN:
                {
                    ecs_iter_t it = ecs_query_iter(world, action_queries[7]);

                    while (ecs_query_next(&it)) {
                        InputAction *as = ecs_field(&it, InputAction, 1);
                        GamepadAction *kas = ecs_field(&it, GamepadAction, 2);
                        for (int i = 0; i < it.count; i++) {
                            InputAction *action = &as[i];
                            GamepadAction *kaction = &kas[i];
                            if (event.cbutton.button == kaction->button) {
                                action->callback(world, action->data);
                            }
                        }
                    }
                    break;
                }
            case SDL_MOUSEWHEEL:
            {
                ecs_iter_t it = ecs_query_iter(world, action_queries[10]);

                while (ecs_query_next(&it)) {
                    AxisAction *as = ecs_field(&it, AxisAction, 1);
                    MouseAxisAction *kas = ecs_field(&it, MouseAxisAction, 2);
                    for (int i = 0; i < it.count; i++) {
                        AxisAction *action = &as[i];
                        MouseAxisAction *kaction = &kas[i];
                        if (kaction->axis == axis_mouse_wheel) {
                            struct AxisValue val = {
                                event.wheel.y,
                            };
                            action->callback(world, val, action->data);
                        }
                    }
                }
                break;
            }
            case SDL_MOUSEMOTION:
            {
                ecs_entity_t mousee = ecs_lookup(world, "mouse_captured");
                const ConVar *mouse = ecs_get(world, mousee, ConVar);
                if (!mouse->i)
                    break;
                const float sensitivity = 0.05;
                float xoffset = event.motion.xrel;
                float yoffset = event.motion.yrel;
                xoffset *= sensitivity;
                yoffset *= sensitivity;
                Camera *cam = ecs_singleton_get_mut(world, Camera);
                cam_rot_yaw(cam, xoffset);
                cam_rot_pitch(cam, -yoffset);
                Transform *cam_pos = ecs_get_mut(world, ecs_id(Camera), Transform);
                // glm_vec3_negate_to(cam->front, cam_pos->pos.raw);
                // glm_vec3_scale(cam_pos->pos.raw, 8, cam_pos->pos.raw);
                ecs_singleton_modified(world, Camera);
                break;
            }
            case SDL_CONTROLLERDEVICEADDED:
                {
                    puts("controller");
                    SDL_GameController *controller = SDL_GameControllerOpen(event.cdevice.which);
                    // SDL_Joystick *joy = SDL_GameControllerGetJoystick(controller);
                    // SDL_Haptic *haptic = SDL_HapticOpenFromJoystick(joy);
                    ecs_singleton_set(world, Gamepad, { controller });
                    if (SDL_GameControllerHasSensor(controller, SDL_SENSOR_GYRO)) {
                        // SDL_GameControllerSetSensorEnabled(controller, SDL_SENSOR_GYRO, SDL_TRUE);
                    }
                    break;
                }
            case SDL_TEXTINPUT:
                {
                    if (!textinput->e) {
                        break;
                    }
                    const Textbox *box = ecs_get(world, textinput->e, Textbox);
                    Label *label = ecs_get_mut(world, box->label, Label);
                    strcat(label->text, event.text.text);
                    break;
                }
        }
    }

    const u8 *kb = SDL_GetKeyboardState(NULL);

    ecs_iter_t it = ecs_query_iter(world, action_queries[2]);
    while (ecs_query_next(&it)) {
        InputAction *as = ecs_field(&it, InputAction, 1);
        KeyboardAction *kas = ecs_field(&it, KeyboardAction, 2);
        for (int i = 0; i < it.count; i++) {
            InputAction *action = &as[i];
            KeyboardAction *kaction = &kas[i];
            if (kb[kaction->key]) {
                if (ecs_has(world, it.entities[i], InputInterval)) {
                    InputInterval *in = ecs_get_mut(world, it.entities[i], InputInterval);
                    if (SDL_GetTicks() - in->last_tick <= in->interval * 1000.0) {
                        continue;
                    } else {
                        in->last_tick = SDL_GetTicks();
                    }
                }
                action->callback(world, action->data);
                // ecs_set(world, 0, NetworkEvent, { network_event_input_action, it.entities[i] });
            }
        }
    }

    const u32 mouses = SDL_GetMouseState(NULL, NULL);

    it = ecs_query_iter(world, action_queries[5]);

    while (ecs_query_next(&it)) {
        InputAction *as = ecs_field(&it, InputAction, 1);
        MouseButtonAction *kas = ecs_field(&it, MouseButtonAction, 2);
        for (int i = 0; i < it.count; i++) {
            MouseButtonAction *kaction = &kas[i];
            InputAction *action = &as[i];
            if (mouses & SDL_BUTTON(kaction->button)) {
                if (ecs_has(world, it.entities[i], InputInterval)) {
                    InputInterval *in = ecs_get_mut(world, it.entities[i], InputInterval);
                    if (SDL_GetTicks() - in->last_tick <= in->interval * 1000.0) {
                        continue;
                    } else {
                        in->last_tick = SDL_GetTicks();
                    }
                }
                action->callback(world, action->data);
            }
        }
    }

    const Gamepad *pad = ecs_singleton_get(world, Gamepad);

    it = ecs_query_iter(world, action_queries[8]);

    while (ecs_query_next(&it)) {
        InputAction *as = ecs_field(&it, InputAction, 1);
        GamepadAction *kas = ecs_field(&it, GamepadAction, 2);
        for (int i = 0; i < it.count; i++) {
            GamepadAction *kaction = &kas[i];
            InputAction *action = &as[i];
            if (pad && SDL_GameControllerGetButton(pad->controller, kaction->button)) {
                if (ecs_has(world, it.entities[i], InputInterval)) {
                    InputInterval *in = ecs_get_mut(world, it.entities[i], InputInterval);
                    if (SDL_GetTicks() - in->last_tick <= in->interval * 1000.0) {
                        continue;
                    } else {
                        in->last_tick = SDL_GetTicks();
                    }
                }
                action->callback(world, action->data);
            }
        }
    }

    it = ecs_query_iter(world, action_queries[9]);

    while (ecs_query_next(&it)) {
        AxisAction *as = ecs_field(&it, AxisAction, 1);
        GamepadAxisAction *kas = ecs_field(&it, GamepadAxisAction, 2);
        for (int i = 0; i < it.count; i++) {
            AxisAction *action = &as[i];
            GamepadAxisAction *kaction = &kas[i];
            if (pad) {
                struct AxisValue value;
                float deadzones[] = {
                    0.0,
                    0.1,
                    0.1,
                    0.0,
                    0.0,
                };
                switch (kaction->axis) {
                    case axis_gyroscope:
                        {
                            float data[3];
                            SDL_GameControllerGetSensorData(pad->controller, SDL_SENSOR_GYRO, data, 3);
                            value.a0 = data[0];
                            value.a1 = data[1];
                            value.a2 = data[2];
                            break;
                        }
                    case axis_left_stick:
                        {
                            value.a0 = (float)SDL_GameControllerGetAxis(pad->controller, SDL_CONTROLLER_AXIS_LEFTX) / 32768;
                            value.a1 = (float)SDL_GameControllerGetAxis(pad->controller, SDL_CONTROLLER_AXIS_LEFTY) / 32768;
                            float axis = glm_vec2_distance((vec2){0.0, 0.0}, (vec2){value.a0, value.a1});
                            if (axis < deadzones[kaction->axis] && axis > -deadzones[kaction->axis]) {
                                continue;
                            }
                            break;
                        }
                    case axis_right_stick:
                        {
                            value.a0 = (float)SDL_GameControllerGetAxis(pad->controller, SDL_CONTROLLER_AXIS_RIGHTX) / 32768;
                            value.a1 = (float)SDL_GameControllerGetAxis(pad->controller, SDL_CONTROLLER_AXIS_RIGHTY) / 32768;
                            float axis = glm_vec2_distance((vec2){0.0, 0.0}, (vec2){value.a0, value.a1});
                            if (axis < deadzones[kaction->axis] && axis > -deadzones[kaction->axis]) {
                                continue;
                            }
                            break;
                        }
                    case axis_left_trigger:
                        {
                            value.a0 = (float)SDL_GameControllerGetAxis(pad->controller, SDL_CONTROLLER_AXIS_TRIGGERLEFT) / 32768;
                            float axis = value.a0;
                            // if (axis < deadzones[kaction->axis] && axis > -deadzones[kaction->axis]) {
                                // continue;
                            // }
                            break;
                        }
                    case axis_right_trigger:
                        {
                            value.a0 = (float)SDL_GameControllerGetAxis(pad->controller, SDL_CONTROLLER_AXIS_TRIGGERRIGHT) / 32768;
                            float axis = value.a0;
                            // if (axis < deadzones[kaction->axis] && axis > -deadzones[kaction->axis]) {
                            //     continue;
                            // }
                            break;
                        }
                }
                action->callback(world, value, action->data);
            }
        }
    }
}

ecs_entity_t new_action_set(ecs_world_t *world, char *name) {
    return ecs_set_name(world, ecs_new_w_pair(world, EcsChildOf, ecs_lookup(world, "action_sets")), name);
}

void action_set_enable(ecs_world_t *world, ecs_entity_t set) {
    ecs_iter_t it = ecs_children(world, set);
    while (ecs_children_next(&it)) {
        for (int i = 0; i < it.count; i++) {
            ecs_enable_component(world, it.entities[i], InputAction, true);
            ecs_enable_component(world, it.entities[i], AxisAction, true);
        }
    }
}
void action_set_disable(ecs_world_t *world, ecs_entity_t set) {
    ecs_iter_t it = ecs_children(world, set);
    while (ecs_children_next(&it)) {
        for (int i = 0; i < it.count; i++) {
            ecs_enable_component(world, it.entities[i], InputAction, false);
            ecs_enable_component(world, it.entities[i], AxisAction, false);
        }
    }
}

ecs_entity_t new_kb_action(ecs_world_t *world, ecs_entity_t set, ecs_id_t event_type, InputActionFunc *callback, void *data, SDL_Scancode key) {
    ecs_entity_t action = ecs_new_w_id(world, event_type);
    ecs_set(world, action, InputAction, { callback, data });
    ecs_set(world, action, KeyboardAction, { key });
    return action;
}

ecs_entity_t new_mouse_action(ecs_world_t *world, ecs_entity_t set, ecs_id_t event_type, InputActionFunc *callback, void *data, u32 button) {
    ecs_entity_t action = ecs_new_w_id(world, event_type);
    ecs_set(world, action, InputAction, { callback, data });
    ecs_set(world, action, MouseButtonAction, { button });
    return action;
}

ecs_entity_t new_mouse_axis_action(ecs_world_t *world, ecs_entity_t set, AxisActionFunc *callback, void *data, MouseAxis axis) {
    ecs_entity_t action = ecs_new_id(world);
    ecs_set(world, action, AxisAction, { callback, data });
    ecs_set(world, action, MouseAxisAction, { axis });
    return action;
}

ecs_entity_t new_gamepad_action(ecs_world_t *world, ecs_entity_t set, ecs_id_t event_type, InputActionFunc *callback, void *data, SDL_GameControllerButton button) {
    ecs_entity_t action = ecs_new_w_id(world, event_type);
    ecs_set(world, action, InputAction, { callback, data });
    ecs_set(world, action, GamepadAction, { button });
    return action;
}

ecs_entity_t new_gamepad_axis_action(ecs_world_t *world, ecs_entity_t set, AxisActionFunc *callback, void *data, GamepadAxis axis) {
    ecs_entity_t action = ecs_new_id(world);
    ecs_set(world, action, AxisAction, { callback, data });
    ecs_set(world, action, GamepadAxisAction, { axis });
    return action;
}

void InputImport(ecs_world_t *world) {
    ECS_MODULE(world, Input);

    
    ECS_COMPONENT_DEFINE(world, InputAction);
    ECS_COMPONENT_DEFINE(world, AxisAction);
    ECS_COMPONENT_DEFINE(world, KeyboardAction);
    ECS_COMPONENT_DEFINE(world, MouseButtonAction);
    ECS_COMPONENT_DEFINE(world, MouseAxisAction);
    ECS_COMPONENT_DEFINE(world, Gamepad);
    ECS_COMPONENT_DEFINE(world, GamepadAction);
    ECS_COMPONENT_DEFINE(world, GamepadAxisAction);
    ECS_COMPONENT_DEFINE(world, InputInterval);

    ECS_TAG_DEFINE(world, action_up);
    ECS_TAG_DEFINE(world, action_down);
    ECS_TAG_DEFINE(world, action_pressed);
        
    ecs_query_t **action_queries = malloc(sizeof(void*) * 11);
    
    action_queries[0] = ecs_query_init(world, &(ecs_query_desc_t) {
        .filter.terms = {
            { ecs_id(InputAction) },
            { ecs_id(KeyboardAction) },
            { action_up },
        }});

    action_queries[1] = ecs_query_init(world, &(ecs_query_desc_t) {
        .filter.terms = {
            { ecs_id(InputAction) },
            { ecs_id(KeyboardAction) },
            { action_down },
        }});

    action_queries[2] = ecs_query_init(world, &(ecs_query_desc_t) {
        .filter.terms = {
            { ecs_id(InputAction) },
            { ecs_id(KeyboardAction) },
            { action_pressed },
        }});

    action_queries[3] = ecs_query_init(world, &(ecs_query_desc_t) {
        .filter.terms = {
            { ecs_id(InputAction) },
            { ecs_id(MouseButtonAction) },
            { action_up },
        }});

    action_queries[4] = ecs_query_init(world, &(ecs_query_desc_t) {
        .filter.terms = {
            { ecs_id(InputAction) },
            { ecs_id(MouseButtonAction) },
            { action_down },
        }});

    action_queries[5] = ecs_query_init(world, &(ecs_query_desc_t) {
        .filter.terms = {
            { ecs_id(InputAction) },
            { ecs_id(MouseButtonAction) },
            { action_pressed },
        }});

    action_queries[6] = ecs_query_init(world, &(ecs_query_desc_t) {
        .filter.terms = {
            { ecs_id(InputAction) },
            { ecs_id(GamepadAction) },
            { action_up },
        }});

    action_queries[7] = ecs_query_init(world, &(ecs_query_desc_t) {
        .filter.terms = {
            { ecs_id(InputAction) },
            { ecs_id(GamepadAction) },
            { action_down },
        }});

    action_queries[8] = ecs_query_init(world, &(ecs_query_desc_t) {
        .filter.terms = {
            { ecs_id(InputAction) },
            { ecs_id(GamepadAction) },
            { action_pressed },
        }});

    action_queries[9] = ecs_query_init(world, &(ecs_query_desc_t) {
        .filter.terms = {
            { ecs_id(AxisAction) },
            { ecs_id(GamepadAxisAction) },
        }});

    action_queries[10] = ecs_query_init(world, &(ecs_query_desc_t) {
        .filter.terms = {
            { ecs_id(AxisAction) },
            { ecs_id(MouseAxisAction) },
        }});

    ecs_id(handle_input) = ecs_system_init(world, &(ecs_system_desc_t){
        .entity = ecs_entity(world, {
            .name = "handle_input",
            .add = { ecs_dependson(PInput) },
        }),
        .query.filter.terms = {
            { .id = ecs_id(Camera), .inout = EcsInOut, .src.flags = EcsIsEntity },
        },
        .ctx = action_queries,
        .callback = handle_input,
        .binding_ctx = NULL,
    });

    ecs_set_name(world, ecs_new_id(world), "action_sets");
}
