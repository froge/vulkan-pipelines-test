#pragma once

#include "flecs.h"
#include "cglm/struct.h"
#include "common.h"
#include "actions.h"

#ifdef UI_H_DECLARE
#define UI_EXTERN
#else
#define UI_EXTERN extern
#endif

typedef vec2s Vector2;
typedef mat3s Matrix3;

typedef struct {
  Vector2 pos;
  Vector2 scale;
  float rot;
}Transform2D;

typedef struct {
  float pts;
  char *text;
}Label;

typedef struct {
  ecs_entity_t label;
  ActionCallback *callback;
  u16 max;
}Textbox;

typedef struct {
  float w;
  float h;
  int rel;
}Container;

typedef struct {
  float x;
  float y;
}Position2D;

typedef enum {
  anchor_top = 1,
  anchor_bottom = 2,
  anchor_left = 4,
  anchor_right = 8,
}AnchorBits;

typedef struct {
  AnchorBits pos;
  AnchorBits scale;
}Anchor;

typedef struct {
  ecs_entity_t texture;
  int advance;
  vec2s bearing;
}Glyph;

typedef struct {
  Glyph *glyphs;
}Glyphs;

typedef Position2D PixelPosition;
typedef Position2D PixelContainer;

vec2s get_anchor_offset(vec2s size, AnchorBits anchor);

void UiImport(ecs_world_t *world);

UI_EXTERN ECS_ENTITY_DECLARE(CurrentTextInput);

UI_EXTERN ECS_COMPONENT_DECLARE(Transform2D);
UI_EXTERN ECS_COMPONENT_DECLARE(Matrix3);
UI_EXTERN ECS_COMPONENT_DECLARE(Label);
UI_EXTERN ECS_COMPONENT_DECLARE(Textbox);
UI_EXTERN ECS_COMPONENT_DECLARE(Container);
UI_EXTERN ECS_COMPONENT_DECLARE(Anchor);
UI_EXTERN ECS_COMPONENT_DECLARE(AnchorBits);
UI_EXTERN ECS_COMPONENT_DECLARE(Position2D);
UI_EXTERN ECS_COMPONENT_DECLARE(Glyphs);
UI_EXTERN ECS_COMPONENT_DECLARE(PixelPosition);
UI_EXTERN ECS_COMPONENT_DECLARE(PixelContainer);
