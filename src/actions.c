#define ACTION_H_DECLARE
#include "actions.h"

ecs_entity_t new_action_(ecs_world_t *world, Action *action) {
  ecs_entity_t scope = ecs_set_scope(world, ecs_lookup(world, "_actions"));
  ecs_entity_t action_e =  ecs_set_ptr(world, 0, Action, action);
  ecs_set_scope(world, scope);
  return action_e;
}

void ActionsImport(ecs_world_t *world) {
  ECS_MODULE(world, Actions);

  ecs_set_name(world, 0, "_actions");

  ECS_COMPONENT_DEFINE(world, Action);
}
