#pragma once

#include <vulkan/vulkan_core.h>
#ifdef VULKAN_H_DECLARE
#define VK_EXTERN
#else
#define VK_EXTERN extern
#endif

#include "common.h"

#define VK_FUNC(name) _ ## name = (PFN_ ## name)vkGetInstanceProcAddr(instance, #name)
#define VK_FUNCD(name) PFN_ ## name _ ## name

VK_EXTERN VK_FUNCD(vkCmdBeginRenderingKHR);
VK_EXTERN VK_FUNCD(vkCmdEndRenderingKHR);

typedef struct Buffer {
    VmaAllocation allocation;
    VkBuffer buffer;
    VkDeviceSize size;
}Buffer;

typedef struct Mesh {
    u32 ind;
    u32 verts;
    struct Buffer vert_buf;
    struct Buffer ind_buf;
}Mesh;

struct Vertex {
    vec3 pos;
    vec3 norm;
    vec2 tex;
};

struct TextureFormat {
    VkFormat format;
    VkImageUsageFlags usage;
    VkImageAspectFlags aspect;
    VkImageLayout layout;
    VkAccessFlags access_flags;
    VkPipelineStageFlags stage_flags;
    VkSampleCountFlags sample_count;
};

struct TextureConfig {
    u32 width;
    u32 height;
    u32 depth;
    VkFormat format;
    u32 mip_levels;
    VkImageViewType img_type;
};

#ifdef VULKAN_H_DECLARE
const struct TextureFormat FBTEX_DEPTH = {
    VK_FORMAT_D32_SFLOAT,
    VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
    VK_IMAGE_ASPECT_DEPTH_BIT,
    VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
    VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT,
    VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
    VK_SAMPLE_COUNT_1_BIT,
};
const struct TextureFormat FBTEX_COLOR = {
    .format = VK_FORMAT_R16G16B16A16_SFLOAT,
    .usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
    .aspect = VK_IMAGE_ASPECT_COLOR_BIT,
    .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    .access_flags = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_COLOR_ATTACHMENT_READ_BIT,
    .stage_flags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
    .sample_count = VK_SAMPLE_COUNT_1_BIT,
};
const struct TextureFormat FBTEX_UV = {
    .format = VK_FORMAT_R16G16_SFLOAT,
    .usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
    .aspect = VK_IMAGE_ASPECT_COLOR_BIT,
    .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    .access_flags = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_COLOR_ATTACHMENT_READ_BIT,
    .stage_flags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
    .sample_count = VK_SAMPLE_COUNT_1_BIT,
};
const struct TextureFormat FBTEX_NORM = {
    .format = VK_FORMAT_R16G16B16A16_SFLOAT,
    .usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
    .aspect = VK_IMAGE_ASPECT_COLOR_BIT,
    .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    .access_flags = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_COLOR_ATTACHMENT_READ_BIT,
    .stage_flags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
    .sample_count = VK_SAMPLE_COUNT_1_BIT,
};
const struct TextureFormat FBTEX_MAT = {
    .format = VK_FORMAT_R16G16_UINT,
    .usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
    .aspect = VK_IMAGE_ASPECT_COLOR_BIT,
    .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    .access_flags = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_COLOR_ATTACHMENT_READ_BIT,
    .stage_flags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
    .sample_count = VK_SAMPLE_COUNT_1_BIT,
};
#else
extern const struct TextureFormat FBTEX_DEPTH;
extern const struct TextureFormat FBTEX_COLOR;
extern const struct TextureFormat FBTEX_UV;
extern const struct TextureFormat FBTEX_NORM;
extern const struct TextureFormat FBTEX_MAT;
#endif

typedef struct Texture {
    VkImage image;
    VkImageView view;
    VmaAllocation allocation;
    struct TextureFormat format;
    int width;
    int height;
    int depth;
    u32 index;
    VkSampler sampler;
}Texture;

typedef struct UniformBuffer {
    VkBuffer buffer;
    VmaAllocation allocation;
    VmaAllocationInfo info;
    void *mapped;
    u32 max;
}UniformBuffer;

typedef struct Shader {
    VkShaderModule mod;
    VkShaderStageFlags stage;
}Shader;

typedef struct Pipeline {
    UniformBuffer *uniform;
    VkPipelineLayout layout;
    VkPipeline pipeline;
    VkDescriptorSet descriptor_set;
}Pipeline;

typedef struct Framebuffer {
    u32 attachment_count;
    ecs_entity_t *colors;
    ecs_entity_t depth_stencil;
    VkClearValue *clear_values;
    VkFramebuffer *fbo;
    VkRenderPass pass;
    u32 x;
    u32 y;
}Framebuffer;

typedef struct RenderPass RenderPass;

typedef void RenderPassObjectCallback(ecs_iter_t *it, VkCommandBuffer cmd, const Pipeline *pipeline, const RenderPass *pass, u32 index);

typedef struct {
    ecs_entity_t tex;
    u32 index;
}DescriptorDependency;

struct RenderPass {
    VkRenderPass pass;
    ecs_entity_t framebuffer;
    ecs_entity_t pipeline;
    ecs_query_t *query;
    RenderPassObjectCallback *callback;
    int indirect;
    VkClearValue *clears;
    // int dependencyc;
    // DescriptorDependency *dependencies;
};

typedef struct {
    VkSwapchainKHR swapchain;
    VkSwapchainKHR swapchain_old;
    VkSwapchainCreateInfoKHR info;
    VkImage *images;
    VkImageView *views;
    u32 count;
    VkSemaphore render_done_s;
    VkSemaphore image_ready;
    VkFence render_done;
    ecs_entity_t depth;
    u32 cur_swap_image;
    ecs_entity_t framebuffer;
}Swapchain;

Buffer create_vertex_buffer(ecs_world_t *world, void *content, VkDeviceSize size);
Buffer create_index_buffer(ecs_world_t *world, void *content, VkDeviceSize size);
void destroy_buffer(ecs_world_t *world, Buffer *buf);

ecs_entity_t new_material(ecs_world_t *world, vec3s albedo, ecs_entity_t texture, ecs_entity_t metallic_roughness_tex, u32 lit, f32 metallic, f32 roughness, char *name);

void descriptor_load_image(ecs_world_t *world, ecs_entity_t image, VkSampler sampler, VkDescriptorSet descriptor_set, u32 index);
ecs_entity_t load_texture(ecs_world_t *world, char *filename);
ecs_entity_t load_texture2d_from_data(ecs_world_t *world, void *data, int width, int height, int nr_channels);
ecs_entity_t load_texture_from_data(ecs_world_t *world, void *data, struct TextureConfig *conf);
void delete_texture(VmaAllocator allocator, VkDevice dev, Texture *texture);
void delete_textures(ecs_iter_t *it);

ecs_entity_t load_shader(ecs_world_t *world, char *filename, VkShaderStageFlags stage);

void render_meshes(ecs_iter_t *it);
void render_passes(ecs_iter_t *it);

void recreate_swapchain(ecs_world_t *world, u32 x, u32 y, u32 vsync);
void toggle_vsync(ecs_world_t *world, ConVar *val);

ecs_entity_t create_fb_texture(ecs_world_t *world, u32 x, u32 y, struct TextureFormat format);
Texture create_fb_texture_manual(ecs_world_t *world, u32 x, u32 y, struct TextureFormat format);
void init_framebuffer(ecs_world_t *world, ecs_entity_t framebuffer);

void wait_for_frame(ecs_iter_t *it);
void present_frame(ecs_iter_t *it);
void begin_new_frame(ecs_iter_t *it);

void render_background(ecs_iter_t *it, VkCommandBuffer cmd, const Pipeline *pipeline, const RenderPass *pass, u32 index);
void render_main(ecs_iter_t *it, VkCommandBuffer cmd, const Pipeline *pipeline, const RenderPass *pass, u32 index);
// void render_outline(ecs_iter_t *it, VkCommandBuffer cmd, const Pipeline *pipeline, const RenderPass *pass);
void render_text(ecs_iter_t *it, VkCommandBuffer cmd, const Pipeline *pipeline, const RenderPass *pass, u32 index);

void VulkanImport(ecs_world_t *world);

typedef struct MeshInfo {
    u32 verts;
    VkDeviceAddress vert_buf;
    VkDeviceAddress ind_buf;  
}MeshInfo;


typedef struct {
    ecs_entity_t global;
    ecs_entity_t frame;
    UniformBuffer frame_buf;
    ecs_entity_t cam;
    ecs_entity_t fb;
    UniformBuffer draws_buf;
    MeshInfo *meshes;
    u32 mesh_ind;
    u32 mat_ind;
    u32 tex_ind;
    UniformBuffer lights_buf;
    UniformBuffer materials_buf;
    UniformBuffer scene_buf;
}MainSets;

VK_EXTERN ECS_COMPONENT_DECLARE(VkQueue);
VK_EXTERN ECS_COMPONENT_DECLARE(Buffer);
VK_EXTERN ECS_COMPONENT_DECLARE(Texture);
VK_EXTERN ECS_COMPONENT_DECLARE(VkCommandBuffer);
VK_EXTERN ECS_COMPONENT_DECLARE(VkDevice);
VK_EXTERN ECS_COMPONENT_DECLARE(VkPhysicalDevice);
VK_EXTERN ECS_COMPONENT_DECLARE(VmaAllocator);
VK_EXTERN ECS_COMPONENT_DECLARE(UniformBuffer);
VK_EXTERN ECS_COMPONENT_DECLARE(Pipeline);
VK_EXTERN ECS_COMPONENT_DECLARE(Framebuffer);
VK_EXTERN ECS_COMPONENT_DECLARE(Swapchain);
VK_EXTERN ECS_COMPONENT_DECLARE(Framebuffer);
VK_EXTERN ECS_COMPONENT_DECLARE(RenderPass);
VK_EXTERN ECS_COMPONENT_DECLARE(Shader);
VK_EXTERN ECS_COMPONENT_DECLARE(VkSampler);
VK_EXTERN ECS_COMPONENT_DECLARE(VkDescriptorSet);
VK_EXTERN ECS_COMPONENT_DECLARE(MainSets);
VK_EXTERN ECS_COMPONENT_DECLARE(Mesh);

VK_EXTERN ECS_SYSTEM_DECLARE(delete_textures);
VK_EXTERN ECS_SYSTEM_DECLARE(wait_for_frame);
VK_EXTERN ECS_SYSTEM_DECLARE(begin_new_frame);
VK_EXTERN ECS_SYSTEM_DECLARE(present_frame);
VK_EXTERN ECS_SYSTEM_DECLARE(render_passes);
