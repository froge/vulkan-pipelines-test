FSHADERS=$(wildcard src/shaders/*.frag)
VSHADERS=$(wildcard src/shaders/*.vert)

FS = $(patsubst src/shaders/%.frag, release/shaders/%.frag.spv, $(FSHADERS))
VS = $(patsubst src/shaders/%.vert, release/shaders/%.vert.spv, $(VSHADERS))

# frags: $(FSHADERS)
	# glslc $(shell basename $^) -o $^

release/shaders/%.frag.spv: src/shaders/%.frag
	glslc $< -o $@ -g

release/shaders/%.vert.spv: src/shaders/%.vert
	glslc $< -o $@ -g

all: $(FS) $(VS)
	meson compile -C build
	cp build/vulkan release/bin

run: all
	cd release && LD_LIBRARY_PATH=$LD_LIBRARY_PATH:bin SDL_VIDEODRIVER=wayland ./bin/vulkan

gdb: all
	cd release && LD_LIBRARY_PATH=$LD_LIBRARY_PATH:bin gdb ./bin/vulkan

renderdoc: all
	WAYLAND_DISPLAY=g qrenderdoc
