#version 460

#extension GL_EXT_nonuniform_qualifier : require

layout (location = 0) out vec4 color;

layout (location = 0) in vec2 tex_coord;

layout (set = 3, binding = 0) uniform sampler2D framebuffer[];

// float fxaa_luma(vec3 rgb) {
//     return rgb.g * (0.587/0.299) + rgb.r;
// }

float fxaa_luma(vec3 rgb) {
    return sqrt(0.2126 * rgb.r + 0.7152 * rgb.g + 0.0722 * rgb.b);
}

vec3 pixel_offset(float offset_x, float offset_y) {
    ivec2 pixel_pos = ivec2(gl_FragCoord.xy);
    return texelFetch(framebuffer[4], pixel_pos + ivec2(offset_x, -offset_y), 0).rgb;
    // return pow(texelFetch(framebuffer[4], pixel_pos - ivec2(offset_x, offset_y), 0).rgb, vec3(1.0/2.2));
}

// vec3 pixel_offset(float offset_x, float offset_y) {
//     // ivec2 pixel_pos = ivec2(gl_FragCoord.xy);
//     vec2 pixel_size = vec2(1.0) / textureSize(framebuffer[4], 0).xy;
//     return texture(framebuffer[4], tex_coord + (vec2(offset_x, offset_y) * pixel_size)).rgb;
// }

vec3 pixel_offsetf(vec2 offset) {
    vec2 pixel_pos = gl_FragCoord.xy;
    return texture(framebuffer[4], (pixel_pos + vec2(offset.x, -offset.y)) / textureSize(framebuffer[4], 0)).rgb;
    // return pow(texture(framebuffer[4], (pixel_pos - offset) / textureSize(framebuffer[4], 0)).rgb, vec3(1.0/2.2));
}

void main () {
    color.a = 1.0;
    // color.rgb = vec3(fxaa_luma(pixel_offset(0, 0)));

    vec3 rgbM = pixel_offset( 0,  0);

    float lumaM = fxaa_luma(rgbM);
    float lumaN = fxaa_luma(pixel_offset( 0,  1));
    float lumaE = fxaa_luma(pixel_offset( 1,  0));
    float lumaS = fxaa_luma(pixel_offset( 0, -1));
    float lumaW = fxaa_luma(pixel_offset(-1,  0));

    float lumaHighest = max(max(max(max(lumaM, lumaN), lumaE), lumaS), lumaW);
    float lumaLowest = min(min(min(min(lumaM, lumaN), lumaE), lumaS), lumaW);
    float lumaRange = lumaHighest - lumaLowest;

    float min_range = 0.0625;
    float rel_threshold = 0.166;
    
    if (lumaRange < max(min_range, rel_threshold * lumaHighest)) {
        color.rgb = rgbM;
        // color.rgb = vec3(0.0);
        return;
    }

    float lumaNE = fxaa_luma(pixel_offset( 1,  1));
    float lumaSE = fxaa_luma(pixel_offset( 1, -1));
    float lumaSW = fxaa_luma(pixel_offset(-1, -1));
    float lumaNW = fxaa_luma(pixel_offset(-1,  1));

    float subpixel_factor = 0.75;
    float subpixel_blend = 2.0 * (lumaN + lumaE + lumaS + lumaW);
    subpixel_blend += lumaNE + lumaNW + lumaSE + lumaSW;
    subpixel_blend *= 1.0 / 12.0;
    subpixel_blend = abs(subpixel_blend - lumaM);
    subpixel_blend = clamp(0.0, 1.0, subpixel_blend / lumaRange);
    subpixel_blend = smoothstep(0.0, 1.0, subpixel_blend);
    subpixel_blend *= subpixel_blend * subpixel_factor;

    float horiz =
        2.0 * abs(lumaN + lumaS - 2.0 * lumaM) +
        abs(lumaNE + lumaSE - 2.0 * lumaE) +
        abs(lumaNW + lumaSW - 2.0 * lumaW);
    float vert =
        2.0 * abs(lumaE + lumaW - 2.0 * lumaM) +
        abs(lumaNE + lumaNW - 2.0 * lumaN) +
        abs(lumaSE + lumaSW - 2.0 * lumaS);
    bool is_horizontal = horiz >= vert;

    float pixel_step = 1.0;
    float lumaPos, lumaNeg;
    vec2 edge_step;
    vec2 off;
    if (is_horizontal) {
        lumaPos = lumaN;
        lumaNeg = lumaS;
        edge_step = vec2(1.0, 0.0);
        off = vec2(0.0, 1.0);
    } else {
        lumaPos = lumaE;
        lumaNeg = lumaW;
        edge_step = vec2(0.0, 1.0);
        off = vec2(1.0, 0.0);
    }

    float gradientP = abs(lumaPos - lumaM);
    float gradientN = abs(lumaNeg - lumaM);

    float luma_gradient, other_luma;
    if (gradientP < gradientN) {
        pixel_step = -pixel_step;
        off = -off;
        luma_gradient = gradientN;
        other_luma = lumaNeg;
    } else {
        luma_gradient = gradientP;
        other_luma = lumaPos;
    }

    vec2 init_off = vec2(0.0);

    if (is_horizontal) {
        init_off.y += 0.5 * pixel_step;
    } else {
        init_off.x += 0.5 * pixel_step;
    }

    float edge_luma = 0.5 * (lumaM + other_luma);
    float gradient_threshold = 0.25 * luma_gradient;
    
    vec2 posP = init_off;
    float lumaDeltaP = 0.0;
    bool atEndP = false;
    
    int max_steps = 100;
    for (int i = 0; i < max_steps; i++) {
        posP += edge_step;
        lumaDeltaP = fxaa_luma(pixel_offsetf(posP)) - edge_luma;
        atEndP = abs(lumaDeltaP) >= gradient_threshold;
        if (atEndP) {
            break;
        }
    }

    vec2 posN = init_off;
    float lumaDeltaN = 0.0;
    bool atEndN = false;
    
    for (int i = 0; i < max_steps; i++) {
        posN -= edge_step;
        lumaDeltaN = fxaa_luma(pixel_offsetf(posN)) - edge_luma;
        atEndN = abs(lumaDeltaN) >= gradient_threshold;
        if (atEndN) {
            break;
        }
    }

    float distP;
    float distN;

    if (is_horizontal) {
        distP =  posP.x;
        distN = -posN.x;
    } else {
        distP =  posP.y;
        distN = -posN.y;
    }

    float shortest_dist;
    bool delta_sign;

    if (distN > distP) {
        shortest_dist = distP;
        delta_sign = lumaDeltaP >= 0.0;
    } else {
        shortest_dist = distN;
        delta_sign = lumaDeltaN >= 0.0;
    }

    float blend_factor;
    if (delta_sign == (lumaM - edge_luma >= 0)) {
        blend_factor = 0.0;
    } else {
        blend_factor = 0.5 - shortest_dist / (distP + distN);
    }

    
    color.rgb = pixel_offsetf(off * vec2(max(blend_factor, subpixel_blend)));
    // color.rgb = vec3(shortest_dist / 100.0);
}
