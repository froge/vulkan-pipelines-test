#define WINDOW_H_DECLARE
#include "window.h"

void WindowingImport(ecs_world_t *world) {
  ECS_MODULE(world, Windowing);
  ECS_COMPONENT_DEFINE(world, Window);
}
