#pragma once

#include "flecs.h"
#include "common.h"

#ifdef CONVARS_H_DECLARE
#define CONVARS_EXTERN
#else
#define CONVARS_EXTERN extern
#endif

enum ConVarType {
    convar_command,
    convar_int,
    convar_bool,
    convar_float,
    convar_string,
    convar_enum,
    convar_entity,
};

typedef void CommandFunc(ecs_world_t *world, void *args);

typedef struct {
    enum ConVarType *args;
    CommandFunc *func;
}Command;


typedef struct ConVar ConVar;

typedef void ConVarFunc(ecs_world_t *world, ConVar *val);

struct ConVar {
    enum ConVarType type;
    union {
        Command command;
        int i;
        float f;
        char *s;
        ecs_entity_t e;
    };
    ConVarFunc *set_callback;
    u8 cheat;
    u8 visible;
};

ecs_entity_t add_convar(ecs_world_t *world, char *name, ConVar *convar);
void toggle_convar(ecs_world_t *world, void *data);

void ConvarsImport(ecs_world_t *world);

CONVARS_EXTERN ECS_COMPONENT_DECLARE(ConVar);
