#pragma once

#include "flecs.h"
#include "miniaudio.h"

#ifdef SOUND_H_DECLARE
#define SOUND_EXTERN
#else
#define SOUND_EXTERN extern
#endif

typedef struct {
    ma_sound *sound;
    ecs_entity_t player;
}Sound;

typedef struct {
    ma_sound *sound;
}SoundResource;

void SoundMImport(ecs_world_t *world);

void play_sound(ecs_world_t *world, ecs_entity_t sound_player, ecs_entity_t sound_resource);

ecs_entity_t load_sound(ecs_world_t *world, char *filename);

SOUND_EXTERN ECS_COMPONENT_DECLARE(Sound);
SOUND_EXTERN ECS_COMPONENT_DECLARE(SoundResource);
SOUND_EXTERN ECS_COMPONENT_DECLARE(ma_engine);

SOUND_EXTERN ECS_SYSTEM_DECLARE(set_listener_pos);
SOUND_EXTERN ECS_SYSTEM_DECLARE(set_sound_pos);

SOUND_EXTERN ECS_TAG_DECLARE(SoundPlayer);

SOUND_EXTERN ECS_ENTITY_DECLARE(Plays);
