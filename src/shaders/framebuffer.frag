#version 460

const int thickness = 1;

#extension GL_EXT_nonuniform_qualifier : require

layout (location = 0) out vec4 color;

layout (location = 0) in vec2 tex_coord;

layout (set = 3, binding = 0) uniform sampler2D framebuffer[];

layout (push_constant) uniform pc {
    vec2 screen_size;
    vec3 cam_front;
    bool among;
};

layout (set = 2, binding = 0) uniform ubo0 {
    mat4 proj;
    mat4 view;
    vec3 cam_pos;
    uint object_count;
    uint light_count;
};

void main () {
    ivec2 pos = ivec2(screen_size * tex_coord);

    if (among) {
        // color.rgb = normal_samples[0] + 1.0 * 0.5;
        vec3 pos_samples[4];
        pos_samples[0] = texelFetch(framebuffer[1], ivec2(pos.x - thickness, pos.y - thickness), 0).rgb + cam_pos;
        color.rgb = pos_samples[0] + 1.0 * 0.5;
        color.a = 1.0;
    } else {
        vec3 pos_samples[4];
        pos_samples[0] = texelFetch(framebuffer[1], ivec2(pos.x - thickness, pos.y - thickness), 0).rgb + cam_pos;
        pos_samples[1] = texelFetch(framebuffer[1], ivec2(pos.x + thickness, pos.y + thickness), 0).rgb + cam_pos;
        pos_samples[2] = texelFetch(framebuffer[1], ivec2(pos.x + thickness, pos.y - thickness), 0).rgb + cam_pos;
        pos_samples[3] = texelFetch(framebuffer[1], ivec2(pos.x - thickness, pos.y + thickness), 0).rgb + cam_pos;

        vec3 pos_horiz = pos_samples[1] - pos_samples[0];
        vec3 pos_vert = pos_samples[3] - pos_samples[2];

        float pos_edge = sqrt(dot(pos_horiz, pos_horiz) + dot(pos_vert, pos_vert));

        float pos_threshold = 0.5;
        bool _pos_edge = pos_edge > pos_threshold;

        vec3 normal_samples[4];
        normal_samples[0] = texelFetch(framebuffer[2], ivec2(pos.x - thickness, pos.y - thickness), 0).rgb;
        normal_samples[1] = texelFetch(framebuffer[2], ivec2(pos.x + thickness, pos.y + thickness), 0).rgb;
        normal_samples[2] = texelFetch(framebuffer[2], ivec2(pos.x + thickness, pos.y - thickness), 0).rgb;
        normal_samples[3] = texelFetch(framebuffer[2], ivec2(pos.x - thickness, pos.y + thickness), 0).rgb;

        vec3 normal_horiz = normal_samples[1] - normal_samples[0];
        vec3 normal_vert = normal_samples[3] - normal_samples[2];

        float normal_edge = sqrt(dot(normal_horiz, normal_horiz) + dot(normal_vert, normal_vert));

        float normal_threshold = 0.6;
        bool _normal_edge = normal_edge > normal_threshold;

        // bool edge;
        // edge = max(_normal_edge, _pos_edge);

        if (!_normal_edge && !_pos_edge) {
            discard;
        }
        // edge = pos_edge;

        color.rgb = vec3(0.0);
        color.a = 1.0;
    }
    // color.rgb = vec3(1.0) * edge;
    // color.rgb = normal_samples[0];
    // color.a = 1.0;
}
