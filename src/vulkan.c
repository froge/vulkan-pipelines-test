#include <vulkan/vulkan.h>
#include <flecs.h>
#include <vk_mem_alloc.h>
#include <cglm/call.h>
#include <vulkan/vulkan_core.h>
#include "SDL_timer.h"
#include "SDL_vulkan.h"
#include "cglm/mat4.h"
#include "cglm/struct/affine.h"
#include "cglm/struct/mat4.h"
#include "cglm/struct/vec3.h"
#include "common.h"
#include "SDL2/SDL.h"
#include "miniaudio.h"

#include <ft2build.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "components.h"

#include "phases.h"
#include "physics.h"
#include "convars.h"
#define VULKAN_H_DECLARE
#include "renderer.h"
#include "vulkan.h"
#include "window.h"
#include "ui.h"

struct ubo0 {
    mat4 proj;
    mat4 view;
    vec3 cam_pos;
    u32 object_count;
    u32 light_count;
    u32 among;
};

struct MaterialInfo {
    u32 lit;
    u32 tex;
    u32 metallic_roughness_tex;
    vec3 albedo;
    float metallic;
    float roughness;
};

#include "vulkan_helper.h"

ecs_entity_t new_material(ecs_world_t *world, vec3s albedo, ecs_entity_t texture, ecs_entity_t metallic_roughness_tex, u32 lit, f32 metallic, f32 roughness, char *name) {
    MainSets *sets = ecs_singleton_get_mut(world, MainSets);

    ecs_entity_t mat = ecs_set(world, 0, Material, { 1, .texture = texture, .lit = lit, .metallic = metallic, .roughness = roughness, .index = sets->mat_ind });
    ecs_set_name(world, mat, name);

    struct MaterialInfo *mat_info = sets->materials_buf.mapped;
    mat_info[sets->mat_ind].lit = lit;
    glm_vec3_copy(albedo.raw, mat_info[sets->mat_ind].albedo);
    mat_info[sets->mat_ind].tex = ecs_get(world, texture, Texture)->index;
    mat_info[sets->mat_ind].metallic_roughness_tex = ecs_get(world, metallic_roughness_tex, Texture)->index;
    mat_info[sets->mat_ind].metallic = metallic;
    mat_info[sets->mat_ind].roughness = roughness;

    sets->mat_ind += 1;
    return mat;
}

Texture create_fb_texture_manual(ecs_world_t *world, u32 x, u32 y, struct TextureFormat format) {
    Texture texture = {0};
    texture.format = format;
    texture.index = -1;
    VkDevice dev = *ecs_singleton_get_mut(world, VkDevice);
    VmaAllocator allocator = *ecs_singleton_get_mut(world, VmaAllocator);

    VkImageCreateInfo depth_info = {
        VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .flags = 0,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = format.format,
        .extent = (VkExtent3D) { x, y, 1 },
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = format.sample_count,
        .tiling = VK_IMAGE_TILING_OPTIMAL,
        .usage = format.usage,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
    };

    VmaAllocationCreateInfo depth_alloc_info = {
        .usage = VMA_MEMORY_USAGE_GPU_ONLY,
        .flags = VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT,
    };

    vmaCreateImage(allocator, &depth_info, &depth_alloc_info, &texture.image, &texture.allocation, NULL);

    VkImageViewCreateInfo depth_view_info = {
        VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = texture.image,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = format.format,
        .components = (VkComponentMapping) {
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY, 
            VK_COMPONENT_SWIZZLE_IDENTITY, 
            VK_COMPONENT_SWIZZLE_IDENTITY
        },
        .subresourceRange = (VkImageSubresourceRange) {
            format.aspect,
            0,
            1,
            0,
            1,
        },
    };

    // VkImageView depth_view;
    vkCreateImageView(dev, &depth_view_info, NULL, &texture.view);

    VkImageMemoryBarrier fbtex_barrier = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .srcAccessMask = 0,
        .dstAccessMask = format.access_flags,
        .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .newLayout = format.layout,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = texture.image,
        .subresourceRange = depth_view_info.subresourceRange,
    };

    VkCommandBuffer cmd = *ecs_singleton_get_mut(world, VkCommandBuffer);
    VkQueue queue = *ecs_singleton_get_mut(world, VkQueue);

    VkCommandBufferBeginInfo begin_info = {0};
    begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    VkResult res = vkBeginCommandBuffer(cmd, &begin_info);
    if (res != VK_SUCCESS) {
        printf("command buffer begin failed\n");
        exit(1);
    }

    vkCmdPipelineBarrier(cmd, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, format.stage_flags, 0, 0, NULL, 0, NULL, 1, &fbtex_barrier);

    vkEndCommandBuffer(cmd);

    VkSubmitInfo submit_info2 = {0};
    submit_info2.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info2.waitSemaphoreCount = 0;
    submit_info2.commandBufferCount = 1;
    submit_info2.pCommandBuffers = &cmd;
    submit_info2.signalSemaphoreCount = 0;
    
    vkQueueSubmit(queue, 1, &submit_info2, VK_NULL_HANDLE);

    vkQueueWaitIdle(queue);

    return texture;
}

ecs_entity_t create_fb_texture(ecs_world_t *world, u32 x, u32 y, struct TextureFormat format) {
    Texture texture = create_fb_texture_manual(world, x, y, format);
    ecs_entity_t fort = ecs_set_ptr(world, 0, Texture, &texture);
    return fort;
}

struct Buffer create_vertex_buffer(ecs_world_t *world, void *content, VkDeviceSize size) {
    struct Buffer ret = {0};
    VmaAllocator *allocator = ecs_singleton_get_mut(world, VmaAllocator);

    ret.size = size;

    VkBufferCreateInfo vbuffer_info = {0};
    vbuffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    vbuffer_info.size = size;
    vbuffer_info.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;

    VmaAllocationCreateInfo alloc_info = {0};
    alloc_info.usage = VMA_MEMORY_USAGE_AUTO;
    alloc_info.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT;

    vmaCreateBuffer(*allocator, &vbuffer_info, &alloc_info, &ret.buffer, &ret.allocation, NULL); 

    float *vert_mapped;
    vmaMapMemory(*allocator, ret.allocation, (void**)&vert_mapped);

    memcpy(vert_mapped, content, size);

    vmaFlushAllocation(*allocator, ret.allocation, 0, 0);

    vmaUnmapMemory(*allocator, ret.allocation);

    return ret;
}

Buffer create_index_buffer(ecs_world_t *world, void *content, VkDeviceSize size) {
    Buffer ret = {0};
    VmaAllocator *allocator = ecs_singleton_get_mut(world, VmaAllocator);

    ret.size = size;

    VkBufferCreateInfo vbuffer_info = {0};
    vbuffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    vbuffer_info.size = size;
    vbuffer_info.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;

    VmaAllocationCreateInfo alloc_info = {0};
    alloc_info.usage = VMA_MEMORY_USAGE_AUTO;
    alloc_info.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT;

    vmaCreateBuffer(*allocator, &vbuffer_info, &alloc_info, &ret.buffer, &ret.allocation, NULL); 

    float *vert_mapped;
    vmaMapMemory(*allocator, ret.allocation, (void**)&vert_mapped);

    memcpy(vert_mapped, content, size);

    vmaFlushAllocation(*allocator, ret.allocation, 0, 0);

    vmaUnmapMemory(*allocator, ret.allocation);

    return ret;
}

void destroy_buffer(ecs_world_t *world, struct Buffer *buf) {
    VmaAllocator allocator = *ecs_singleton_get_mut(world, VmaAllocator);
    vmaDestroyBuffer(allocator, buf->buffer, buf->allocation);
}


void descriptor_load_image(ecs_world_t *world, ecs_entity_t image, VkSampler sampler, VkDescriptorSet descriptor_set, u32 index) {
    VkDevice dev = *ecs_singleton_get_mut(world, VkDevice);

    Texture *img = ecs_get_mut(world, image, Texture);
    img->index = index;
    img->sampler = sampler;
    VkDescriptorImageInfo descriptor_image_info = {0};
    descriptor_image_info.sampler = sampler;
    descriptor_image_info.imageView = img->view;
    VkImageLayout layout = img->format.layout;
    if (layout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
        layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
    } else if (layout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL) {
        layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    }
    descriptor_image_info.imageLayout = layout;
    
    VkWriteDescriptorSet write_ds = {0};
    write_ds.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write_ds.dstSet = descriptor_set;
    write_ds.dstBinding = 0;
    write_ds.dstArrayElement = index;
    write_ds.descriptorCount = 1;
    write_ds.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    write_ds.pImageInfo = &descriptor_image_info;

    vkUpdateDescriptorSets(dev, 1, &write_ds, 0, NULL);

    ecs_singleton_modified(world, VkDevice);
}

ecs_entity_t load_texture(ecs_world_t *world, char *filename) {
    int nr_channels;
    int width, height;
    void *data = stbi_load(filename, &width, &height, &nr_channels, 0);
    if (!data) {
        printf("texture %s failed to load\n", filename);
        exit(1);
    }
    ecs_entity_t ent = load_texture2d_from_data(world, data, width, height, nr_channels);
    ecs_set_name(world, ent, filename);
    stbi_image_free(data);
    return ent;
}

ecs_entity_t load_texture2d_from_data(ecs_world_t *world, void *data, int width, int height, int nr_channels) {
    VkFormat format;
    switch (nr_channels) {
        case 1:
            format = VK_FORMAT_R8_SRGB;
            break;
        case 2:
            format = VK_FORMAT_R8G8_SRGB;
            break;
        case 3:
            format = VK_FORMAT_R8G8B8_SRGB;
            break;
        case 4:
            format = VK_FORMAT_R8G8B8A8_SRGB;
            break;
    }
    struct TextureConfig tex = {
        width,
        height,
        1,
        format,
        1,
        VK_IMAGE_VIEW_TYPE_2D,
    };
    return load_texture_from_data(world, data, &tex);
}

ecs_entity_t load_texture_from_data(ecs_world_t *world, void *data, struct TextureConfig *conf) {
    VkCommandBuffer cmd = *ecs_singleton_get_mut(world, VkCommandBuffer);
    VkQueue queue = *ecs_singleton_get_mut(world, VkQueue);
    VkDevice dev = *ecs_singleton_get_mut(world, VkDevice);
    VmaAllocator allocator = *ecs_singleton_get_mut(world, VmaAllocator);

    VkImageMemoryBarrier texture_write_barrier = {0};
    texture_write_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    texture_write_barrier.srcAccessMask = 0;
    texture_write_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    texture_write_barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    texture_write_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    texture_write_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    texture_write_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    texture_write_barrier.subresourceRange = (VkImageSubresourceRange) {
        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .baseMipLevel = 0,
        .levelCount = 1,
        .baseArrayLayer = 0,
        .layerCount = 1 
    };

    VkImageMemoryBarrier texture_read_barrier = texture_write_barrier;
    texture_read_barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    texture_read_barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
    texture_read_barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    texture_read_barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VmaAllocationCreateInfo alloc_info = {0};
    alloc_info.usage = VMA_MEMORY_USAGE_AUTO;
    alloc_info.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT;

    VkCommandBufferBeginInfo begin_info = {0};
    begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    u64 size = conf->width * conf->height * conf->depth * size_from_format(conf->format);

    VkBufferCreateInfo staging_info = {0};
    staging_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    staging_info.size = size;
    staging_info.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;

    VkBuffer staging_buffer;
    VmaAllocation staging_allocation;
    vmaCreateBuffer(allocator, &staging_info, &alloc_info, &staging_buffer, &staging_allocation, NULL);

    void *staging_mapped;
    vmaMapMemory(allocator, staging_allocation, &staging_mapped);
    memcpy(staging_mapped, data, size);
    vmaFlushAllocation(allocator, staging_allocation, 0, 0);
    vmaUnmapMemory(allocator, staging_allocation);

    if (!conf->width || !conf->height || !conf->depth) {
        printf("invalid width, height or depth specified\n");
        return 0;
    }

    VkImageType img_type;
    switch (conf->img_type) {
        case VK_IMAGE_VIEW_TYPE_1D:
            img_type = VK_IMAGE_TYPE_1D;
            break;
        case VK_IMAGE_VIEW_TYPE_1D_ARRAY:
            img_type = VK_IMAGE_TYPE_1D;
            break;
        case VK_IMAGE_VIEW_TYPE_2D:
            img_type = VK_IMAGE_TYPE_2D;
            break;
        case VK_IMAGE_VIEW_TYPE_2D_ARRAY:
            img_type = VK_IMAGE_TYPE_2D;
            break;
        case VK_IMAGE_VIEW_TYPE_CUBE:
            img_type = VK_IMAGE_TYPE_2D;
            break;
        case VK_IMAGE_VIEW_TYPE_CUBE_ARRAY:
            img_type = VK_IMAGE_TYPE_2D;
            break;
        case VK_IMAGE_VIEW_TYPE_3D:
            img_type = VK_IMAGE_TYPE_3D;
            break;
    }
    

    VkImageCreateInfo image_info = {0};
    image_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    image_info.flags = 0;
    image_info.imageType = img_type;
    image_info.format = conf->format;
    image_info.extent = (VkExtent3D){conf->width, conf->height, conf->depth};
    image_info.mipLevels = 1;
    image_info.arrayLayers = 1;
    image_info.samples = VK_SAMPLE_COUNT_1_BIT;
    image_info.tiling = VK_IMAGE_TILING_OPTIMAL;
    image_info.usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    image_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    image_info.queueFamilyIndexCount = 0;
    image_info.pQueueFamilyIndices = NULL;
    image_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    VmaAllocationCreateInfo image_alloc_info = {0};
    image_alloc_info.usage = VMA_MEMORY_USAGE_GPU_ONLY;
   
    VkImage image;
    VmaAllocation image_allocation;
    vmaCreateImage(allocator, &image_info, &image_alloc_info, &image, &image_allocation, NULL);
    
    VkResult res;
    res = vkBeginCommandBuffer(cmd, &begin_info);
    if (res != VK_SUCCESS) {
        printf("command buffer begin failed\n");
        exit(1);
    }

    texture_write_barrier.image = image;
    texture_read_barrier.image = image;

    vkCmdPipelineBarrier(cmd, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 0, NULL, 1, &texture_write_barrier);

    VkImageSubresourceLayers image_subresource;
    image_subresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    image_subresource.mipLevel = 0;
    image_subresource.baseArrayLayer = 0;
    image_subresource.layerCount = 1;

    VkBufferImageCopy region = {0};
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;
    region.imageSubresource = image_subresource;
    region.imageOffset = (VkOffset3D){0, 0, 0};
    region.imageExtent = (VkExtent3D){conf->width, conf->height, conf->depth};
    
    vkCmdCopyBufferToImage(cmd, staging_buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

    vkCmdPipelineBarrier(cmd, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT, 0, 0, NULL, 0, NULL, 1, &texture_read_barrier);

    vkEndCommandBuffer(cmd);

    VkSubmitInfo submit_info2 = {0};
    submit_info2.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info2.waitSemaphoreCount = 0;
    submit_info2.commandBufferCount = 1;
    submit_info2.pCommandBuffers = &cmd;
    submit_info2.signalSemaphoreCount = 0;
    
    vkQueueSubmit(queue, 1, &submit_info2, VK_NULL_HANDLE);

    vkQueueWaitIdle(queue);

    vmaDestroyBuffer(allocator, staging_buffer, staging_allocation);

    VkImageViewCreateInfo image_view_info = {0};
    image_view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    image_view_info.image = image;
    image_view_info.viewType = conf->img_type;
    image_view_info.format = conf->format;
    image_view_info.components = (VkComponentMapping){VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, VK_COMPONENT_SWIZZLE_IDENTITY, 
VK_COMPONENT_SWIZZLE_IDENTITY};
    image_view_info.subresourceRange = (VkImageSubresourceRange){
        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .baseArrayLayer = 0,
        .baseMipLevel = 0,
        .layerCount = 1,
        .levelCount = 1,
    };

    VkImageView image_view;
    vkCreateImageView(dev, &image_view_info, NULL, &image_view);

    struct Texture ret;
    ret.image = image;
    ret.view = image_view;
    ret.allocation = image_allocation;
    ret.width = conf->width;
    ret.height = conf->height;
    ret.depth = conf->depth;
    ret.format.format = conf->format;
    ret.format.layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    ecs_entity_t image_e = ecs_set_ptr(world, 0, Texture, &ret);

    return image_e;
}

void delete_texture(VmaAllocator allocator, VkDevice dev, Texture *texture) {
    vmaDestroyImage(allocator, texture->image, texture->allocation);
    vkDestroyImageView(dev, texture->view, NULL);
}

void delete_textures(ecs_iter_t *it) {
    Texture *textures = ecs_field(it, Texture, 1);
    VmaAllocator allocator = *ecs_singleton_get_mut(it->world, VmaAllocator);
    VkDevice dev = *ecs_singleton_get_mut(it->world, VkDevice);

    for (int i = 0; i < it->count; i++) {
        Texture *texture = &textures[i];
        delete_texture(allocator, dev, texture);
    }
}

ecs_entity_t load_shader(ecs_world_t *world, char *filename, VkShaderStageFlags stage) {
    VkDevice dev = *ecs_singleton_get_mut(world, VkDevice);
    ecs_entity_t shader = ecs_lookup(world, filename);

    if (!shader) {
        FILE *file = fopen(filename, "rb");
        u64 len;
        fseek(file, 0, SEEK_END);
        len = ftell(file);
        rewind(file);
        char *shader_bin = malloc(len);
        fread(shader_bin, len, 1, file);
        fclose(file);

        VkShaderModuleCreateInfo mod_info = {
            VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
            .codeSize = len,
            .pCode = (u32*)shader_bin,
        };

        VkShaderModule mod;
        vkCreateShaderModule(dev, &mod_info, NULL, &mod);

        ecs_singleton_modified(world, VkDevice);

        shader = ecs_set(world, 0, Shader, { mod, stage });
        ecs_set_name(world, shader, filename);
    }
    return shader;
}

void render_background(ecs_iter_t *it, VkCommandBuffer cmd, const Pipeline *pipeline, const RenderPass *pass, u32 index) {
    ecs_world_t *world = it->world;
    // Mesh *meshes = ecs_field(it, Mesh, 1);
    Matrix4 *transforms = ecs_field(it, Matrix4, 1);
    for (int i = 0; i < it->count; i++) {
        ecs_entity_t entity = it->entities[i];
        // Mesh *mesh = &meshes[i];
        Matrix4 *transform = &transforms[i];
        // const Buffer *vert_buf = ecs_get(world, mesh->vert_buffer, Buffer);

        // vkCmdBindVertexBuffers(cmd, 0, 1, &vert_buf->buffer, &(VkDeviceSize){0});
        // vkCmdPushConstants(cmd, pipeline->layout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(mat4), transform->raw);
        // vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->layout, 0, 1, &pipeline->descriptor_set, 0, NULL);
        vkCmdDraw(cmd, 6, 1, 0, 0);
    }
}

void render_text(ecs_iter_t *it, VkCommandBuffer cmd, const Pipeline *pipeline, const RenderPass *pass, u32 index) {
    ecs_world_t *world = it->world;
    const Window *window = ecs_singleton_get(world, Window);
    const Framebuffer *buf = ecs_get(world, window->framebuffer, Framebuffer);
    // mat4s proj = glms_ortho(0.0, buf->x, buf->y, 0.0, 0.0, 1.0);

    for (int i = 0; i < it->count; i++) {
        ecs_entity_t entity = it->entities[i];
        const Transform2D *tr = ecs_get(world, entity, Transform2D);
        const Label *label = ecs_get(world, entity, Label);
        const AnchorBits *anchor = ecs_get(world, entity, AnchorBits);

        // vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->layout, 0, 1, &pipeline->descriptor_set, 0, NULL);

        const Glyphs *glyphs = ecs_singleton_get(world, Glyphs);

        int pen_x = 0;
        int pen_y = 64;
        int len = strlen(label->text);

        float scale = label->pts / 64;
        Vector2 size = {{ 0, 0 }};
        for (int i = 0; i < len; i++) {
            Glyph *glyph = &glyphs->glyphs[(int)label->text[i] - 32];
            size.x += (glyph->advance >> 6) * scale;
        }
        size.y = 48;

        vec2s offset = get_anchor_offset(size, *anchor);
        
        for (int i = 0; i < len; i++) {
            Glyph *glyph = &glyphs->glyphs[(int)label->text[i] - 32];
            if (!glyph->texture) {
                pen_x += (glyph->advance >> 6) * scale;
                continue;
            }

            const Texture *tex = ecs_get(world, glyph->texture, Texture);

            float pixel_x = 1.0 / (float)buf->x;
            float pixel_y = 1.0 / (float)buf->y;

            // float scale = label->pts / 64;
            // float scale = 1.0;

            vec3s test;
            test.x = tex->width  * pixel_x * scale;
            test.y = tex->height * pixel_y * scale;
            test.z = 1;

            float rx = pen_x + glyph->bearing.x;
            float ry = pen_y - glyph->bearing.y / 2.0;

            rx *= scale;
            ry *= scale;

            rx += floor(offset.x) + floor(tr->pos.x * (float)buf->x);
            ry += floor(offset.y) + floor(tr->pos.y * (float)buf->y);

            // printf("%f, %f\n", test.x, test.y);

            mat4s mat = glms_translate_make((vec3s) {{ ((float)rx * pixel_x) * 2.0 - 1.0, 1.0 - ((float)ry * pixel_y) * 2.0, 0.0 }});
            // mat4s ortho = glms_ortho(0, buf->x, buf->y, 0, -1.0, 1.0);
            // mat4s mat = glms_translate_make((vec3s) {{ (float)rx, (float)ry, 0.0 }});

            mat = glms_scale(mat, test);

            // vkCmdPushConstants(cmd, pipeline->layout, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(mat4), ortho.raw);
            vkCmdPushConstants(cmd, pipeline->layout, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, sizeof(mat4), sizeof(mat4), mat.raw);

            int index = tex->index;
            vkCmdPushConstants(cmd, pipeline->layout, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, sizeof(mat4) + sizeof(mat4), sizeof(u32), &index);

            pen_x += (glyph->advance >> 6) * scale;
            vkCmdDraw(cmd, 6, 1, 0, 0);
        }
    }
}

void render_main(ecs_iter_t *it, VkCommandBuffer cmd, const Pipeline *pipeline, const RenderPass *pass, u32 index) {
    ecs_world_t *world = it->world;
    // Matrix4 *transforms = ecs_field(it, Matrix4, 1);
    // Render3D *render3ds = ecs_field(it, Render3D, 2);
    const MainSets *mainsets = ecs_singleton_get(world, MainSets);

    // for (int i = 0; i < it->count; i++) {
    //     ecs_entity_t entity = it->entities[i];
    //     Mesh *mesh = ecs_get(world, render3ds->mesh, Mesh);
    //     Matrix4 *transform = &transforms[i];
    //     const Render3D *r3d = &render3ds[i];
    //     const Material *mat = ecs_get(world, r3d->mat, Material);

    //     // if (ecs_has(world, entity, RenderOutline)) {
    //     //     vkCmdSetStencilWriteMask(cmd, VK_STENCIL_FACE_FRONT_BIT, 0xff);
    //     // } else {
    //     //     vkCmdSetStencilWriteMask(cmd, VK_STENCIL_FACE_FRONT_BIT, 0x00);
    //     // }

    //     vkCmdBindVertexBuffers(cmd, 0, 1, &mesh->vert_buf.buffer, &(VkDeviceSize){0});
    //     u32 ie = index + i;

    //     struct FragmentConstants {
    //         vec3 color;
    //         u32 source;
    //         u32 index;
    //     };

    //     struct FragmentConstants constants = {};
    //     constants.source = mat->color_source;
    //     glm_vec3_copy((float*)mat->albedo.raw, constants.color);
    //     constants.index = ecs_get(world, mat->texture, Texture)->index;

    // vkCmdPushConstants(cmd, pipeline->layout, VK_SHADER_STAGE_FRAGMENT_BIT, 0, sizeof(u32), &among->i);
    //     vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->layout, 0, 1, &pipeline->descriptor_set, 0, NULL);
    //     vkCmdDraw(cmd, mesh->verts, 1, 0, ie);
    // }
    vkCmdDrawIndirect(cmd, mainsets->draws_buf.buffer, 0, index, sizeof(VkDrawIndirectCommand));
}

void wait_for_frame(ecs_iter_t *it) {
    Swapchain *chudchain = ecs_singleton_get_mut(it->world, Swapchain);
    VkDevice dev = *ecs_singleton_get(it->world, VkDevice);
    vkWaitForFences(dev, 1, &chudchain->render_done, VK_TRUE, (i64)-1);
    VkResult result = vkAcquireNextImageKHR(dev, chudchain->swapchain, (i64)-1, chudchain->image_ready, VK_NULL_HANDLE, &chudchain->cur_swap_image);
    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
        puts("chud");
        ecs_world_t *world = it->world;
        Window *window = ecs_singleton_get_mut(world, Window);
        VkPhysicalDevice physdev = *ecs_singleton_get_mut(world, VkPhysicalDevice);
        VkSurfaceKHR surf = chudchain->info.surface;

        VkSurfaceCapabilitiesKHR caps;

        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physdev, surf, &caps);

        int x = caps.currentExtent.width;
        int y = caps.currentExtent.height;

        if (caps.currentExtent.width == -1) {
            SDL_Vulkan_GetDrawableSize(window->window, &x, &y);
        }

        ecs_entity_t vsync_c = ecs_lookup(world, "vsync");
        const ConVar *vsync = ecs_get(world, vsync_c, ConVar);

        recreate_swapchain(world, x, y, vsync->i);

        ecs_modified(world, window->framebuffer, Framebuffer);
    }
    vkResetFences(dev, 1, &chudchain->render_done);
    ecs_singleton_modified(it->world, Swapchain);
}

void present_frame(ecs_iter_t *it) {
    Swapchain *swapchain = ecs_singleton_get_mut(it->world, Swapchain);
    // Window *window = ecs_singleton_get_mut(it->world, Window);
    VkCommandBuffer cmd = *ecs_singleton_get_mut(it->world, VkCommandBuffer);
    VkQueue queue = *ecs_singleton_get(it->world, VkQueue);

    VkSubmitInfo submit_info = {0};
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.waitSemaphoreCount = 1;
    submit_info.pWaitSemaphores = &swapchain->image_ready;
    submit_info.pWaitDstStageMask = (VkPipelineStageFlags[]){ VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
    submit_info.signalSemaphoreCount = 1;
    submit_info.pSignalSemaphores = &swapchain->render_done_s;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &cmd;

    VkPresentInfoKHR present_info = {0};
    present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    present_info.waitSemaphoreCount = 1;
    present_info.pWaitSemaphores = &swapchain->render_done_s;
    present_info.swapchainCount = 1;
    present_info.pSwapchains = &swapchain->swapchain;
    present_info.pImageIndices = &swapchain->cur_swap_image;
    present_info.pResults = NULL;

    u64 chud = SDL_GetPerformanceCounter();
    vkEndCommandBuffer(cmd);
    vkQueueSubmit(queue, 1, &submit_info, swapchain->render_done);
    VkResult result = vkQueuePresentKHR(queue, &present_info);
    // printf("rendering took %lf ms\n", ((double)SDL_GetPerformanceCounter() - (double)chud) / SDL_GetPerformanceFrequency() * 1000.0);
    ecs_entity_t mousee = ecs_lookup(it->world, "resized");
    ConVar *mouse = ecs_get_mut(it->world, mousee, ConVar);
    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || mouse->i) {
        // puts("forntite");
        mouse->i = 0;
        ecs_world_t *world = it->world;
        Swapchain *chudchain = ecs_singleton_get_mut(it->world, Swapchain);
        VkDevice dev = *ecs_singleton_get(it->world, VkDevice);
        Window *window = ecs_singleton_get_mut(world, Window);
        VkPhysicalDevice physdev = *ecs_singleton_get_mut(world, VkPhysicalDevice);
        VmaAllocator allocator = *ecs_singleton_get_mut(world, VmaAllocator);
        VkSurfaceKHR surf = chudchain->info.surface;

        VkSurfaceCapabilitiesKHR caps;

        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physdev, surf, &caps);

        int x = caps.currentExtent.width;
        int y = caps.currentExtent.height;

        if (caps.currentExtent.width == -1) {
            SDL_Vulkan_GetDrawableSize(window->window, &x, &y);
        }

        Framebuffer *framebuffer = ecs_get_mut(world, window->framebuffer, Framebuffer);
        Texture *depth = ecs_get_mut(world, chudchain->depth, Texture);

        ecs_entity_t vsync_c = ecs_lookup(world, "vsync");
        const ConVar *vsync = ecs_get(world, vsync_c, ConVar);
        
        recreate_swapchain(world, x, y, vsync->i);

        ecs_modified(world, window->framebuffer, Framebuffer);
    }
}

void begin_new_frame(ecs_iter_t *it) {
    Swapchain *swapchain = ecs_singleton_get_mut(it->world, Swapchain);
    Window *window = ecs_singleton_get_mut(it->world, Window);
    VkCommandBuffer cmd = *ecs_singleton_get_mut(it->world, VkCommandBuffer);

    wait_for_frame(it);

    VkCommandBufferBeginInfo begin_info = {0};
    begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    VkResult res = vkBeginCommandBuffer(cmd, &begin_info);
    if (res != VK_SUCCESS) {
        printf("command buffer could not begin: %i\n", res);
        exit(1);
    }
}

// void set_positions(ecs_iter_t *it) {
//     // it->term_index 
//     const MainSets *mainsets = ecs_singleton_get(it->world, MainSets);
//     // if (it->entities[0] == ecs_iter_first(it)) {
//     //     it->ctx = 0;
//     // }

//     Matrix4 *matrices = ecs_field(it, Matrix4, 2);
//     for (int i = 0; i < it->count; i++) {
//         glm_mat4_copy(matrices[i].raw, cur->model);
//         // glm_mat4_pick3(glms_mat4_transpose(glms_mat4_inv_fast(matrices[i])).raw, cur->inv_model);
//         it->ctx += 1;
//     }
// }
void render_passes(ecs_iter_t *it) {
    ecs_world_t *world = it->world;

    RenderPass *passes = ecs_field(it, RenderPass, 1);
    // RenderPass *parents = ecs_field(&it, RenderPass, 2);

    MainSets *mainsets = ecs_singleton_get_mut(world, MainSets);

    const VkDescriptorSet *texture_set = ecs_get(world, mainsets->global, VkDescriptorSet);
    const VkDescriptorSet *frame_set = ecs_get(world, mainsets->frame, VkDescriptorSet);
    const VkDescriptorSet *camera_set = ecs_get(world, mainsets->cam, VkDescriptorSet);
    const VkDescriptorSet *fb_set = ecs_get(world, mainsets->fb, VkDescriptorSet);

    ecs_query_t **queries = it->ctx;

    ecs_query_t *main_pass_query = queries[0];
    ecs_query_t *lights_query = queries[1];

    struct ObjectInfo {
        mat4 model;
        mat3 inv_model;
        u32 mesh;
        u32 mat;
    };

    // printf("%i, %i, %i\n", sizeof(struct ObjectInfo), offsetof(struct ObjectInfo, mesh), offsetof(struct ObjectInfo, mat));

    // u32 count = 0;
        // count += iter.count;
    u32 index = 0;
    ecs_iter_t iter = ecs_query_iter(world, main_pass_query);
    while (ecs_iter_next(&iter)) {
        index += iter.count;
    }

    const VmaAllocator allocator = *ecs_singleton_get(world, VmaAllocator);
    if (index >= mainsets->frame_buf.max - 1) {
        printf("resized\n");
        vmaDestroyBuffer(allocator, mainsets->frame_buf.buffer, mainsets->frame_buf.allocation);
        vmaDestroyBuffer(allocator, mainsets->draws_buf.buffer, mainsets->draws_buf.allocation);
        mainsets->frame_buf.max += 100;
        VmaAllocationCreateInfo alloc_info = {0};
        alloc_info.usage = VMA_MEMORY_USAGE_AUTO;
        alloc_info.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT | VMA_ALLOCATION_CREATE_MAPPED_BIT;

        VkBufferCreateInfo draws_buffer_info = {0};
        draws_buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        draws_buffer_info.size = sizeof(VkDrawIndirectCommand) * mainsets->frame_buf.max;
        draws_buffer_info.usage = VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT;

        VkBufferCreateInfo objects_buffer_info = {0};
        objects_buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        objects_buffer_info.size = sizeof(struct ObjectInfo) * mainsets->frame_buf.max;
        objects_buffer_info.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

        vmaCreateBuffer(allocator, &draws_buffer_info, &alloc_info, &mainsets->draws_buf.buffer, &mainsets->draws_buf.allocation, &mainsets->draws_buf.info);
        vmaCreateBuffer(allocator, &objects_buffer_info, &alloc_info, &mainsets->frame_buf.buffer, &mainsets->frame_buf.allocation, &mainsets->frame_buf.info);
        // mainsets->frame_buf.mapped = mainsets->frame_buf.info.pMappedData + mainsets->frame_buf.info.offset;

        VkDescriptorBufferInfo buf_info = {0};
        buf_info.buffer = mainsets->frame_buf.buffer;
        buf_info.offset = 0;
        buf_info.range = VK_WHOLE_SIZE;

        VkWriteDescriptorSet write_ds = {0};
        write_ds.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write_ds.dstSet = *frame_set;
        write_ds.dstBinding = 0;
        write_ds.dstArrayElement = 0;
        write_ds.descriptorCount = 1;
        write_ds.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        write_ds.pBufferInfo = &buf_info;

        VkDevice dev = *ecs_singleton_get_mut(world, VkDevice);
        vkUpdateDescriptorSets(dev, 1, &write_ds, 0, NULL);
    }

    // ConVar *chud = ecs_get(world, ecs_lookup(world, "among"), ConVar);
    // if (!chud->i) {
    index = 0;
    iter = ecs_query_iter(world, main_pass_query);
    struct ObjectInfo *info = mainsets->frame_buf.info.pMappedData;
    VkDrawIndirectCommand *commands = mainsets->draws_buf.info.pMappedData;
    while (ecs_iter_next(&iter)) {
        Matrix4 *matrices = ecs_field(&iter, Matrix4, 1);
        Render3D *renders = ecs_field(&iter, Render3D, 2);
        for (int i = 0; i < iter.count; i++) {
            fort:;
            struct ObjectInfo *cur = &info[index];
            VkDrawIndirectCommand *ccom = &commands[index];
            if (!renders[i].visible) {
                i += 1;
                if (i < iter.count) {
                    goto fort;
                } else {
                    continue;
                }
            }
            const Mesh *mesh = ecs_get(world, renders[i].mesh, Mesh);
            const Material *mat = ecs_get(world, renders[i].mat, Material);
            glm_mat4_copy(matrices[i].raw, cur->model);
            mat4s inv = glms_mat4_transpose(glms_mat4_inv(matrices[i]));
            glm_mat4_pick3(inv.raw, cur->inv_model);
            cur->mesh = mesh->ind;
            // if (mat->texture) {
            //     const Texture *tex = ecs_get(world, mat->texture, Texture);
            //     cur->mat = tex->index;
            // } else {
            //     cur->mat = 0;
            // }
            cur->mat = mat->index;
            ccom->vertexCount = mesh->verts;
            ccom->instanceCount = 1;
            ccom->firstVertex = 0;
            ccom->firstInstance = index;
            index += 1;
        }
    }

    vmaFlushAllocations(allocator, 2, (VmaAllocation[]){mainsets->draws_buf.allocation, mainsets->frame_buf.allocation}, NULL, NULL);

    u32 blocks_count = index;

    index = 0;
    iter = ecs_query_iter(world, lights_query);
    while (ecs_iter_next(&iter)) {
        for (int i = 0; i < iter.count; i++) {
            index += 1;
        }
    }

    struct LightInfo {
        vec3 pos;
        vec3 color;
        vec3 dir;
        u32 type;
        u32 shadowed;
    };

    index = 0;
    iter = ecs_query_iter(world, lights_query);
    struct LightInfo *lights_mapped = mainsets->lights_buf.mapped;
    while (ecs_iter_next(&iter)) {
        Transform *poss = ecs_field(&iter, WorldTransform, 1);
        Light *lights = ecs_field(&iter, Light, 2);
        for (int i = 0; i < iter.count; i++) {
            struct LightInfo *light_mapped = &lights_mapped[index];
            Transform *pos = &poss[i];
            Light *light = &lights[i];
            glm_vec3_copy(pos->pos.raw, light_mapped->pos);
            glm_vec3_scale(light->color, light->strength, light_mapped->color);
            glm_vec3_copy(light->dir, light_mapped->dir);
            // light_mapped->strength = light->strength;
            light_mapped->type = light->type;
            light_mapped->shadowed = light->shadowed;
            index += 1;
        }
    }

    // printf("%i\n", index);
    u32 lights_count = index;

    vmaFlushAllocation(allocator, mainsets->lights_buf.allocation, NULL, NULL); // i only need to submit when new lights exist
    
    const Swapchain *swapchain = ecs_singleton_get(world, Swapchain);
    VkCommandBuffer cmd = *ecs_singleton_get_mut(world, VkCommandBuffer);
    for (int i =  0; i < it->count; i++) {
        RenderPass *pass = &passes[i];
        const Pipeline *pipeline = ecs_get(world, pass->pipeline, Pipeline);
        Framebuffer *framebuffer;
        if (pass->framebuffer) {
            framebuffer = ecs_get_mut(world, pass->framebuffer, Framebuffer);
        } else {
            framebuffer = ecs_get_mut(world, swapchain->framebuffer, Framebuffer);
        }
        vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->layout, 0, 1, texture_set, 0, NULL);
        vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->layout, 1, 1, frame_set, 0, NULL);
        vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->layout, 2, 1, camera_set, 0, NULL);
        vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->layout, 3, 1, fb_set, 0, NULL);

        const ConVar *among = ecs_get(world, ecs_lookup(world, "among"), ConVar);
        struct ubo0 uniform = {0};
        glm_mat4_identity(uniform.proj);
        glm_perspective(glm_rad(90.0), (float)framebuffer->x / (float)framebuffer->y, 0.01, 100.0, uniform.proj);
        mat4 reverse_y = {
            { 1.0f, 0.0f, 0.0f, 0.0f },
            { 0.0f, 1.0f, 0.0f, 0.0f },
            { 0.0f, 0.0f, 1.0f, 0.0f },
            { 0.0f, 0.0f, 0.0f, 1.0 },
        };
        // glm_mat4_mul(uniform.proj, reverse_y, uniform.proj);
        cam_view(world, &uniform.view);
        WorldTransform *pos = ecs_get(world, ecs_id(Camera), WorldTransform);
        glm_vec3_copy(pos->pos.raw, uniform.cam_pos);
        uniform.object_count = blocks_count;
        uniform.light_count = lights_count;
        uniform.among = among->i;

        memcpy(pipeline->uniform->mapped, &uniform, sizeof(struct ubo0));

        vmaFlushAllocation(allocator, pipeline->uniform->allocation, 0, 0);

        // VkSampler sampler = *ecs_singleton_get(world, VkSampler);
        // for (int y = 0; y < pass->dependencyc; y++) {
        //     descriptor_load_image(world, pass->dependencies[y].tex, sampler, pipeline->descriptor_set, pass->dependencies[y].index);
        // }

        VkViewport viewport = {0};
        viewport.x = 0;
        viewport.y = framebuffer->y;
        viewport.width = framebuffer->x;
        viewport.height = -(float)framebuffer->y;
        viewport.minDepth = 0.0;
        viewport.maxDepth = 1.0;

        VkRect2D scissor = {0};
        scissor.offset = (VkOffset2D){0, 0};
        scissor.extent = (VkExtent2D){framebuffer->x, framebuffer->y};

        vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline->pipeline);

        vkCmdSetViewport(cmd, 0, 1, &viewport);
        vkCmdSetScissor(cmd, 0, 1, &scissor);

        // VkClearValue val = {
        //     .color = { .float32 = { 0, 0, 0, 0 } },
        //     // .depthStencil = { .depth = 1.0, .stencil = 0x00 },  
        // };

        // VkClearValue depth = {
        //     .depthStencil = { .depth = 1.0, .stencil = 0x00 },  
        // };

        VkRenderPassBeginInfo info = {
            .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
            .pNext = NULL,
            .renderPass = pass->pass, 
            .framebuffer = *framebuffer->fbo,
            .renderArea = scissor,
            .clearValueCount = 0,
            .pClearValues = 0,
        };

        if (!pass->framebuffer) {
            info.framebuffer = framebuffer->fbo[swapchain->cur_swap_image];
        }
        if (pass->clears) {
            info.clearValueCount = framebuffer->attachment_count + (framebuffer->depth_stencil ? 1 : 0);
            info.pClearValues = pass->clears;
        }
        // if (framebuffer->depth_stencil) {
            // ((VkClearValue*)info.pClearValues)[framebuffer->attachment_count] = depth;
        // }

        vkCmdBeginRenderPass(cmd, &info, VK_SUBPASS_CONTENTS_INLINE);

        u64 old = SDL_GetPerformanceCounter();
        if (pass->indirect) {
            ecs_iter_t iter;
            iter.world = world;
            pass->callback(&iter, cmd, pipeline, pass, blocks_count);
        } else {
            ecs_iter_t iter = ecs_query_iter(world, pass->query);
            iter.param = (void*)pipeline;
            iter.ctx = (void*)pass;
            u32 index = 0;
            while (ecs_iter_next(&iter)) {
                pass->callback(&iter, cmd, pipeline, pass, index);
                index += iter.count;
            }
        }
        u64 delta = SDL_GetPerformanceCounter() - old;
        double _delta = (double)delta / (double)SDL_GetPerformanceFrequency();
        // printf("%s: %lfms\n", ecs_get_name(world, it->entities[i]), _delta * 1000.0);

        vkCmdEndRenderPass(cmd);
    }
    ecs_singleton_modified(world, VkCommandBuffer);
}

void init_framebuffer(ecs_world_t *world, ecs_entity_t framebuffer) {
    VmaAllocator allocator = *ecs_singleton_get_mut(world, VmaAllocator);
    VkDevice dev = *ecs_singleton_get_mut(world, VkDevice);
    Framebuffer *fb = ecs_get_mut(world, framebuffer, Framebuffer);

    u32 count = fb->attachment_count + (fb->depth_stencil ? 1 : 0);

    VkImageView *fortnite = malloc(sizeof(VkImageView) * count);

    for (int i = 0; i < fb->attachment_count; i++) {
        const Texture *color = ecs_get(world, fb->colors[i], Texture);
        fortnite[i] = color->view;
    }

    if (fb->depth_stencil) {
        const Texture *color = ecs_get(world, fb->depth_stencil, Texture);
        fortnite[count - 1] = color->view;
    }

    VkFramebufferCreateInfo framebuffer_info = {
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .renderPass = fb->pass,
        .attachmentCount = count,
        .pAttachments = fortnite,
        .width = fb->x,
        .height = fb->y,
        .layers = 1,
    };

    fb->fbo = malloc(sizeof(VkFramebuffer));
    vkCreateFramebuffer(dev, &framebuffer_info, NULL, fb->fbo);

    free(fortnite);
}

void recreate_swapchain(ecs_world_t *world, u32 x, u32 y, u32 vsync) {
    Swapchain *swapchain = ecs_singleton_get_mut(world, Swapchain);
    VkDevice dev = *ecs_singleton_get_mut(world, VkDevice);
    Framebuffer *framebuffer = ecs_get_mut(world, swapchain->framebuffer, Framebuffer);
    vkWaitForFences(dev, 1, &swapchain->render_done, VK_TRUE, (i64)-1);

    swapchain->cur_swap_image = 0;
    framebuffer->x = x;
    framebuffer->y = y;

    VmaAllocator allocator = *ecs_singleton_get_mut(world, VmaAllocator);

    Texture *depth = ecs_get_mut(world, swapchain->depth, Texture);

    delete_texture(allocator, dev, depth);
    ecs_modified(world, swapchain->depth, Texture);

    Texture new_texture = create_fb_texture_manual(world, framebuffer->x, framebuffer->y, FBTEX_DEPTH);

    ecs_set_ptr(world, swapchain->depth, Texture, &new_texture);

    swapchain->swapchain_old = swapchain->swapchain;
    swapchain->info.imageExtent = (VkExtent2D){ framebuffer->x, framebuffer->y };
    swapchain->info.oldSwapchain = swapchain->swapchain_old;

    if (vsync) {
        swapchain->info.presentMode = VK_PRESENT_MODE_FIFO_KHR;
    } else {
        swapchain->info.presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
    }

    vkCreateSwapchainKHR(dev, &swapchain->info, NULL, &swapchain->swapchain);
    vkDestroySwapchainKHR(dev, swapchain->swapchain_old, NULL);
    u32 old_count = swapchain->count;
    vkGetSwapchainImagesKHR(dev, swapchain->swapchain, &swapchain->count, NULL);
    swapchain->images = realloc(swapchain->images, swapchain->count * sizeof(VkImage));
    vkGetSwapchainImagesKHR(dev, swapchain->swapchain, &swapchain->count, swapchain->images);
    VkImageViewCreateInfo view_info = {0};
    view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
    view_info.format = swapchain->info.imageFormat;
    view_info.subresourceRange = (VkImageSubresourceRange) {
        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .baseMipLevel = 0,
        .levelCount = 1,
        .baseArrayLayer = 0,
        .layerCount = 1 
    };

    swapchain->views = realloc(swapchain->views, swapchain->count * sizeof(VkImageView));

    for (int i = 0;  i < old_count; i++) {
        vkDestroyFramebuffer(dev, framebuffer->fbo[i], NULL);
    }

    for (int i = 0; i < old_count; i++) {
        vkDestroyImageView(dev, swapchain->views[i], NULL);
    }

    for (int i = 0; i < swapchain->count; i++) {
        view_info.image = swapchain->images[i];
        VkResult res = vkCreateImageView(dev, &view_info, NULL, &swapchain->views[i]);
        if (res != VK_SUCCESS) {
            printf("swapchain image views could not be created: %i\n", res);
            exit(1);
        }
    }

    framebuffer->fbo = realloc(framebuffer->fbo, sizeof(VkFramebuffer) * swapchain->count);
    for (int i = 0; i < swapchain->count; i++) {
        VkFramebufferCreateInfo framebuffer_info = {
            .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
            .pNext = NULL,
            .flags = 0,
            .renderPass = framebuffer->pass,
            .attachmentCount = 2,
            .pAttachments = (VkImageView[]){ swapchain->views[i], depth->view },
            .width = framebuffer->x,
            .height = framebuffer->y,
            .layers = 1,
        };

        vkCreateFramebuffer(dev, &framebuffer_info, NULL, &framebuffer->fbo[i]);
    }
    // return;

    ecs_filter_t *filter = ecs_filter(world, {
        .terms = {
            { ecs_id(Framebuffer) },
        },
    });

    // VkSampler sampler = *ecs_singleton_get_mut(world, VkSampler);
    const MainSets *texture_set = ecs_singleton_get(world, MainSets);
    VkDescriptorSet fb_set = *ecs_get_mut(world, texture_set->fb, VkDescriptorSet);
    
    ecs_iter_t iter = ecs_filter_iter(world, filter);
    ecs_iter_t *it = &iter;
    // if (false) {
    while (ecs_filter_next(it)) {
        Framebuffer *fbs = ecs_field(it, Framebuffer, 1);
        for (int j = 0; j < it->count; j++) {
            Framebuffer *fb = &fbs[j];
            fb->x = framebuffer->x;
            fb->y = framebuffer->y;

            if (!fb->attachment_count) {
                puts("empty fb");
                continue;
            }

            vkDestroyFramebuffer(dev, *fb->fbo, NULL);

            u32 count = fb->attachment_count + (fb->depth_stencil ? 1 : 0);
            VkImageView *fortnite = malloc(sizeof(VkImageView) * count);

            for (int i = 0; i < fb->attachment_count; i++) {
                Texture *color = ecs_get_mut(world, fb->colors[i], Texture);
                delete_texture(allocator, dev, color);

                Texture new_texture = create_fb_texture_manual(world, fb->x, fb->y, color->format);
                new_texture.sampler = color->sampler;

                new_texture.index = color->index;

                ecs_set_ptr(world, fb->colors[i], Texture, &new_texture);
                fortnite[i] = new_texture.view;

                if (color->index == -1) {
                    continue;
                }
                // VkSampler sampler = *ecs_get(world, color->sampler, VkSampler);
                descriptor_load_image(world, fb->colors[i], color->sampler, fb_set, color->index);
                printf("%i\n", color->index);
            }

            if (fb->depth_stencil) {
                Texture *color = ecs_get_mut(world, fb->depth_stencil, Texture);
                delete_texture(allocator, dev, color);

                Texture new_texture = create_fb_texture_manual(world, fb->x, fb->y, color->format);
                new_texture.sampler = color->sampler;

                new_texture.index = color->index;

                ecs_set_ptr(world, fb->depth_stencil, Texture, &new_texture);
                fortnite[count - 1] = new_texture.view;

                // VkSampler sampler = *ecs_get(world, color->sampler, VkSampler);
                if (color->index != -1) {
                    descriptor_load_image(world, fb->depth_stencil, color->sampler, fb_set, color->index);
                    printf("%i\n", color->index);
                }
            }

            VkFramebufferCreateInfo framebuffer_info = {
                .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
                .pNext = NULL,
                .flags = 0,
                .renderPass = fb->pass,
                .attachmentCount = count,
                .pAttachments = fortnite,
                .width = fb->x,
                .height = fb->y,
                .layers = 1,
            };

            vkCreateFramebuffer(dev, &framebuffer_info, NULL, fb->fbo);
            free(fortnite);
        }
    }
}

void toggle_vsync(ecs_world_t *world, ConVar *val) {
    Swapchain *chudchain = ecs_singleton_get_mut(world, Swapchain);
    Window *window = ecs_singleton_get_mut(world, Window);
    VkDevice dev = *ecs_singleton_get_mut(world, VkDevice);
    VkPhysicalDevice physdev = *ecs_singleton_get_mut(world, VkPhysicalDevice);
    Framebuffer *framebuffer = ecs_get_mut(world, window->framebuffer, Framebuffer);
    printf("%i\n", val->i);
    recreate_swapchain(world, framebuffer->x, framebuffer->y, val->i);
}

// VkSubpassDescription get_subpass(ecs_world_t *world, ecs_entity_t framebuffer_e) {
//     Framebuffer *fb = ecs_get_mut(world, framebuffer_e, Framebuffer);
//     u32 count = fb->attachment_count;

//     VkAttachmentReference *refs = malloc(sizeof(VkAttachmentReference) * count);
//     for (int i = 0; i < count; i++) {
//         refs[i].attachment = i;
//         refs[i].layout = ecs_get(world, fb->colors[i], Texture)->format.layout;
//     }

//     VkSubpassDescription subpass = {
//         .flags = 0,
//         .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
//         .inputAttachmentCount = 0,
//         .pInputAttachments = NULL,
//         .colorAttachmentCount = count,
//         .pColorAttachments = refs,
//         .pResolveAttachments = NULL,
//         .pDepthStencilAttachment = &(VkAttachmentReference){ count, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL },
//         .preserveAttachmentCount = 0,
//         .pPreserveAttachments = NULL
//     };
//     return subpass;
// }

void VulkanImport(ecs_world_t *world) {
    ECS_MODULE(world, Vulkan);

    // VK_FUNC(vkCmdBeginRenderingKHR);
    // VK_FUNC(vkCmdEndRenderingKHR);

    ECS_COMPONENT_DEFINE(world, VkQueue);
    ECS_COMPONENT_DEFINE(world, Buffer);
    ECS_COMPONENT_DEFINE(world, Texture);
    ECS_COMPONENT_DEFINE(world, VkCommandBuffer);
    ECS_COMPONENT_DEFINE(world, VkDevice);
    ECS_COMPONENT_DEFINE(world, VkPhysicalDevice);
    ECS_COMPONENT_DEFINE(world, VmaAllocator);
    ECS_COMPONENT_DEFINE(world, UniformBuffer);
    ECS_COMPONENT_DEFINE(world, Pipeline);
    ECS_COMPONENT_DEFINE(world, Framebuffer);
    ECS_COMPONENT_DEFINE(world, Swapchain);
    ECS_COMPONENT_DEFINE(world, Framebuffer);
    ECS_COMPONENT_DEFINE(world, RenderPass);
    ECS_COMPONENT_DEFINE(world, Shader);
    ECS_COMPONENT_DEFINE(world, VkSampler);
    ECS_COMPONENT_DEFINE(world, VkDescriptorSet);
    ECS_COMPONENT_DEFINE(world, MainSets);
    ECS_COMPONENT_DEFINE(world, Mesh);

    ECS_SYSTEM_DEFINE(world, delete_textures, 0, Texture);
    // ECS_SYSTEM_DEFINE(world, wait_for_frame, EcsOnLoad);
    ECS_SYSTEM_DEFINE(world, begin_new_frame, PInput);
    ECS_SYSTEM_DEFINE(world, present_frame, PPresent);
    // ECS_SYSTEM(world, set_positions, POutputs, Mesh, Matrix4, Render3D);

    ecs_query_t *main_pass_query = ecs_query(world, {
        .filter.terms = {
            // { ecs_id(Mesh) },
            { ecs_id(Matrix4) },
            { ecs_id(Render3D) },
        }
    });

    ecs_query_t *light_query = ecs_query(world, {
        .filter.terms = {
            { ecs_id(WorldTransform) },
            { ecs_id(Light) },
        }
    });

    ecs_query_t **queries = malloc(sizeof(void*) * 2);
    queries[0] = main_pass_query;
    queries[1] = light_query;

   ecs_system_init(world, &(ecs_system_desc_t){
        .entity = ecs_entity(world, {
            .name = "render_passes",
            .add = { ecs_dependson(POutputs) },
        }),
        .query.filter.terms = {
            { .id = ecs_id(RenderPass), .inout = EcsIn },
            { .id = ecs_id(RenderPass), .src.flags = EcsCascade, .src.trav = EcsDependsOn, .inout = EcsIn }, 
            { .id = ecs_id(Matrix4), .src.flags = EcsIsEntity, .inout = EcsIn },
            { .id = ecs_id(Camera), .inout = EcsInOut, .src.flags = EcsIsEntity },
        },
        .ctx = queries,
        .callback = render_passes,
    });

}

