#version 460

layout (location = 0) out vec4 color;

layout (location = 0) in vec2 tex_coord;
layout (location = 1) in vec3 normal;

layout (set = 0, binding = 0) uniform sampler2D tex[2];
layout (set = 0, binding = 1) uniform ubo0 {
    float time;
    int index;
}fortnite;

layout (push_constant) uniform pc {
    vec2 offset;
    vec2 screen_size;
};

void main () {

    vec2 uv = gl_FragCoord.xy / screen_size * 2.0 - 1.0;
    // if (length(uv - offset) < 0.2) {
    //     discard;
    // }
    // if (length(gl_FragCoord.xy - vec2(600, 400)) > 200) {
    //     return;
    // }
    // color = vec4(texture(tex[fortnite.index], vec2(mod(tex_coord.x - sin(fortnite.time), 1.0), tex_coord.y)).rgb, 0.5);
    color = dot(normalize(vec3(-1, 1, -1)), normal) * texture(tex[0], tex_coord);
    // color = vec4(gl_FragCoord.xy, 0.0, 1.0);
}
