#version 460

#extension GL_EXT_buffer_reference2 : require
#extension GL_EXT_nonuniform_qualifier : require
#extension GL_EXT_scalar_block_layout : require
#extension GL_EXT_debug_printf : enable

layout (location = 0) out vec4 color;

layout (location = 0) in vec2 tex_coord;

layout (set = 0, binding = 0) uniform sampler2D tex[];
layout (set = 0, binding = 0) uniform sampler3D tex3[];

struct MaterialInfo {
    bool lit;
    uint tex;
    uint metallic_roughness_tex;
    vec3 albedo;
    float metallic;
    float roughness;
};

layout (set = 0, binding = 2, scalar) readonly buffer fortnite {
    MaterialInfo materials[];
};

struct LightInfo {
    vec3 pos;
    vec3 color;
    vec3 dir;
    uint type;
    uint shadowed;
};

layout (set = 1, binding = 1, scalar) readonly buffer chud {
    LightInfo infos[];
};

layout (set = 3, binding = 0) uniform sampler2D framebuffer[];
layout (set = 3, binding = 0) uniform usampler2D framebufferi[];

layout (set = 2, binding = 0) uniform ubo0 {
    mat4 proj;
    mat4 view;
    vec3 cam_pos;
    uint object_count;
    uint light_count;
    bool among;
};

const float PI = 3.14159265359;

vec3 fresnel_schlick(float cos_theta, vec3 f0) {
    return f0 + (1.0 - f0) * pow(clamp(1.0 - cos_theta, 0.0, 1.0), 5.0);
}

float distribution_ggx(vec3 norm, vec3 halfway, float roughness) {
    float a = roughness * roughness;
    float a2 = a*a;
    float NdotH = max(dot(norm, halfway), 0.0);
    float NdotH2 = NdotH * NdotH;

    float num = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return num / denom;
}

float geometry_schlick_ggx(float NdotV, float roughness) {
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return num / denom;
}

float geometry_smith(vec3 norm, vec3 view, vec3 light, float roughness) {
    float NdotV = max(dot(norm, view), 0.0);
    float NdotL = max(dot(norm, light), 0.0);
    float ggx2 = geometry_schlick_ggx(NdotV, roughness);
    float ggx1 = geometry_schlick_ggx(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 calc_light(vec3 view_dir, vec3 norm, vec3 light_dir, vec3 radiance, vec3 albedo, uint mat_index, vec2 uv) {
    vec3 halfway = normalize(view_dir + light_dir);
    MaterialInfo mat = materials[mat_index];

    vec2 mrt = texture(tex[nonuniformEXT(mat.metallic_roughness_tex)], uv).gb;
    float roughness = mat.roughness * mrt.r;
    float metallic = mat.metallic * mrt.g;

    vec3 f0 = vec3(0.04);
    f0      = mix(f0, albedo, metallic);
    vec3  F = fresnel_schlick(max(dot(halfway, view_dir), 0.0), f0);

    float NDF = distribution_ggx(norm, halfway, roughness);
    float G = geometry_smith(norm, view_dir, light_dir, roughness);

    float NdotL = max(dot(norm, light_dir), 0.0);

    vec3 numerator = NDF * G * F;
    float denominator = 4.0 * max(dot(norm, view_dir), 0.0) * NdotL + 0.0001;
    vec3 specular = numerator / denominator;

    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;

    kD *= 1.0 - metallic;

    return (kD * albedo / PI + specular) * radiance * NdotL;
}

void main () {
    uvec2 mat = texture(framebufferi[3], tex_coord).rg;
    vec2 frag_tex = texture(framebuffer[0], tex_coord).rg;
    vec4 pixel_sample = texture(tex[nonuniformEXT(materials[mat.r].tex)], frag_tex);
    vec3 pixel = pixel_sample.rgb * materials[mat.r].albedo;
    if (pixel_sample.a < 0.5) {
        // discard;
    }
    if (!materials[mat.r].lit) {
        color.rgb = pixel;
        // color.rgb = vec3(0.0, 0.0, 0.0);
        color.a = 1;
        return;
    }
    vec3 pos = texture(framebuffer[1], tex_coord).rgb + cam_pos;
    vec3 view_dir = normalize(cam_pos - pos);
    vec3 norm = texture(framebuffer[2], tex_coord).rgb;

    vec3 albedo = pixel * materials[mat.r].albedo;

    vec3 out_col = vec3(0.0);
    for (int i = 0; i < light_count; i++) {
        switch (infos[i].type) {
            case 0:
                out_col += calc_light(view_dir, norm, infos[i].dir, infos[i].color, albedo, mat.r, frag_tex);
                break;
            case 1:
                float dist = length(infos[i].pos - pos);
                float attenuation = 1.0 / (dist * dist);
                vec3 radiance = infos[i].color * attenuation;
                vec3 light_dir = normalize(infos[i].pos - pos);
                out_col += calc_light(view_dir, norm, light_dir, radiance, albedo, mat.r, frag_tex);
                break;
            case 2:
                break;
        }
            // out_col = vec3(1.0);
        // out_col = infos[i].color;
    }
    float ao = 1.0;
    vec3 ambient = 0.03 * albedo * ao;
    vec3 hdr_col = ambient + out_col;
    // color.rgb = texture(tex[uint(col.b * 255.0)], color.rg).rgb;
    // color.rgb = norm;
    float gamma = 2.2;

    // hdr_col = pow(hdr_col, vec3(gamma));
    // uv.y = 1.0 - uv.y;

    vec3 encoded = hdr_col / (hdr_col + vec3(1.0));
    const float LUT_DIMS = 48.0;
    vec3 uv = encoded * ((LUT_DIMS - 1.0) / LUT_DIMS) + 0.5 / LUT_DIMS;
    vec3 mapped = texture(tex3[2], uv).rgb;

    // vec3 corrected = pow(mapped, vec3(1.0/gamma));
    color.rgb = mapped;
    // color.rgb = corrected;
    // color.rgb = norm;
    color.a = 1.0;
}
