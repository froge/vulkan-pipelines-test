#version 460

#extension GL_EXT_scalar_block_layout : require
#extension GL_EXT_nonuniform_qualifier : require

layout (location = 0) out vec4 color;

layout (location = 0) in vec2 tex_coord;

layout (set = 0, binding = 0) uniform sampler2D tex[];

layout (push_constant, scalar) uniform pc {
    mat4 view;
    mat4 model;
    uint index;
};

void main () {
    vec3 tex_col;
    // if (source != 0) {
    // } else {
        // color.rgb = albedo;
    // }
    color.rgb = vec3(1.0);
    // color.rg = tex_coord;
    // color.b = 1.0;
    // if (texture(tex[index], tex_coord).r < 0.5) {
    //     discard;
    // }
    color.a = smoothstep(0.45, 0.55, texture(tex[nonuniformEXT(index)], tex_coord).r); 
    // color.a = 1.0;
}
