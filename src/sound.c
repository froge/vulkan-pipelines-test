#define SOUND_H_DECLARE

#include "SDL2/SDL.h"
#include "common.h"
#include "sound.h"
#include "cglm/call.h"
#include "components.h"
#include "physics.h"
#include "renderer.h"

#include "phases.h"

ecs_entity_t load_sound(ecs_world_t *world, char *filename) {
    ma_engine *engine = ecs_singleton_get_mut(world, ma_engine);
    // SoundEngine *_engine = ecs_singleton_get_mut(world, SoundEngine);
    // SoundResource *sound = ecs_get_mut(world, sound_e, SoundResource);
    ma_sound *sound = malloc(sizeof(ma_sound));
    ecs_entity_t sound_e = ecs_set(world, 0, SoundResource, {sound});
    ecs_set_name(world, sound_e, filename);
    ma_result result = ma_sound_init_from_file(engine, filename, MA_SOUND_FLAG_DECODE, NULL, NULL, sound);
    if (result != MA_SUCCESS) {
     printf("wtf you doin\n");
     exit(1);
    }
    ecs_singleton_modified(world, ma_engine);
    return sound_e;
}

void set_listener_pos(ecs_iter_t *it) {
    ecs_world_t *world = it->world;
    const Camera *camera = ecs_singleton_get(world, Camera);
    const WorldTransform *pos = ecs_get(world, ecs_id(Camera), WorldTransform);
    ma_engine *engine = ecs_singleton_get_mut(world, ma_engine);
    // printf("%f %f %f\n", camera->pos[0], camera->pos[1], camera->pos[2]);
    ma_engine_listener_set_position(engine, 0, pos->pos.x, pos->pos.y, pos->pos.z);
    ma_engine_listener_set_direction(engine, 0, camera->front[0], camera->front[1], camera->front[2]);
    ma_engine_listener_set_world_up(engine, 0, 0, 1, 0);
    ecs_singleton_modified(world, ma_engine);
}

void cleanup_sound(ecs_iter_t *it) {
    Sound *sounds = ecs_field(it, Sound, 1);
    for (int i = 0; i < it->count; i++) {
        Sound *sound = &sounds[i];
        ma_sound_uninit(sound->sound);
        free(sound->sound);
    }
}

ECS_CTOR(Sound, ptr, {
    // ptr->sound = NULL;
});

ECS_DTOR(Sound, ptr, {
    if (ptr->sound) {
        ma_sound_uninit(ptr->sound);
        free(ptr->sound);
        ptr->player = 0;
    }
});

ECS_MOVE(Sound, dst, src, {
    // free(dst->sound);
    dst->sound = src->sound;
    src->sound = NULL;
    dst->player = src->player;
    src->player = 0;
});

ECS_COPY(Sound, dst, src, {
    dst->sound = src->sound;
    dst->player = src->player;
});

void delete_sound(ecs_world_t *world, void *data) {
    ecs_entity_t entity = (ecs_entity_t)data;
    ecs_delete(world, entity);
}

void set_sound_pos(ecs_iter_t *it) {
    Sound *sounds = ecs_field(it, Sound, 1);
    // Velocity *ps = ecs_field(it, Velocity, 2);
    // ecs_entity_t fart = ecs_field_src(it, 1);
    for (int i = 0; i < it->count; i++) {
        // Position *transform = &ts[i];
        // Velocity *physics = &ps[i];
        Sound *sound = &sounds[i];
        // u32 index = 0;
        ecs_entity_t sound_e = sound->player;
        // while ((sound_e = ecs_get_target(it->world, it->entities[i], Plays, index++))) {
        // if () {
        //     printf("wtf going on \n");
        //     continue;
        // }
        if (!ecs_is_alive(it->world, sound_e) || ma_sound_at_end(sound->sound)) {
            ecs_run_post_frame(it->world, delete_sound, (void *)it->entities[i]);
            continue;
        }
        const Transform *transform = ecs_get(it->world, sound_e, WorldTransform);
        const Velocity *physics = ecs_get(it->world, sound_e, Velocity);
        // Sound *sound = ecs_get_mut(it->world, sound_e, Sound);
        ma_sound_set_position(sound->sound, transform->pos.x, transform->pos.y, transform->pos.z);
        // printf("%f %f %f\n", transform->pos[0], transform->pos[1], transform->pos[2]);
        ma_sound_set_velocity(sound->sound, physics->x, physics->y, physics->z);
        // ecs_modified(it->world, sound_e, Sound);
        // }
    }
    // printf("the chud\n");
}

struct SoundEndedCallbackData {
    ecs_world_t *world;
    ecs_entity_t entity;
};

void queue_delete_sound(void *data, ma_sound *sound) {
    struct SoundEndedCallbackData *d = data;
    ecs_run_post_frame(d->world, delete_sound, (void*)d->entity);
    free(d);
}

void play_sound(ecs_world_t *world, ecs_entity_t sound_player, ecs_entity_t sound_resource) {
    if (!ecs_has(world, sound_player, SoundPlayer)) {
        printf("cant play sound on an entity without SoundPlayer component\n");
        printf("entity name: %s\n", ecs_get_name(world, sound_player));
        return;
    }
    ma_engine *engine = ecs_singleton_get_mut(world, ma_engine);
    const SoundResource *soundr = ecs_get(world, sound_resource, SoundResource);
    ma_sound *sound = malloc(sizeof(ma_sound));
    ma_sound_init_copy(engine, soundr->sound, 0, NULL, sound);
    ma_sound_start(sound);
    ma_sound_set_volume(sound, 5.0);
    // printf("%llx\n", sound);
    ecs_entity_t sound_e = ecs_set(world, 0, Sound, {sound, sound_player});
    // ecs_add_pair(world, sound_player, Plays, sound_e);
    // struct SoundEndedCallbackData *sounde = malloc(sizeof(*sounde));
    // sounde->world = world;
    // sounde->entity = sound_e;
    // ma_sound_set_end_callback(sound, &queue_delete_sound, sounde);
}

void SoundMImport(ecs_world_t *world) {
    ECS_MODULE(world, SoundM);

    ECS_COMPONENT_DEFINE(world, Sound);
    ECS_COMPONENT_DEFINE(world, SoundResource);
    ECS_COMPONENT_DEFINE(world, ma_engine);

    ECS_TAG_DEFINE(world, SoundPlayer);

    ECS_ENTITY_DEFINE(world, Plays);

    ECS_SYSTEM_DEFINE(world, set_listener_pos, POutputs);
    ECS_SYSTEM_DEFINE(world, set_sound_pos, POutputs, [in] Sound, [in] WorldTransform(), [in] Velocity(), SoundPlayer());

    ecs_set_hooks(world, Sound, {
        .ctor = ecs_ctor(Sound),
        .move = ecs_move(Sound),
        .copy = ecs_copy(Sound),
        .dtor = ecs_dtor(Sound),
    });

    ecs_set_name(world, ecs_new_id(world), "sounds");
    // ECS_OBSERVER(world, cleanup_sound, EcsOnRemove, Sound);
}
