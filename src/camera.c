#include "flecs.h"
#include <cglm/call.h>
#include <math.h>
#include "physics.h"

#define RENDERER_EXTERN extern
#include "camera.h"

void cam_calc_front(struct Camera *cam) {
	cam->front[0] = cos(glm_rad(cam->yaw)) * cos(glm_rad(cam->pitch));
	cam->front[1] = sin(glm_rad(cam->pitch));
	cam->front[2] = sin(glm_rad(cam->yaw)) * cos(glm_rad(cam->pitch));
	glm_normalize(cam->front);
}

void init_camera(struct Camera *cam) {
	cam->pos[0] = 0.0;
	cam->pos[1] = 0.0;
	cam->pos[2] = 0.0;
	cam->up[0] = 0.0;
	cam->up[1] = 1.0;
	cam->up[2] = 0.0;
	cam->yaw = 0;
	cam->pitch = 0;
	cam_calc_front(cam);
}

void move_z(struct Camera *cam, vec3 *target, float amount) {
	vec3 temp;
	glm_vec3_copy(cam->front, temp);
	temp[1] = 0.;
	glm_normalize(temp);
	glm_vec3_scale(temp, amount, temp);
	glm_vec3_add(*target, temp, *target);
}

void move_x(struct Camera *cam, vec3 *target, float amount) {
	vec3 temp;
	glm_vec3_cross(cam->front, cam->up, temp);
	glm_normalize(temp);
	glm_vec3_scale(temp, amount, temp);
	temp[1] = 0.;
	glm_vec3_add(*target, temp, *target);
}

void move_y(struct Camera *cam, vec3 *target, float amount) {
	vec3 temp;
	temp[0] = 0.;
	temp[1] = amount;
	temp[2] = 0.;
	glm_vec3_add(*target, temp, *target);
}

/*
void move_z(struct Camera *cam, float amount) {
	vec3 temp;
	glm_vec3_copy(cam->front, temp);
	temp[1] = 0.;
	glm_normalize(temp);
	glm_vec3_scale(temp, amount, temp);
	glm_vec3_add(cam->pos, temp, cam->pos);
}

void move_x(struct Camera *cam, float amount) {
	vec3 temp;
	glm_vec3_cross(cam->front, cam->up, temp);
	glm_normalize(temp);
	glm_vec3_scale(temp, amount, temp);
	temp[1] = 0.;
	glm_vec3_add(cam->pos, temp, cam->pos);
}

void move_y(struct Camera *cam, float amount) {
	vec3 temp;
	temp[0] = 0.;
	temp[1] = amount;
	temp[2] = 0.;
	glm_vec3_add(cam->pos, temp, cam->pos);
}
*/

void cam_rot_pitch(struct Camera *cam, float degrees) {
	cam->pitch += degrees;
	if (cam->pitch > 89.) {
		cam->pitch = 89.;
	}
	if (cam->pitch < -89) {
		cam->pitch = -89.;
	}
	cam_calc_front(cam);
}

void cam_rot_yaw(struct Camera *cam, float degrees) {
	cam->yaw += degrees;
	cam_calc_front(cam);
}

void cam_view(ecs_world_t *world, mat4 *view) {
	const struct Camera *cam = ecs_singleton_get(world, Camera);
	const WorldTransform *pos = ecs_get(world, ecs_id(Camera), WorldTransform );
	vec3 temp;
	// glm_vec3_sub(pos->pos.raw, (float*)cam->front, temp);
	glm_vec3_add(pos->pos.raw, cam->front, temp);
	glm_lookat(pos->pos.raw, temp, (float*)cam->up, *view);
}
