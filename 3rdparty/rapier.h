#pragma once

#include "common.h"

#ifdef RAPIER_H_DEFINE
#define RAPIER_EXTERN extern
#else
#define RAPIER_EXTERN
#endif

typedef struct {
    f32 x;
    f32 y;
    f32 z;
}RVector3;

typedef struct {
    f32 x;
    f32 y;
    f32 z;
    f32 w;
}RVector4;

typedef RVector4 RQuaternion;

typedef enum {
    RigidBodyTypeDynamic,
    RigidBodyTypeFixed,
    RigidBodyTypeKinematicPositionBased,
    RigidBodyTypeKinematicVelocityBased,
}RigidBodyType;

typedef struct {
    RigidBodyType type;
    RVector3 pos;
    RQuaternion rotation;
    RVector3 linvel;
    f32 gravity_scale;
    u64 user_data;
}RigidBodyOptions;

typedef struct RigidBody RigidBody;
typedef struct RigidBodySet RigidBodySet;
typedef struct ColliderSet ColliderSet;
typedef struct PhysicsState PhysicsState;
typedef struct PhysicsPipeline PhysicsPipeline;
typedef struct Collider Collider;
typedef struct KinematicCharacterController KinematicCharacterController;

typedef struct {
    u32 a;
    u32 b;
}RigidBodyHandle;

typedef RigidBodyHandle ColliderHandle;

RigidBodySet *rapier_RigidBodySet_new();
ColliderSet *rapier_ColliderSet_new();
PhysicsState *rapier_PhysicsState_new();
PhysicsPipeline *rapier_PhysicsPipeline_new();
KinematicCharacterController *rapier_KinematicCharacterController_create();

void rapier_PhysicsPipeline_step(PhysicsPipeline *pipeline, PhysicsState *state, RigidBodySet *set, ColliderSet *collider_set);
RigidBody *rapier_RigidBody_create(const RigidBodyOptions *options);
RigidBodyHandle rapier_RigidBodySet_insert(RigidBodySet *set, RigidBody *body);
void rapier_RigidBodySet_remove(RigidBodySet *rset, ColliderSet *cset, PhysicsState *state, RigidBodyHandle body);
Collider *rapier_Collider_create_cuboid(RVector3 size, f32 density);
ColliderHandle rapier_ColliderSet_insert_with_parent(ColliderSet *collider_set, Collider *collider, RigidBodyHandle handle, RigidBodySet *rigid_body_set);
RigidBody *rapier_RigidBodySet_index(RigidBodySet *set, RigidBodyHandle handle);
void rapier_RigidBody_translation(RigidBody *body, RVector3 *pos);
void rapier_RigidBody_set_translation(RigidBody *body, RVector3 *pos);
void rapier_RigidBody_linvel(RigidBody *body, RVector3 *pos);
void rapier_RigidBody_set_linvel(RigidBody *body, RVector3 *pos);
void rapier_RigidBody_set_angvel(RigidBody *body, RVector3 *pos);
void rapier_RigidBody_rotation(RigidBody *body, RVector4 *rot);
void rapier_RigidBody_set_rotation(RigidBody *body, RVector4 *rot);
void rapier_RigidBody_predict_position(RigidBody *body, f32 delta, RVector3 *pos, RVector4 *rot);
u8 rapier_RigidBody_is_sleeping(RigidBody *body);
u64 rapier_RigidBody_user_data(RigidBody *body);
void rapier_PhysicsState_set_delta(PhysicsState *state, f32 delta);
int rapier_KinematicCharacterController_move_body(KinematicCharacterController *controller, PhysicsState *state, RigidBodySet *rset, ColliderSet *cset, RigidBodyHandle handle, RVector3 *desired_pos);

typedef void ContactHandler(void *user_data, RigidBodyHandle col1, RigidBodyHandle col2);
void rapier_PhysicsState_set_contact_force_handler(PhysicsState *state, ContactHandler *handler);
void rapier_PhysicsState_set_user_data(PhysicsState *state, void *data);
void *rapier_PhysicsState_get_user_data(PhysicsState *state);
