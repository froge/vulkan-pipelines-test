#include "enet.h"
#include <stdio.h>

#define NETWORKING_H_DECLARE
#include "networking.h"
#include "actions.h"
#include "phases.h"
#include "convars.h"
#include "input.h"
#include "renderer.h"

void networking_init_client(ecs_world_t *world, char *addr) {
  ENetHost *client = enet_host_create(NULL, 1, 2, 0, 0);
  puts("clietn");

  if (!client) {
    printf("chuddie death\n");
    exit(1);
  }

  ENetAddress address;
  ENetEvent event;
  ENetPeer *peer;
  enet_address_set_host(&address, addr);
  address.port = 27015;

  peer = enet_host_connect(client,  &address, 2, 0);

  if (enet_host_service(client, &event, 1000) > 0 && event.type == ENET_EVENT_TYPE_CONNECT) {
  }

  peer->data = (void*)ecs_new_id(world);
  ecs_entity_t player = ecs_new_id(world);
  // ecs_set(world, player, Mesh, { 36, ecs_lookup(world, "cube_buf"), 0 });
  ecs_add(world, player, Render3D);
  ecs_set(world, player, Position, {});

  ecs_set(world, (ecs_entity_t)peer->data, Peer, { peer, player });
  ecs_set_name(world, (ecs_entity_t)peer->data, "server");
  
  ecs_singleton_set(world, Host, { client, 0 });
}

void networking_init_server(ecs_world_t *world) {
  ENetAddress address = {
    ENET_HOST_ANY,
    27015,
  };

  ENetHost *server = enet_host_create(&address, 100, 2, 0, 0);
  if (!server) {
    printf("chuddie death\n");
    exit(1);
  }

  ecs_singleton_set(world, Host, { server, 1 });
}

void networking_init(ecs_world_t *world) {
  if (enet_initialize()) {
    printf("chuddie death\n");
    exit(1);
  }
  Players fort = { 10, 1, malloc(sizeof(struct Player) * 10) };
  for (int i = 0; i < fort.max; i++) {
    fort.players[i].alive = 0;
    fort.players[i].generation = 0;
  }
  ecs_singleton_set_ptr(world, Players, &fort);
}

void networking_deinit(ecs_world_t *world) {
  enet_deinitialize();
}

void handle_networking(ecs_iter_t *it) {
  ENetEvent event;

  ConVar *sequence = ecs_get(it->world, ecs_lookup(it->world, "networking_sequence_id"), ConVar);
  Host *_host = ecs_singleton_get_mut(it->world, Host);
  Players *clients = ecs_singleton_get_mut(it->world, Players);
  ENetHost *host = _host->host;

  ecs_query_t **queries = it->ctx;
  ecs_query_t *event_query = queries[0];
  ecs_query_t *networked_query = queries[1];
  ecs_iter_t _iter = ecs_query_iter(it->world, event_query);
  ecs_iter_t *iter = &_iter;
  while (ecs_query_next(iter)) {
    NetworkEvent *evs = ecs_field(iter, NetworkEvent, 1);

    for (int i = 0; i < iter->count; i++) {
      NetworkEvent *ev = &evs[i];
      ENetPacket *packet;
      // if (ev->type == network_event_action) {
      //   packet = enet_packet_create(NULL, sizeof(NetworkEvent), ENET_PACKET_FLAG_RELIABLE);
      // }
      packet = enet_packet_create(NULL, sizeof(NetworkEvent), ENET_PACKET_FLAG_RELIABLE);
      u8 *data = packet->data;
      memcpy(data, ev, sizeof(NetworkEvent));
      enet_host_broadcast(host, 0, packet);
      ecs_delete(it->world, iter->entities[i]);
    }
  }

  _iter = ecs_query_iter(it->world, networked_query);
  // iter = &_iter;
  ConVar *send_sequence = ecs_get(it->world, ecs_lookup(it->world, "networking_send_sequence_id"), ConVar);
  while (ecs_query_next(&_iter)) {
    ecs_id_t id = ecs_field_id(&_iter, 1);
    ecs_entity_t second = ecs_pair_second(it->world, id);

    for (int i = 0; i < _iter.count; i++) {
      ComponentChange c = {
        send_sequence->i,
        second,
        ecs_get_id(it->world, _iter.entities[i], second),
      };
      NetworkEvent e = {
        network_event_component_change,
        _iter.entities[i],
      };
      if (ecs_has(it->world, e.entity, PlayerTag)) {
        const PlayerTag *tag = ecs_get(it->world, e.entity, PlayerTag);
        if ((!tag->player && _host->server) || (tag->player && !_host->server)) {
          continue;
        }
        e.type = network_event_player_component_change;
        e.player.entity = tag->offset;
        e.entity = 0;
        e.player.player = tag->player;
        e.player.generation = clients->players[tag->player].generation;
        // printf("changed %i\n", e.player.player);
      } else if (!_host->server) {
        continue;
      }
      // puts(ecs_get_name(it->world, c.component));
      const ecs_type_info_t *info = ecs_get_type_info(it->world, second);
      u32 len = sizeof(NetworkEvent) + sizeof(u16) + sizeof(ecs_id_t) + info->size;
      ENetPacket *packet = enet_packet_create(NULL, len, 0);
      u8 *data = packet->data;
      memcpy(data, &e, sizeof(NetworkEvent));
      data += sizeof(NetworkEvent);
      memcpy(data, &c.sequence, sizeof(u16));
      data += sizeof(u16);
      memcpy(data, &c.component, sizeof(ecs_id_t));
      data += sizeof(ecs_id_t);
      memcpy(data, c.value, info->size);
      enet_host_broadcast(host, 0, packet);
    }
    // free(name);
  }
  send_sequence->i += 1;

  while (enet_host_service(host, &event, 0) > 0) {
    switch (event.type) {
      case ENET_EVENT_TYPE_CONNECT:
        {
        u16 new = 0;
        for (int i = 0; i < clients->len; i++) {
          if (!clients->players[i].alive) {
            new = i;
            break;
          }
        }
        if (!new) {
          new = clients->len;
          clients->len += 1;
        }
        printf("chuddy connect: %i\n", new);
        event.peer->data = (void*)(u64)new;

        struct Player *client = &clients->players[new];
        client->alive = 1;
        client->entities = malloc(sizeof(ecs_entity_t) * 10);
        client->len = 1;
        client->max = 10;
        client->peer = event.peer;

        NetworkEvent player_connect = {
          network_event_player_connected,
          0,
          .player = {
            .player = new,
            .generation = client->generation,
          },
        };
        u32 len = sizeof(NetworkEvent);
        ENetPacket *packet = enet_packet_create(&player_connect, len, 0);
        // enet_host_broadcast(_host->host, 0, packet);
        for (int i = 1; i < clients->len - 1; i++) {
          enet_peer_send(clients->players[i].peer, 0, packet);
        }

        NetworkEvent player_connect2 = {
          network_event_player_identify,
          0,
          .player = {
            .player = new,
            .generation = client->generation,
          },
        };

        packet = enet_packet_create(&player_connect2, len, 0);
        enet_peer_send(event.peer, 0, packet);

        ecs_entity_t player = ecs_new_id(it->world);
        ecs_set(it->world, player, PlayerTag, { 0, new });
        ecs_set(it->world, player, Position, { { 0, 0, 0 } });
        // ecs_add_pair(it->world, player, Networked, ecs_id(Position));

        client->entities[0] = player;
        break;
      }
      case ENET_EVENT_TYPE_RECEIVE:
      {
        ComponentChange c;
        NetworkEvent e;

        u8 *data = event.packet->data;
        memcpy(&e, data, sizeof(NetworkEvent));
        data += sizeof(NetworkEvent);
        switch (e.type) {
          case network_event_player_identify:
            clients->me = e.player.player;
            clients->players[e.player.player] = clients->players[0];
            clients->len = e.player.player + 1;
            for (int i = 1; i < clients->len - 1; i++) {
              struct Player *client = &clients->players[i];
              client->generation = e.player.generation;
              client->alive = 1;
              client->entities = malloc(sizeof(ecs_entity_t) * 10);
              client->len = 1;
              client->max = 10;
              client->peer = 0;

              ecs_entity_t player = ecs_new_id(it->world);
              // ecs_set(it->world, player, Mesh, { 36, ecs_lookup(it->world, "cube_buf"), 0 });
              ecs_add(it->world, player, Render3D);
              ecs_set(it->world, player, Position, {});
              ecs_set(it->world, player, Matrix4, {});

              client->entities[0] = player;
            }
            printf("identified as %i\n", e.player.player);
            break;
          case network_event_player_connected:
            {
              printf("new player connected: id: %i, gen: %i\n", e.player.player, e.player.generation);
              if (clients->len < e.player.player) {
                 clients->len = e.player.player;
              }
              struct Player *client = &clients->players[e.player.player];
              client->generation = e.player.generation;
              client->alive = 1;
              client->entities = malloc(sizeof(ecs_entity_t) * 10);
              client->len = 1;
              client->max = 10;
              client->peer = 0;

              ecs_entity_t player = ecs_new_id(it->world);
              // ecs_set(it->world, player, Mesh, { 36, ecs_lookup(it->world, "cube_buf"), 0 });
              ecs_add(it->world, player, Render3D);
              ecs_set(it->world, player, Position, {});
              ecs_set(it->world, player, Matrix4, {});

              client->entities[0] = player;
              break;
            }
          case network_event_player_component_change:
            if (_host->server) {
              struct Player *player = &clients->players[(u64)event.peer->data];
              e.player.player = (u64)event.peer->data;
              ENetPacket *packet = enet_packet_create(NULL, event.packet->dataLength, 0);
              memcpy(packet->data, &e, sizeof(NetworkEvent));
              memcpy(packet->data + sizeof(NetworkEvent), event.packet->data + sizeof(NetworkEvent), event.packet->dataLength - sizeof(NetworkEvent));
              for (int i = 1; i < clients->len; i++) {
                if (e.player.player == i) {
                  continue;
                }
                enet_peer_send(clients->players[i].peer, 0, packet);
              }
              e.entity = player->entities[e.player.entity];
              if (player->len < e.player.entity + 1) {
                player->len = e.player.entity + 1;
              }
            } else {
              e.entity = clients->players[e.player.player].entities[e.player.entity];
            }
            if (!e.entity) {
              clients->players[e.player.player].entities[e.player.entity] = ecs_new_id(it->world);
              e.entity = clients->players[e.player.player].entities[e.player.entity];
            }
            // e.entity += ecs_get(it->world, (ecs_entity_t)event.peer->data, Peer)->player;
            // puts("fortnite");
          case network_event_component_change:
            memcpy(&c.sequence, data, sizeof(u16));
            data += sizeof(u16);
            memcpy(&c.component, data, sizeof(ecs_id_t));
            data += sizeof(ecs_id_t);
            // puts(ecs_get_name(it->world, c.component));
            // printf("%i\n", c.sequence);
            const ecs_type_info_t *info = ecs_get_type_info(it->world, c.component);
            c.value = data;
            void *component = ecs_get_mut_id(it->world, e.entity, c.component);
            memcpy(component, c.value, info->size);
            if (c.sequence > sequence->i) {
              sequence->i = c.sequence;
            }
            break;
          case network_event_action:
          {
            const Action *action = ecs_get(it->world, e.action, Action);
            action->callback(it->world, e.entity, action->data);
            break;
          }
          case network_event_input_action:
          {
            const InputAction *action = ecs_get(it->world, e.entity, InputAction);
            action->callback(it->world, action->data);
            break;
          }
        }
        enet_packet_destroy(event.packet);
        break;
      }
      case ENET_EVENT_TYPE_DISCONNECT:
        printf("chuddy %lu disconnected\n", (size_t)event.peer->data);
        const Peer *peer = ecs_get(it->world, (ecs_entity_t)event.peer->data, Peer);
        ecs_delete(it->world, peer->player);
        ecs_delete(it->world, (ecs_entity_t)event.peer->data);
        break;
    }
  }
}

ecs_entity_t new_networked_entity(ecs_world_t *world) {
  ecs_entity_t ent;
  // ecs_set_entity_range(world, 0, 10000);
  ent = ecs_new_id(world);
  // ecs_set_entity_range(world, 10000, 0);
  return ent;
}

void NetworkingImport(ecs_world_t *world) {
  ECS_MODULE(world, Networking);

  ECS_COMPONENT_DEFINE(world, Host);
  ECS_COMPONENT_DEFINE(world, Peer);
  ECS_COMPONENT_DEFINE(world, NetworkEvent);
  ECS_COMPONENT_DEFINE(world, PlayerNetworked);
  ECS_COMPONENT_DEFINE(world, PlayerTag);
  ECS_COMPONENT_DEFINE(world, Players);

  ECS_TAG_DEFINE(world, Networked);

  ecs_query_t **queries = malloc(sizeof(ecs_query_t*) * 2);

  queries[0] = ecs_query(world, {
    .filter.terms = {
      { .id = ecs_id(NetworkEvent) },
    },
  });

  queries[1] = ecs_query(world, {
    .filter.terms = {
      { .id = ecs_pair(ecs_id(Networked), EcsWildcard) },
    },
  });

  ecs_system_init(world, &(ecs_system_desc_t){
      .entity = ecs_entity(world, {
          .name = "handle_networking",
          .add = { ecs_dependson(POutputs) },
      }),
      .ctx = queries,
      .callback = handle_networking,
      .interval = (float)1 / 64,
  });
}
