#pragma once

#include "flecs.h"
#include "common.h"

#ifdef ACTION_H_DECLARE
#define ACTION_EXTERN
#else
#define ACTION_EXTERN extern
#endif

typedef void ActionCallback(ecs_world_t *world, ecs_entity_t ent, void *data);

typedef struct {
  ActionCallback *callback;
  void *data;
}Action;

void ActionsImport(ecs_world_t *world);

ecs_entity_t new_action_(ecs_world_t *world, Action *action);

#define new_action(world, action) new_action_(world, &(Action)action)

ACTION_EXTERN ECS_COMPONENT_DECLARE(Action);
