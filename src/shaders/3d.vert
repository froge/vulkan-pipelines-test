#version 460

#extension GL_EXT_buffer_reference2 : require
#extension GL_EXT_scalar_block_layout : require

layout (location = 0) out vec2 tex_coord_out;
layout (location = 1) out vec3 normal_out;
layout (location = 2) out vec3 pos_out;
layout (location = 3) out flat int draw_index;

struct Vertex {
    vec3 pos;
    vec3 normal;
    vec2 tex_coord;
};

layout (buffer_reference, scalar, buffer_reference_align = 8 * 4) buffer VertexBuffer {
    Vertex vertices[];
};

layout (buffer_reference, scalar) buffer IndexBuffer {
    uint indices[];
};

struct MeshInfo {
    uint verts;
    VertexBuffer vertices;
    IndexBuffer indices;
};

layout (set = 0, binding = 1, scalar) readonly buffer nite {
    MeshInfo meshes[];
};

struct ObjectInfo {
    mat4 model;
    mat3 inv_model;
    uint mesh;
    uint mat;
    uint pad;
};

layout (set = 1, binding = 0, scalar) readonly buffer fort {
    ObjectInfo objects[];
};

layout (set = 2, binding = 0, scalar) uniform ubo0 {
    mat4 proj;
    mat4 view;
    vec3 cam_pos;
};

// layout (push_constant) uniform pc {
// };

const uint stride = 8;

void main () {
    ObjectInfo object = objects[gl_BaseInstance];
    mat4 model = object.model; 
    MeshInfo mesh = meshes[objects[gl_BaseInstance].mesh];

    uint idx = mesh.indices.indices[gl_VertexIndex];

    Vertex vert = mesh.vertices.vertices[idx];
    // vec3 pos = vec3(vert[0].fart, vert[1].fart, vert[2].fart);
    // vec3 normal = vec3(vert[3].fart, vert[4].fart, vert[5].fart);
    // vec2 tex_coord = vec2(vert[6].fart, vert[7].fart);

    // vec3 pos = vert.pos;
    // vec3 normal = vert.normal;
    // vec2 tex_coord = vert.tex_coord;

    gl_Position = proj * view * model * vec4(vert.pos, 1.0);
    // gl_Position.xyz = pos;
    // gl_Position.w = 1.0;
    pos_out = (model * vec4(vert.pos, 1.0)).rgb;
    tex_coord_out = vert.tex_coord;
    // normal_out = mat3(transpose(inverse(model))) * vert.normal;
    normal_out = objects[gl_BaseInstance].inv_model * vert.normal;
    draw_index = gl_BaseInstance;
    // normal_out = normal;
}
