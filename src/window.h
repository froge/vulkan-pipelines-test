#pragma once

#include "flecs.h"
#include "SDL2/SDL.h"

#ifdef WINDOW_H_DECLARE
#define WINDOW_EXTERN
#else
#define WINDOW_EXTERN extern
#endif

typedef struct Window {
    SDL_Window *window;
    ecs_entity_t framebuffer;
}Window;

void WindowingImport(ecs_world_t *world);

WINDOW_EXTERN ECS_COMPONENT_DECLARE(Window);
