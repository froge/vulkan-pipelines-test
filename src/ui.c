#include "SDL_keyboard.h"
#define UI_H_DECLARE
#include "ui.h"

#include <ft2build.h>
#include FT_FREETYPE_H

#include "convars.h"
#include "renderer.h"
#include "vk_mem_alloc.h"
#include "vulkan/vulkan.h"
#include "vulkan.h"
#include "phases.h"

void init_glyphs(ecs_world_t *world) {
  FT_Library ft;
  if (FT_Init_FreeType(&ft)) {
    puts("you chud dont have freetype");
    exit(1);
  }
  FT_Face face;
  if (FT_New_Face(ft, "fonts/liberation.ttf", 0, &face)) {
    printf("font bad \n");
    exit(1);
  }

  FT_Set_Pixel_Sizes(face, 0, 64);

  Glyph *glyphs = malloc(sizeof(Glyph) * 128);

  for (int i = 32; i < 128; i++) {
    if (FT_Load_Char(face, i, FT_LOAD_DEFAULT)) {
      printf("font bad \n");
      exit(1);
    }
    if (FT_Render_Glyph(face->glyph, FT_RENDER_MODE_SDF)) {
      printf("font bad \n");
      exit(1);
    }
    if (face->glyph->bitmap.width) {
      glyphs[i - 32].texture = load_texture2d_from_data(world, face->glyph->bitmap.buffer, face->glyph->bitmap.width, face->glyph->bitmap.rows, 1);
    } else {
      glyphs[i - 32].texture = 0;
    }
    glyphs[i - 32].advance = face->glyph->advance.x;
    glyphs[i - 32].bearing = (vec2s){ { face->glyph->bitmap_left, face->glyph->bitmap_top } };
  }

  ecs_singleton_set(world, Glyphs, { glyphs });
}

vec2s get_anchor_offset(vec2s size, AnchorBits anchor) {
  vec2s offset;
  if (anchor & anchor_left && anchor & anchor_right) {
    offset.x = -(size.x / 2);
  } else if (anchor & anchor_left) {
    offset.x = 0;
  } else if (anchor & anchor_right) {
    offset.x = -size.x;
  }

  if (anchor & anchor_top && anchor & anchor_bottom) {
    offset.y = -(size.y / 2);
  } else if (anchor & anchor_top) {
    offset.y = 0;
  } else if (anchor & anchor_bottom) {
    offset.y = -size.y;
  }
  return offset;
}

#define deltas 1000

// typedef struct {
//   float s[deltas];
// }PastDelta;

// ECS_COMPONENT_DECLARE(PastDelta);

void text_input_change_callback(ecs_world_t *world, ConVar *val) {
  if (val->e) {
    SDL_StartTextInput();
  } else {
    SDL_StopTextInput();
  }
}

void UiImport(ecs_world_t *world) {
  ECS_MODULE(world, Ui);

  ECS_COMPONENT_DEFINE(world, Transform2D);
  ECS_COMPONENT_DEFINE(world, Matrix3);
  ECS_COMPONENT_DEFINE(world, Label);
  ECS_COMPONENT_DEFINE(world, Container);
  ECS_COMPONENT_DEFINE(world, Anchor);
  ECS_COMPONENT_DEFINE(world, AnchorBits);
  ECS_COMPONENT_DEFINE(world, Position2D);
  ECS_COMPONENT_DEFINE(world, Glyphs);
  ECS_COMPONENT_DEFINE(world, Textbox);
  ECS_COMPONENT_DEFINE(world, PixelPosition);
  ECS_COMPONENT_DEFINE(world, PixelContainer);
  // ECS_COMPONENT_DEFINE(world, PastDelta);

  // ECS_SYSTEM(world, set_text, PInput, Label);
  ECS_ENTITY_DEFINE(world, CurrentTextInput, ConVar);
  ecs_set(world, CurrentTextInput, ConVar, { convar_entity, .e = 0, text_input_change_callback });

  init_glyphs(world);
  add_convar(world, "ui_scale", &(ConVar){ convar_float, .f = 1.0 });
}
