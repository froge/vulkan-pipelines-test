#include "SDL_audio.h"
#include "SDL_gamecontroller.h"
#include "SDL_haptic.h"
#include "SDL_hints.h"
#include "SDL_video.h"
#include "cglm/struct/quat.h"
#include "rapier.h"
// #include <cglm/affine-pre.h>
#include <cglm/affine.h>
#include <cglm/call/quat.h>
#include <cglm/call/mat4.h>
// #include <cglm/mat4.h>
#include <cglm/vec3.h>
#include <stdio.h>
#include <stdlib.h>

#include <cglm/call.h>
// #include <cglm/struct.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#include <vulkan/vulkan.h>
#include <vulkan/vulkan_core.h>

#define STB_IMAGE_IMPLEMENTATION
#include <vk_mem_alloc.h>
#include "common.h"

// #include "camera.c"

#include "flecs.h"
#include "miniaudio.h"

#define VK_FUNC(name) _ ## name = (PFN_ ## name)vkGetInstanceProcAddr(instance, #name)

struct ubo0 {
    mat4 proj;
    mat4 view;
    vec3 cam_pos;
    u32 object_count;
    u32 light_count;
};

// #include "components.h"

#include "phases.h"
#include "convars.h"
#include "physics.h"
#include "renderer.h"
#include "vulkan.h"
#include "input.h"
#include "actions.h"
#include "window.h"
#include "sound.h"
#include "gltf.h"

#include "game/player.h"

// #include "networking.h"
#include "ui.h"

#include "scripts.c"

#include "pipelines/pipelines.h"

#include "dds.c"

#define lookup(world, name) ecs_lookup_path_w_sep(world, 0, name, "/", "/", false)


// void teleport_chud(ecs_world_t *world, void *data) {
//     ecs_entity_t block = ecs_lookup(world, (char*)data);
//     Transform3D *pos = ecs_get_mut(world, block, Transform3D);
//     pos->pos.y = 10;
//     ecs_modified(world, block, Transform3D);
// }

void teleportnite(ecs_world_t *world, ecs_entity_t ent, void *data) {
    Transform *transform = ecs_get_mut(world, ent, Transform);
    transform->pos.y = 10;
    ecs_entity_t soundr = lookup(world, "sounds/test.mp3");
    play_sound(world, ent, soundr);
    // teleport_chud(world, (void*)it.entities[i]);
    ecs_modified(world, ent, Transform);
}

void teleportnite_ppu(ecs_world_t *world, ecs_entity_t ent, void *data) {
    ecs_entity_t ent_p = ecs_get_parent(world, ent);
    Transform *transform = ecs_get_mut(world, ent_p, Transform);
    transform->pos.y = 10;
    ecs_entity_t soundr = lookup(world, "sounds/test.mp3");
    play_sound(world, ent_p, soundr);
    // teleport_chud(world, (void*)it.entities[i]);
    ecs_modified(world, ent_p, Transform);
}

struct PlaySoundData {
    u64 player;
    u64 sound;
};

void queue_play_sound(ecs_world_t *world, void *data) {
    struct PlaySoundData *dat = data;
    play_sound(world, dat->player, dat->sound);
    free(data);
}

void throw_block(ecs_world_t *world, void *data) {
    PhysicsSystem *system = ecs_singleton_get_mut(world, PhysicsSystem);
    u64 fortnite = (u64)data;

    Collider *collider =
        rapier_Collider_create_cuboid((RVector3){0.5, 0.5, 0.5}, fortnite / 10.0);

    ecs_entity_t block = ecs_new_id(world);

    ivec2 mouse_posi;
    SDL_GetMouseState(&mouse_posi[0], &mouse_posi[1]);
    vec2 mouse_pos = {mouse_posi[0], mouse_posi[1]};
    Ray ray;

    if (ecs_singleton_get(world, Gamepad)) {
        Camera *cam = ecs_singleton_get(world, Camera);
        Transform *cam_pos = ecs_get(world, ecs_id(Camera), WorldTransform);
        ray.origin = cam_pos->pos;
        glm_vec3_copy(cam->front, ray.direction.raw);
    } else {
        ray = ray_from_mouse_pos(world, mouse_pos);
    }

    RigidBodyOptions options = {
        .type = RigidBodyTypeDynamic,
        .pos = { ray.origin.x + ray.direction.x, ray.origin.y + ray.direction.y, ray.origin.z + ray.direction.z },
        .rotation = {1, 0, 0, 0},
        .linvel = { ray.direction.x * fortnite, ray.direction.y * fortnite, ray.direction.z * fortnite },
        .gravity_scale = 1.0,
        .user_data = block,
    };

    RigidBody *body = rapier_RigidBody_create(&options);
    RigidBodyHandle handle = rapier_RigidBodySet_insert(system->rset, body);
    rapier_ColliderSet_insert_with_parent(system->cset, collider, handle,
                                          system->rset);

    ecs_entity_t cube_mesh = lookup(world, "cube_mesh");

    ecs_entity_t mat;
    if (fortnite == 100) {
        mat = ecs_lookup(world, "red");
    } else {
        if (block % 2) {
          mat = ecs_lookup(world, "grass");
        } else {
          mat = ecs_lookup(world, "stone");
        }
    }
    ecs_set(world, block, Render3D, {mat, cube_mesh, 1});
    ecs_add(world, block, RenderOutline);
    ecs_add(world, block, SoundPlayer);
    ecs_add(world, block, Matrix4);
    ecs_set(world, block, AABB, { { { -0.5, -0.5, -0.5 } }, { { 0.5, 0.5, 0.5 } } });

    if (fortnite == 100) {
        ecs_set(world, block, Light, { light_type_point, { 0, 0, 0 }, { 1, 0, 0 }, 10.0, 0});
    }
    // body = rapier_RigidBodySet_index(system->rset, handle);
    // rapier_RigidBody_set_translation( body, &(RVector3){ray.origin.x + ray.direction.x, ray.origin.y + ray.direction.y, ray.origin.z + ray.direction.z});
    // rapier_RigidBody_set_linvel(body, &(RVector3){ray.direction.x * fortnite, ray.direction.y * fortnite, ray.direction.z * fortnite});

    Transform pos = default_transform();
    pos.pos = (Vector3){{ray.origin.x + ray.direction.x, ray.origin.y + ray.direction.y, ray.origin.z + ray.direction.z}};

    ecs_set_ptr(world, block, Transform, &pos);
    ecs_set(world, block, Velocity, {{ray.direction.x * 10, ray.direction.y * 10, ray.direction.z * 10}});
    // ecs_set(world, block, Rotation, {{1, 0, 0, 0}});
    ecs_set(world, block, PhysicsBody, {handle});
    // ecs_entity_t soundr = ecs_lookup(world, "sounds/test.mp3");
    // play_sound(world, block, soundr);
    // struct PlaySoundData *sounddata = malloc(sizeof(struct PlaySoundData));
    // sounddata->player = block;
    // sounddata->sound = soundr;
    // ecs_run_post_frame(world, queue_play_sound, sounddata);
}

void mouse_captured_callback(ecs_world_t *world, ConVar *val) {
    SDL_SetRelativeMouseMode(val->i);
}

void timescale_set_action(ecs_world_t *world, void *ctx) {
    ecs_set_time_scale(world, *(float*)&ctx);
    physics_set_timescale(world, *(float*)&ctx);
}

void timescale_changed_callback(ecs_world_t *world, ConVar *val) {
    ecs_run_post_frame(world, timescale_set_action, *(void**)&val->f);
}

// ecs_entity_t load_file(ecs_world_t *world, char *filename) {    
//     FILE *file = fopen(filename, "rb");
//     u64 len;
//     fseek(file, 0, SEEK_END);
//     len = ftell(file);
//     rewind(file);
//     void *data = malloc(len);
//     fread(data, len, 1, file);
//     fclose(file);
//     return ecs_set(world, 0, File, {data, len});
// }

void select_objects(ecs_world_t *world, void *data) {
    ecs_filter_t *filter = ecs_filter_init(world, &(ecs_filter_desc_t){
        .terms = { 
            { ecs_id(CollisionMesh) }, 
            { ecs_id(Matrix4) }, 
            { ecs_id(Selectable) } 
        } 
    });
    ecs_iter_t iter = ecs_filter_iter(world, filter);
    ecs_iter_t *it = &iter;
    float best_dist = INFINITY;
    Selectable *best_sel = NULL;
    ecs_entity_t best = 0;
    while (ecs_iter_next(it)) {
        CollisionMesh *meshes = ecs_field(it, CollisionMesh, 1);
        Matrix4 *transforms = ecs_field(it, Matrix4, 2);
        Selectable *sels = ecs_field(it, Selectable, 3);
        for (int i = 0; i < it->count; i++) {
            CollisionMesh *cmesh = &meshes[i];
            Matrix4 *transform = &transforms[i];
            Selectable *sel = &sels[i];
            float dist;
            ivec2 mouse_posi;
            SDL_GetMouseState(&mouse_posi[0], &mouse_posi[1]);
            vec2 mouse_pos = { mouse_posi[0], mouse_posi[1] };
            Ray ray = ray_from_mouse_pos(world, mouse_pos);
            if (intersect_mesh(ray, (CollisionMesh*)cmesh, *transform, &dist)) {
                if (dist < best_dist) {
                    best_dist = dist;
                    best = it->entities[i];
                    best_sel = sel;
                }
            }
        }
    }
    if (best) {
        const Action *action = ecs_get(it->world, best_sel->action, Action);
        action->callback(world, best, action->data);
        // ecs_set(world, 0, NetworkEvent, { network_event_action, best, { best_sel->action } });
    }
}


void convar_int_plusone(ecs_world_t *world, void *data) {
    ecs_entity_t mousee = ecs_lookup(world, (char*)data);
    ConVar *mouse = ecs_get_mut(world, mousee, ConVar);
    mouse->i += 1.0;
    if (mouse->set_callback) {
        mouse->set_callback(world, mouse);
    }
    ecs_modified(world, mousee, ConVar);
}

void convar_int_minusone(ecs_world_t *world, void *data) {
    ecs_entity_t mousee = ecs_lookup(world, (char*)data);
    ConVar *mouse = ecs_get_mut(world, mousee, ConVar);
    mouse->i -= 1.0;
    if (mouse->set_callback) {
        mouse->set_callback(world, mouse);
    }
    ecs_modified(world, mousee, ConVar);
}

void convar_float_plusone(ecs_world_t *world, void *data) {
    ecs_entity_t mousee = ecs_lookup(world, (char*)data);
    ConVar *mouse = ecs_get_mut(world, mousee, ConVar);
    mouse->f += 1.0;
    if (mouse->set_callback) {
        mouse->set_callback(world, mouse);
    }
    ecs_modified(world, mousee, ConVar);
}

void convar_float_minusone(ecs_world_t *world, void *data) {
    ecs_entity_t mousee = ecs_lookup(world, (char*)data);
    ConVar *mouse = ecs_get_mut(world, mousee, ConVar);
    mouse->f -= 1.0;
    if (mouse->set_callback) {
        mouse->set_callback(world, mouse);
    }
    ecs_modified(world, mousee, ConVar);
}

int check_family_properties(VkPhysicalDevice dev, u32 *fam) {
    u32 count;
    vkGetPhysicalDeviceQueueFamilyProperties(dev, &count, NULL);
    VkQueueFamilyProperties *props = malloc(sizeof(VkQueueFamilyProperties) * count);
    vkGetPhysicalDeviceQueueFamilyProperties(dev, &count, props);
    for (int i = 0; i < count; i++) {
        if (props[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
            *fam = i;
            return 1;
        }
    }
    return 0;
}

// void camera_ray(ecs_iter_t *it) {
//     Transform3D *transforms = ecs_field(it, Transform3D, 1);
//     CollisionMesh *meshes = ecs_field(it, CollisionMesh, 2);
//     Camera *cam = ecs_singleton_get_mut(it->world, Camera);
//     for (int i = 0; i < it->count; i++) {
//         Transform3D *transform = &transforms[i];
//         CollisionMesh *mesh = &meshes[i];
//         float dist;
//         ivec2 mouse_posi;
//         SDL_GetMouseState(&mouse_posi[0], &mouse_posi[1]);
//         vec2 mouse_pos = { mouse_posi[0], mouse_posi[1] };
//         Ray ray = ray_from_mouse_pos(it->world, mouse_pos);
//         if (intersect_mesh(ray, mesh, transform->transform, &dist)) {
//             printf("intersected with %s\n", ecs_get_name(it->world, it->entities[i]));
//         }
//     }
//     ecs_singleton_modified(it->world, Camera);
// }

void move_scaled(ecs_world_t *world, AxisValue value, void *data) {
    Player *player = ecs_singleton_get_mut(world, Player);
    DeltaTime delta = *ecs_singleton_get(world, DeltaTime);
    // float speed = 10 * delta;
    float x =  value.a0;
    float y = -value.a1;

    player->desired_dir.x = x;
    player->desired_dir.z = y;

    // move_z(cam, &cam->pos, y);
    // move_x(cam, &cam->pos, x);
}

void look_scaled_gyro(ecs_world_t *world, AxisValue value, void *data) {
    Camera *cam = ecs_singleton_get_mut(world, Camera);
    DeltaTime delta = *ecs_singleton_get(world, DeltaTime);
    float speed = -200.0 * delta;
    float y = speed * -value.a0;
    float x = speed *  value.a1;

    cam_rot_pitch(cam, y);
    cam_rot_yaw  (cam, x);
}

void look_scaled(ecs_world_t *world, AxisValue value, void *data) {
    Camera *cam = ecs_singleton_get_mut(world, Camera);
    DeltaTime delta = *ecs_singleton_get(world, DeltaTime);
    float speed = 200.0 * delta;
    float x = speed *  value.a0;
    float y = speed * -value.a1;

    cam_rot_pitch(cam, y);
    cam_rot_yaw  (cam, x);
}
void make_chud(ecs_world_t *world, AxisValue value, void *data) {
    Transform *transform = ecs_get_mut(world, ecs_lookup(world, data), Transform);
    // printf("%f\n", value.a0);
    transform->pos.y = 10 * value.a0;
}

void set_text(ecs_iter_t *it) {
  const ConVar *timescale = ecs_get(it->world, ecs_lookup(it->world, "timescale"), ConVar);

  Label *label = ecs_get_mut(it->world, (ecs_entity_t)it->ctx, Label);
  sprintf(label->text, "FPS: %.0f", 1.0 / (it->delta_time / timescale->f));
  // sprintf(label->text, "frametime: %.3fms", (it->delta_time / timescale->f * 1000.0));
}

void set_rumble(ecs_world_t *world, void *data) {
    Gamepad *gamepad = ecs_singleton_get(world, Gamepad);
    SDL_GameControllerRumble(gamepad->controller, 0xffff, 10, 100);
}

void audio_callback(void *user_data, u8 *buffer, int buffer_size) {
    u32 size_in_frames = (u32)buffer_size / ma_get_bytes_per_frame(ma_format_f32, ma_engine_get_channels(user_data));
    ma_engine_read_pcm_frames(user_data, buffer, size_in_frames, NULL);
}

void set_convar_float(ecs_world_t *world, void *data) {
    struct SetConvarFloat {
        char *name;
        float value;
    };
    struct SetConvarFloat *set = data;
    ConVar *cv = ecs_get_mut(world, ecs_lookup(world, set->name), ConVar);
    cv->f = set->value;
}

void backspace_textbox(ecs_world_t *world, void *data) {
    ConVar *current = ecs_get_mut(world, CurrentTextInput, ConVar);
    if (!current->e) {
        return;
    }
    Textbox *box = ecs_get(world, current->e, Textbox);
    Label *label = ecs_get_mut(world, box->label, Label);
    label->text[strlen(label->text) - 1] = 0;
    ecs_modified(world, box->label, Label);
}

void deselect_textbox(ecs_world_t *world, void *data) {
    ecs_entity_t fart = (ecs_entity_t)data;
    action_set_disable(world, fart);
    ConVar *current = ecs_get_mut(world, CurrentTextInput, ConVar);
    current->e = 0;
    current->set_callback(world, current);
    ecs_modified(world, CurrentTextInput, ConVar);
}

void select_textbox(ecs_world_t *world, void *data) {
    ecs_entity_t fart = (ecs_entity_t)data;
    action_set_enable(world, fart);
    ecs_entity_t fortnite = ecs_lookup(world, "baller");
    ConVar *current = ecs_get_mut(world, CurrentTextInput, ConVar);
    current->e = fortnite;
    current->set_callback(world, current);
    ecs_modified(world, CurrentTextInput, ConVar);

    // ecs_query_t *query = data;
    // ecs_iter_t iter = ecs_query_iter(world, query);
    // ecs_iter_t *it = &iter;
    // int x, y;
    // SDL_GetMouseState(&x, &y);
    // while (ecs_iter_next(it)) {
    //     Textbox *boxes = ecs_field(it, Textbox, 1);
    //     Position2D *poss = ecs_field(it, Position2D, 2);
    //     Container *containers = ecs_field(it, Container, 3);
    //     for (int i = 0; i < it->count; i++) {
    //         Textbox *box = &boxes[i];
    //         Position2D *pos = &poss[i];
    //         Container *container = &containers[i];
    //     }
    // }
}

void bully_light_val(ecs_world_t *world, struct AxisValue val, void *data) {
    ecs_entity_t light_e = (u64)data;
    Light *light = ecs_get_mut(world, light_e, Light);
    light->strength += val.a0 * 0.5;
    ecs_modified(world, light_e, Light);
}

int main(int argc, char **argv) {
    ecs_world_t *world = ecs_init();
    // printf("%s\n", NULL);

    ecs_set_entity_range(world, 0, 1000);

    // ecs_singleton_set(world, EcsRest, {0});
    // ECS_IMPORT(world, FlecsMonitor);

    ECS_IMPORT(world, Phases);
    ECS_IMPORT(world, Convars);
    ECS_IMPORT(world, Input);
    ECS_IMPORT(world, Actions);
    ECS_IMPORT(world, Physics);
    ECS_IMPORT(world, Renderer);
    ECS_IMPORT(world, Vulkan);
    ECS_IMPORT(world, Windowing);
    ECS_IMPORT(world, SoundM);
    // ECS_IMPORT(world, Networking);
    ECS_COMPONENT_DEFINE(world, Selected);
    ECS_COMPONENT_DEFINE(world, Player);
    ecs_singleton_set(world, DeltaTime, { 1.0 / 11.0 });

    // ecs_filter_t *filter_ = ecs_filter(world, {
    //     .terms = {
    //         { .id = ecs_id(EcsComponent) },
    //     },
    // });
    // ecs_iter_t tier = ecs_filter_iter(world, filter_);
    // while (ecs_iter_next(&tier)) {
    //     for (int i = 0; i < tier.count; i++) {
    //         puts(ecs_get_name(world, tier.entities[i]));
    //     }
    // }

    // if (argc > 1) {
    //     networking_init(world);
    //     networking_init_client(world, argv[1]);
    //     ECS_TAG(world, Chud);
    // } else {
        // networking_init(world);
        // networking_init_server(world);
    // }

    add_convar(world, "networking_sequence_id", &(ConVar){convar_int, .i = 0});
    add_convar(world, "networking_send_sequence_id",
               &(ConVar){convar_int, .i = 0});
    // ECS_SYSTEM(world, camera_ray, EcsOnUpdate, Transform3D, CollisionMesh);
    add_convar(world, "speed", &(ConVar){ convar_float, .f = 5.0 });
    add_convar(world, "gravity", &(ConVar){ convar_float, .f = 9.81 });
    add_convar(world, "resized", &(ConVar){ convar_bool, .i = 0 });
    add_convar(world, "teleport", &(ConVar){ convar_bool, .i = 0 });
    add_convar(world, "mouse_captured", &(ConVar){ convar_bool, .i = 0, .set_callback = &mouse_captured_callback});
    add_convar(world, "vsync", &(ConVar){ convar_bool, .i = 1, .set_callback = &toggle_vsync});
    add_convar(world, "among", &(ConVar){ convar_bool, .i = 0 });
    ecs_entity_t timescale_e = add_convar(world, "timescale", &(ConVar){ convar_float, .f = 1.0, .set_callback = &timescale_changed_callback });
    ecs_entity_t quit_e = add_convar(world, "quit", &(ConVar){ convar_bool, .i = 0 });

    // ECS_OBSERVER(world, delete_textures, EcsOnDelete, Texture);
    SDL_SetHint(SDL_HINT_VIDEO_MINIMIZE_ON_FOCUS_LOSS, 0);

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
    // SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_GAMECONTROLLER);

    SDL_Window *window = SDL_CreateWindow("among us gamer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1280, 800, SDL_WINDOW_VULKAN | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_RESIZABLE);


    // SDL_Vulkan_LoadLibrary(NULL);

    // ecs_entity_t _ecs_pipeline = ecs_pipeline(world, {});

    ecs_singleton_set(world, Window, { window });

    ma_engine_config config = {};
    config.noDevice = MA_TRUE;
    config.channels = 2;
    config.sampleRate = 48000;
    
    ma_engine engine;
    if (ma_engine_init(&config, &engine) != MA_SUCCESS) {
        puts("chud");
        exit(1);
    }

    ecs_singleton_set_ptr(world, ma_engine, &engine);

    SDL_AudioSpec desired_spec = {
        ma_engine_get_sample_rate(&engine),
        AUDIO_F32,
        ma_engine_get_channels(&engine),
        0,
        512,
        0,
        0,
        audio_callback,
        // NULL,
        &engine,        
    };
    SDL_AudioSpec obtained_spec = {};
    SDL_AudioDeviceID audio_device = SDL_OpenAudioDevice(NULL, 0, &desired_spec, &obtained_spec, SDL_AUDIO_ALLOW_ANY_CHANGE);
    if (!audio_device) {
        puts("no more sound\n");
        exit(1);
    }
    SDL_PauseAudioDevice(audio_device, 0);

    // play_test(&engine);
    // ma_sound sound;
    // ma_sound_init_from_file(&engine, "test.mp3", 0, NULL, NULL, &sound);
    // ma_sound_start(&sound);
    // ecs_singleton_set(world, SoundEngine, {engine});

    load_sound(world, "sounds/test.mp3");

    
    u32 jort;
    vkEnumerateInstanceVersion(&jort);

    char *extra_exts[] = {"VK_KHR_get_physical_device_properties2", "VK_KHR_display"};
    int extra_ext_count = 1;

    u32 exts_count;
    SDL_Vulkan_GetInstanceExtensions(window, &exts_count, NULL);

    char **exts = malloc(sizeof(char*) * (exts_count + extra_ext_count));

    SDL_Vulkan_GetInstanceExtensions(window, &exts_count, (const char**)exts);

    for (int i = 0; i < extra_ext_count; i++) {
        exts[i + exts_count] = extra_exts[i];
    }

    exts_count += extra_ext_count;

    for (int i = 0; i < exts_count; i++) {
        // printf("%s\n", exts[i]);
    }


    VkApplicationInfo app_info = {0};
    app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    app_info.pApplicationName = "vulkan test";
    app_info.applicationVersion = 10;
    app_info.apiVersion = VK_API_VERSION_1_3;
    app_info.pEngineName = "test engine";

    const char *layers[] = {"VK_LAYER_KHRONOS_validation"};

    VkInstanceCreateInfo instance_info = {0};
    instance_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instance_info.enabledExtensionCount = exts_count;
    instance_info.ppEnabledExtensionNames = (const char* const*)exts;
    instance_info.enabledLayerCount = 0;
    instance_info.ppEnabledLayerNames = (const char* const*)layers;
    instance_info.pApplicationInfo = &app_info;

    VkInstance instance;
    VkResult res;
    res = vkCreateInstance(&instance_info, NULL, &instance);
    if (res != VK_SUCCESS) {
        printf("instance could not be created: %i\n", res);
        exit(1);
    }

    VK_FUNC(vkCmdBeginRenderingKHR);
    VK_FUNC(vkCmdEndRenderingKHR);

    printf("Vulkan instance version: %i.%i.%i\n", VK_VERSION_MAJOR(jort), VK_VERSION_MINOR(jort), VK_VERSION_PATCH(jort));

    u32 dev_count;
    vkEnumeratePhysicalDevices(instance, &dev_count, NULL);

    VkPhysicalDevice *physdevs = malloc(sizeof(VkPhysicalDevice) * dev_count);

    vkEnumeratePhysicalDevices(instance, &dev_count, physdevs);

    int chosen_dev = 0;
    int chosen_type = -1;
    int chosen_family = -1;

    for (int i = 0; i < dev_count; i++) {
        VkPhysicalDeviceProperties props;
        vkGetPhysicalDeviceProperties(physdevs[i], &props);
        u32 fam;
        if (!check_family_properties(physdevs[i], &fam)) {
            continue;
        }
        if (chosen_type == -1) {
            chosen_type = props.deviceType;
            chosen_family = fam;
        } else if (chosen_type != VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU && props.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU) {
            chosen_dev = i;
            chosen_type = props.deviceType;
            chosen_family = fam;
        }
        printf("found %s device %i: %s\n", (props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) ? "fast" : "slow", i, props.deviceName);
    }

    if (chosen_family == -1) {
        printf("no compatible device found\n");
        exit(1);
    }

    // chosen_dev = 1;

    printf("chose device %i\n", chosen_dev);

    ecs_singleton_set_ptr(world, VkPhysicalDevice, &physdevs[chosen_dev]);

    VkDeviceQueueCreateInfo queue_info = {0};
    queue_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queue_info.queueFamilyIndex = chosen_family;
    queue_info.queueCount = 1;
    float prio = 1.0;
    queue_info.pQueuePriorities = &prio;

    // VkPhysicalDeviceDynamicRenderingFeaturesKHR dynamic_rendering_feature = {0};
    // dynamic_rendering_feature.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DYNAMIC_RENDERING_FEATURES_KHR;
    // dynamic_rendering_feature.dynamicRendering = VK_TRUE;
    // dynamic_rendering_feature.pNext = NULL;

    VkPhysicalDeviceFeatures features = {
        .multiDrawIndirect = true,
        .drawIndirectFirstInstance = true,
        .samplerAnisotropy = true,
        // .sparseBinding = VK_TRUE,
        // .sparseResidencyBuffer = VK_TRUE,
        // .sparseResidencyAliased = VK_TRUE,
    };

    VkPhysicalDeviceVulkan12Features v12f = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES,
        .bufferDeviceAddress = true,
        .descriptorIndexing = true,
        .shaderSampledImageArrayNonUniformIndexing = true,
        .runtimeDescriptorArray = true,
        .scalarBlockLayout = true,
    };

    VkPhysicalDeviceVulkan11Features v11f = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES,
        .pNext = &v12f,
        .shaderDrawParameters = true,
    };

    VkPhysicalDeviceFeatures2 features2 = {0};
    features2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
    features2.pNext = &v11f;
    features2.features = features;



    VkDeviceCreateInfo device_info = {0};
    device_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    device_info.pNext = &features2;
    // device layers are deprecated
    // no device specific extensions used yet
    device_info.enabledExtensionCount = 1;
    // char *ext[7] = {"VK_KHR_dynamic_rendering", "VK_KHR_depth_stencil_resolve", "VK_KHR_create_renderpass2", "VK_KHR_multiview", "VK_KHR_maintenance2", "VK_KHR_swapchain", "VK_KHR_maintenance1"};
    char *ext[1] = {"VK_KHR_swapchain"};
    device_info.ppEnabledExtensionNames = (const char* const*)ext;
    device_info.queueCreateInfoCount = 1;
    device_info.pQueueCreateInfos = &queue_info;

    VkDevice dev;
    res = vkCreateDevice(physdevs[chosen_dev], &device_info, NULL, &dev);
    if (res != VK_SUCCESS) {
        printf("device could not be created: %i\n", res);
        exit(1);
    }

    // ecs_entity_t dev_e = ecs_new_id(world);
    ecs_singleton_set(world, VkDevice, {dev});

    VmaAllocatorCreateInfo allocator_create_info = {0};
    allocator_create_info.physicalDevice = physdevs[chosen_dev];
    allocator_create_info.device = dev;
    allocator_create_info.instance = instance;
    allocator_create_info.vulkanApiVersion = VK_API_VERSION_1_2;
    allocator_create_info.flags = VMA_ALLOCATOR_CREATE_BUFFER_DEVICE_ADDRESS_BIT;
    VmaAllocator allocator;
    
    vmaCreateAllocator(&allocator_create_info, &allocator);

    ecs_singleton_set(world, VmaAllocator, {allocator});

    allocator = *ecs_singleton_get_mut(world, VmaAllocator);

    VkCommandPoolCreateInfo command_pool_info = {0};
    command_pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    command_pool_info.queueFamilyIndex = chosen_family;
    command_pool_info.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    VkCommandPool pool;
    res = vkCreateCommandPool(dev, &command_pool_info, NULL, &pool);
    if (res != VK_SUCCESS) {
        printf("command pool could not be created: %i\n", res);
        exit(1);
    }

    VkCommandBufferAllocateInfo cmd_info = {0};
    cmd_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    cmd_info.commandPool = pool;
    cmd_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    cmd_info.commandBufferCount = 1;

    VkCommandBuffer cmd;
    res = vkAllocateCommandBuffers(dev, &cmd_info, &cmd);
    if (res != VK_SUCCESS) {
        printf("command buffer could not be created: %i\n", res);
        exit(1);
    }

    ecs_singleton_set(world, VkCommandBuffer, { cmd });

    VkSurfaceKHR surf;
    SDL_Vulkan_CreateSurface(window, instance, &surf);

    int w, h;

    VkSurfaceCapabilitiesKHR caps;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physdevs[chosen_dev], surf, &caps);

    w = caps.currentExtent.width;
    h = caps.currentExtent.height;

    if (w == -1) {
        SDL_Vulkan_GetDrawableSize(window, &w, &h);
    }

    VkQueue queue;
    vkGetDeviceQueue(dev, chosen_family, 0, &queue);  // TODO

    ecs_singleton_set(world, VkQueue, {queue});
 
    // VkImage chudchain->depth_image;
    // VmaAllocation chudchain->depth_allocation;
    VkSurfaceFormatKHR format;
    u32 format_count = 0;
    vkGetPhysicalDeviceSurfaceFormatsKHR(physdevs[chosen_dev], surf, &format_count, NULL); // TODO choose best format from list
    VkSurfaceFormatKHR *formats = malloc(sizeof(VkSurfaceFormatKHR) * format_count);
    vkGetPhysicalDeviceSurfaceFormatsKHR(physdevs[chosen_dev], surf, &format_count, formats); // TODO choose best format from list

    format = formats[0];
    for (int i = 0; i < format_count; i++) {
        if (formats[i].format == VK_FORMAT_R8G8B8A8_SRGB || formats[i].format == VK_FORMAT_A8B8G8R8_SRGB_PACK32 || formats[i].format == VK_FORMAT_B8G8R8A8_SRGB) {
            format = formats[i];
            break;
        }
    }
    printf("format: %i\n", format.format);

    const ConVar *vsync = ecs_get(world, ecs_lookup(world, "vsync"), ConVar);
    
    VkSwapchainCreateInfoKHR swapchain_info = {0};
    swapchain_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swapchain_info.surface = surf;
    swapchain_info.minImageCount = caps.minImageCount;
    swapchain_info.imageFormat = format.format;
    swapchain_info.imageColorSpace = format.colorSpace;
    swapchain_info.imageExtent = (VkExtent2D) { w, h };
    swapchain_info.imageArrayLayers = 1;
    swapchain_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    swapchain_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    swapchain_info.queueFamilyIndexCount = 1;
    swapchain_info.pQueueFamilyIndices = (u32*)&chosen_family;
    swapchain_info.preTransform = caps.currentTransform;
    swapchain_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    if (vsync->i) {
        swapchain_info.presentMode = VK_PRESENT_MODE_FIFO_KHR;
    } else {
        swapchain_info.presentMode = VK_PRESENT_MODE_MAILBOX_KHR;
    }
    swapchain_info.clipped = VK_TRUE;
    swapchain_info.oldSwapchain = VK_NULL_HANDLE;

    Swapchain chudchain_ = {0};
    Swapchain *chudchain = &chudchain_;

    res = vkCreateSwapchainKHR(dev, &swapchain_info, NULL, &chudchain->swapchain);

    vkGetSwapchainImagesKHR(dev, chudchain->swapchain, &chudchain->count, NULL);
    chudchain->images = malloc(chudchain->count * sizeof(VkImage));
    vkGetSwapchainImagesKHR(dev, chudchain->swapchain, &chudchain->count, chudchain->images);

    // printf("opacity: %i\n", SDL_SetWindowOpacity(window, 0.0));

    VkImageViewCreateInfo view_info = {0};
    view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
    view_info.format = format.format;
    view_info.subresourceRange = (VkImageSubresourceRange) {
        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .baseMipLevel = 0,
        .levelCount = 1,
        .baseArrayLayer = 0,
        .layerCount = 1 
    };

    chudchain->views = malloc(chudchain->count * sizeof(VkImageView));

    for (int i = 0; i < chudchain->count; i++) {
        view_info.image = chudchain->images[i];
        res = vkCreateImageView(dev, &view_info, NULL, &chudchain->views[i]);
        if (res != VK_SUCCESS) {
            printf("swapchain image views could not be created: %i\n", res);
            exit(1);
        }
    }

    chudchain->info = swapchain_info;

    VkSemaphoreCreateInfo semaphore_info = {0};
    semaphore_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fence_info = {0};
    fence_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fence_info.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    vkCreateSemaphore(dev, &semaphore_info, NULL, &chudchain->image_ready); // TODO
    vkCreateSemaphore(dev, &semaphore_info, NULL, &chudchain->render_done_s); // TODO

    vkCreateFence(dev, &fence_info, NULL, &chudchain->render_done); // TODO

    chudchain->depth = create_fb_texture(world, 1920, 1080, FBTEX_DEPTH);
    chudchain->cur_swap_image = 0;

    ecs_singleton_set_ptr(world, Swapchain, chudchain);

    u32 cur_swap_image = 0;


    chudchain = ecs_singleton_get_mut(world, Swapchain);

    // PFN_vkCmdBeginRenderingKHR _vkCmdBeginRenderingKHR = (PFN_vkCmdBeginRenderingKHR)vkGetInstanceProcAddr(instance, "vkCmdBeginRenderingKHR");
    // PFN_vkCmdEndRenderingKHR _vkCmdEndRenderingKHR = (PFN_vkCmdEndRenderingKHR)vkGetInstanceProcAddr(instance, "vkCmdEndRenderingKHR");

    VmaAllocationCreateInfo alloc_info = {0};
    alloc_info.usage = VMA_MEMORY_USAGE_AUTO;
    alloc_info.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT | VMA_ALLOCATION_CREATE_MAPPED_BIT;

    VkBufferCreateInfo buffer_info = {0};
    buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    buffer_info.size = sizeof(struct ubo0);
    buffer_info.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;

    VkBuffer buffer;
    VmaAllocation uniform_allocation;
    VmaAllocationInfo uniform_info;
    vmaCreateBuffer(allocator, &buffer_info, &alloc_info, &buffer, &uniform_allocation, &uniform_info);

    float verts[][4] = {
        { 1.0,  1.0, 1.0, 0.0 },
        {-1.0,  1.0, 0.0, 0.0 },
        {-1.0, -1.0, 0.0, 1.0 },
        { 1.0,  1.0, 1.0, 0.0 },
        {-1.0, -1.0, 0.0, 1.0 },
        { 1.0, -1.0, 1.0, 1.0 }
    };

    u32 indices[] = {
        // back
        0, 1, 2,
        3, 0, 2,

        // front
        4, 5, 6,
        5, 7, 6,

        // bottom
        8, 9,  10,
        8, 10, 11,        

        // top
        12, 13, 14,
        13, 15, 14,

        // left
        16, 17, 18,
        19, 16, 18,

        // right
        20, 21, 22,
        21, 23, 22,        
    };

    struct Vertex vertices[] = {
        // back
        {{-0.5, -0.5, -0.5}, {0.0, 0.0, -1.0}, {0.0, 0.0}}, //0.0, // bottom left
        {{-0.5,  0.5, -0.5}, {0.0, 0.0, -1.0}, {0.0, 1.0}}, //0.0, // top left
        {{ 0.5,  0.5, -0.5}, {0.0, 0.0, -1.0}, {1.0, 1.0}}, //0.0, // top right
        {{ 0.5, -0.5, -0.5}, {0.0, 0.0, -1.0}, {1.0, 0.0}}, //0.0, // bottom right
        // {{-0.5, -0.5, -0.5}, {0.0, 0.0, -1.0}, {0.0, 0.0}}, //0.0, // bottom left
        // {{ 0.5,  0.5, -0.5}, {0.0, 0.0, -1.0}, {1.0, 1.0}}, //0.0, // top right

        // front
        {{-0.5,  0.5,  0.5}, {0.0, 0.0, 1.0}, {0.0, 1.0}}, //0.0, // top left
        {{-0.5, -0.5,  0.5}, {0.0, 0.0, 1.0}, {0.0, 0.0}}, //0.0, // bottom left
        {{ 0.5,  0.5,  0.5}, {0.0, 0.0, 1.0}, {1.0, 1.0}}, //0.0, // top right
        // {{-0.5, -0.5,  0.5}, {0.0, 0.0, 1.0}, {0.0, 0.0}}, //0.0, // bottom left
        {{ 0.5, -0.5,  0.5}, {0.0, 0.0, 1.0}, {1.0, 0.0}}, //0.0, // bottom right
        // {{ 0.5,  0.5,  0.5}, {0.0, 0.0, 1.0}, {1.0, 1.0}}, //0.0, // top right

        // bottom
        {{-0.5, -0.5,  0.5}, {0.0, -1.0, 0.0}, {0.0, 0.0}}, //0.0, // bottom left
        {{-0.5, -0.5, -0.5}, {0.0, -1.0, 0.0}, {0.0, 1.0}}, //0.0, // top left
        {{ 0.5, -0.5, -0.5}, {0.0, -1.0, 0.0}, {1.0, 1.0}}, //0.0, // top right
        // {{-0.5, -0.5,  0.5}, {0.0, -1.0, 0.0}, {0.0, 0.0}}, //0.0, // bottom left
        // {{ 0.5, -0.5, -0.5}, {0.0, -1.0, 0.0}, {1.0, 1.0}}, //0.0, // top right
        {{ 0.5, -0.5,  0.5}, {0.0, -1.0, 0.0}, {1.0, 0.0}}, //0.0, // bottom right

        // top
        {{-0.5,  0.5, -0.5}, {0.0, 1.0, 0.0}, {0.0, 1.0}}, //0.0, // top left
        {{-0.5,  0.5,  0.5}, {0.0, 1.0, 0.0}, {0.0, 0.0}}, //0.0, // bottom left
        {{ 0.5,  0.5, -0.5}, {0.0, 1.0, 0.0}, {1.0, 1.0}}, //0.0, // top right
        // {{-0.5,  0.5,  0.5}, {0.0, 1.0, 0.0}, {0.0, 0.0}}, //0.0, // bottom left
        {{ 0.5,  0.5,  0.5}, {0.0, 1.0, 0.0}, {1.0, 0.0}}, //0.0, // bottom right
        // {{ 0.5,  0.5, -0.5}, {0.0, 1.0, 0.0}, {1.0, 1.0}}, //0.0, // top right

        // left
        {{-0.5, -0.5,  0.5}, {-1.0, 0.0, 0.0}, {0.0, 0.0}}, //0.0, // bottom left
        {{-0.5,  0.5,  0.5}, {-1.0, 0.0, 0.0}, {0.0, 1.0}}, //0.0, // top left
        {{-0.5,  0.5, -0.5}, {-1.0, 0.0, 0.0}, {1.0, 1.0}}, //0.0, // top right
        {{-0.5, -0.5, -0.5}, {-1.0, 0.0, 0.0}, {1.0, 0.0}}, //0.0, // bottom right
        // {{-0.5, -0.5,  0.5}, {-1.0, 0.0, 0.0}, {0.0, 0.0}}, //0.0, // bottom left
        // {{-0.5,  0.5, -0.5}, {-1.0, 0.0, 0.0}, {1.0, 1.0}}, //0.0, // top right

        // right
        {{ 0.5,  0.5,  0.5}, {1.0, 0.0, 0.0}, {0.0, 1.0}}, //0.0, // top left
        {{ 0.5, -0.5,  0.5}, {1.0, 0.0, 0.0}, {0.0, 0.0}}, //0.0, // bottom left
        {{ 0.5,  0.5, -0.5}, {1.0, 0.0, 0.0}, {1.0, 1.0}}, //0.0, // top right
        // {{ 0.5, -0.5,  0.5}, {1.0, 0.0, 0.0}, {0.0, 0.0}}, //0.0, // bottom left
        {{ 0.5, -0.5, -0.5}, {1.0, 0.0, 0.0}, {1.0, 0.0}}, //0.0, // bottom right
        // {{ 0.5,  0.5, -0.5}, {1.0, 0.0, 0.0}, {1.0, 1.0}}, //0.0, // top right
    };

    CollisionMesh mesh = {
        sizeof(vertices) / sizeof(struct Vertex),
        malloc(mesh.verts * sizeof(vec3s)),
    };

    for (int i = 0; i < mesh.verts; i++) {
        mesh.data[i].x = vertices[i].pos[0];
        mesh.data[i].y = vertices[i].pos[1];
        mesh.data[i].z = vertices[i].pos[2];
    }

    struct Buffer vert_buf = create_vertex_buffer(world, vertices, sizeof(vertices));
    struct Buffer ind_buf = create_index_buffer(world, indices, sizeof(indices));

    Buffer fullscreen_buf = create_vertex_buffer(world, verts, sizeof(verts));

    ecs_entity_t fullscreen_buf_e = ecs_new_id(world);
    ecs_set_ptr(world, fullscreen_buf_e, Buffer, &fullscreen_buf);

    struct ubo0 uniform = {0};
    // uniform.time = 1;
    // uniform.color[0] = 1;
    // uniform.color[1] = 0;
    // uniform.color[2] = 0;

    // struct ubo0 *uniform_mapped;
    // vmaMapMemory(allocator, uniform_allocation, (void*)&uniform_mapped);

    memcpy(uniform_info.pMappedData, &uniform, sizeof(struct ubo0));

    vmaFlushAllocation(allocator, uniform_allocation, 0, 0);

    struct UniformBuffer uniform_buffer = { buffer, uniform_allocation, uniform_info, uniform_info.pMappedData };

    VkDescriptorPoolSize pool_sizes[] = {
        { .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, .descriptorCount = 2048 },
        { .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,         .descriptorCount = 1   },
        { .type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,         .descriptorCount = 5   }
    };

    VkDescriptorPoolCreateInfo d_pool_info = {0};
    d_pool_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    d_pool_info.maxSets = 4;
    d_pool_info.poolSizeCount = sizeof(pool_sizes) / sizeof(VkDescriptorPoolSize);
    d_pool_info.pPoolSizes = pool_sizes;

    VkDescriptorPool descriptor_pool;
    vkCreateDescriptorPool(dev, &d_pool_info, NULL, &descriptor_pool);

    VkDescriptorSetLayoutBinding global_bindings[] = {
        { 0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,   1024, VK_SHADER_STAGE_FRAGMENT_BIT, NULL },
        { 1, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,              1, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,   NULL },
        { 2, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,              1, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,   NULL }
    };

    VkDescriptorSetLayoutCreateInfo global_set_info = {0};
    global_set_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    global_set_info.bindingCount = 3;
    global_set_info.pBindings = global_bindings;

    VkDescriptorSetLayout global_set_layout;
    vkCreateDescriptorSetLayout(dev, &global_set_info, NULL, &global_set_layout); 

    VkDescriptorSetAllocateInfo global_set_alloc_info = {0};
    global_set_alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    global_set_alloc_info.descriptorPool = descriptor_pool;
    global_set_alloc_info.descriptorSetCount = 1;
    global_set_alloc_info.pSetLayouts = &global_set_layout;

    VkDescriptorSet global_set;
    res = vkAllocateDescriptorSets(dev, &global_set_alloc_info, &global_set);
    if (res != VK_SUCCESS) {
        printf("allocating descriptor sets failed with %i\n", res);
        exit(1);
    }

    ecs_singleton_set_ptr(world, VkDescriptorSet, &global_set);

    VmaAllocationCreateInfo meshes_alloc_info = {0};
    meshes_alloc_info.usage = VMA_MEMORY_USAGE_AUTO;
    meshes_alloc_info.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT | VMA_ALLOCATION_CREATE_MAPPED_BIT;

    VkBufferCreateInfo meshes_buffer_info = {0};
    meshes_buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    meshes_buffer_info.size = sizeof(struct MeshInfo) * 100;
    meshes_buffer_info.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

    VkBuffer meshes_buffer;
    VmaAllocation meshes_allocation;
    VmaAllocationInfo meshes_info;
    vmaCreateBuffer(allocator, &meshes_buffer_info, &meshes_alloc_info, &meshes_buffer, &meshes_allocation, &meshes_info);

    ecs_entity_t cube_mesh = ecs_new_id(world);
    ecs_set(world, cube_mesh, Mesh, { 0, 36, vert_buf, ind_buf });
    ecs_set_name(world, cube_mesh, "cube_mesh");

    struct MeshInfo *info = meshes_info.pMappedData;

    VkBufferDeviceAddressInfo address_info = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO,
        .buffer = vert_buf.buffer,
    };

    info->verts = 36;
    info->vert_buf = vkGetBufferDeviceAddress(dev, &address_info);
    address_info.buffer = ind_buf.buffer;
    info->ind_buf = vkGetBufferDeviceAddress(dev, &address_info);

    vmaFlushAllocation(allocator, meshes_allocation, 0, 0);

    struct MaterialInfo {
        u32 lit;
        u32 tex;
        u32 metallic_roughness_tex;
        vec3 albedo;
        float metallic;
        float roughness;
    };

    VmaAllocationCreateInfo materials_alloc_info = {0};
    materials_alloc_info.usage = VMA_MEMORY_USAGE_AUTO;
    materials_alloc_info.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT | VMA_ALLOCATION_CREATE_MAPPED_BIT;

    VkBufferCreateInfo materials_buffer_info = {0};
    materials_buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    materials_buffer_info.size = sizeof(struct MaterialInfo) * 200;
    materials_buffer_info.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

    VkBuffer materials_buffer;
    VmaAllocation materials_allocation;
    VmaAllocationInfo materials_info;
    vmaCreateBuffer(allocator, &materials_buffer_info, &materials_alloc_info, &materials_buffer, &materials_allocation, &materials_info);

    // bufin.buffer = meshes_buffer;
    // bufin.offset = 0;
    // bufin.range = VK_WHOLE_SIZE;

    // write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    // write.dstSet = global_set;
    // write.dstBinding = 1;
    // write.dstArrayElement = 0;
    // write.descriptorCount = 1;
    // write.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    // write.pBufferInfo = &bufin;

    // vkUpdateDescriptorSets(dev, 1, &write, 0, NULL);

    VkDescriptorSetLayoutBinding frame_bindings[] = {
        { 0, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1, VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_VERTEX_BIT, NULL },
        { 1, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1, VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_VERTEX_BIT, NULL },
        { 2, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1, VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_VERTEX_BIT, NULL },
    };

    VkDescriptorSetLayoutCreateInfo frame_set_info = {0};
    frame_set_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    frame_set_info.bindingCount = 2;
    frame_set_info.pBindings = frame_bindings;

    VkDescriptorSetLayout frame_set_layout;
    vkCreateDescriptorSetLayout(dev, &frame_set_info, NULL, &frame_set_layout); 

    VkDescriptorSetAllocateInfo frame_set_alloc_info = {0};
    frame_set_alloc_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    frame_set_alloc_info.descriptorPool = descriptor_pool;
    frame_set_alloc_info.descriptorSetCount = 1;
    frame_set_alloc_info.pSetLayouts = &frame_set_layout;

    VkDescriptorSet frame_set;
    res = vkAllocateDescriptorSets(dev, &frame_set_alloc_info, &frame_set);
    if (res != VK_SUCCESS) {
        printf("allocating descriptor sets failed with %i\n", res);
        exit(1);
    }

    ecs_entity_t frame_set_e = ecs_set_ptr(world, 0, VkDescriptorSet, &frame_set);

    struct ObjectInfo {
        mat4 model;
        mat3 inv_model;
        u32 mesh;
        u32 mat;
    };

    VmaAllocationCreateInfo objects_alloc_info = {0};
    objects_alloc_info.usage = VMA_MEMORY_USAGE_AUTO;
    objects_alloc_info.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT | VMA_ALLOCATION_CREATE_MAPPED_BIT;

    VkBufferCreateInfo objects_buffer_info = {0};
    objects_buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    objects_buffer_info.size = sizeof(struct ObjectInfo) * 100;
    objects_buffer_info.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

    VkBuffer objects_buffer;
    VmaAllocation objects_allocation;
    VmaAllocationInfo objects_info;
    vmaCreateBuffer(allocator, &objects_buffer_info, &objects_alloc_info, &objects_buffer, &objects_allocation, &objects_info);

    struct LightInfo {
        vec3 pos;
        vec3 color;
        vec3 dir;
        u16 type;
        u16 shadowed;
    };

    VmaAllocationCreateInfo lights_alloc_info = {0};
    lights_alloc_info.usage = VMA_MEMORY_USAGE_AUTO;
    lights_alloc_info.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT | VMA_ALLOCATION_CREATE_MAPPED_BIT;

    VkBufferCreateInfo lights_buffer_info = {0};
    lights_buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    lights_buffer_info.size = sizeof(struct LightInfo) * 100;
    lights_buffer_info.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

    VkBuffer lights_buffer;
    VmaAllocation lights_allocation;
    VmaAllocationInfo lights_info;
    vmaCreateBuffer(allocator, &lights_buffer_info, &lights_alloc_info, &lights_buffer, &lights_allocation, &lights_info);

    struct SceneInfo {
        u32 object_count;   
    };

    VmaAllocationCreateInfo scene_alloc_info = {0};
    scene_alloc_info.usage = VMA_MEMORY_USAGE_AUTO;
    scene_alloc_info.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT | VMA_ALLOCATION_CREATE_MAPPED_BIT;

    VkBufferCreateInfo scene_buffer_info = {0};
    scene_buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    scene_buffer_info.size = sizeof(struct SceneInfo);
    scene_buffer_info.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

    VkBuffer scene_buffer;
    VmaAllocation scene_allocation;
    VmaAllocationInfo scene_info;
    vmaCreateBuffer(allocator, &scene_buffer_info, &scene_alloc_info, &scene_buffer, &scene_allocation, &scene_info);

    VmaAllocationCreateInfo draws_alloc_info = {0};
    draws_alloc_info.usage = VMA_MEMORY_USAGE_AUTO;
    draws_alloc_info.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT | VMA_ALLOCATION_CREATE_MAPPED_BIT;

    VkBufferCreateInfo draws_buffer_info = {0};
    draws_buffer_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    draws_buffer_info.size = sizeof(VkDrawIndirectCommand) * 100;
    draws_buffer_info.usage = VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT;

    VkBuffer draws_buffer;
    VmaAllocation draws_allocation;
    VmaAllocationInfo draws_info;
    vmaCreateBuffer(allocator, &draws_buffer_info, &draws_alloc_info, &draws_buffer, &draws_allocation, &draws_info);

    // void *objects_mapped;
    // vmaMapMemory(allocator, objects_allocation, &objects_mapped);


    VkDescriptorSetLayoutBinding camera_layout_bindings[] = {
        { 0, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,         1, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,   NULL },
    };

    VkDescriptorSetLayoutCreateInfo camera_layout_info = {0};
    camera_layout_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    camera_layout_info.bindingCount = 1;
    camera_layout_info.pBindings = camera_layout_bindings;

    VkDescriptorSetLayout camera_layout;
    vkCreateDescriptorSetLayout(dev, &camera_layout_info, NULL, &camera_layout); 

    VkDescriptorSetAllocateInfo camera_set_info = {0};
    camera_set_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    camera_set_info.descriptorPool = descriptor_pool;
    camera_set_info.descriptorSetCount = 1;
    camera_set_info.pSetLayouts = &camera_layout;

    VkDescriptorSet camera_set;
    res = vkAllocateDescriptorSets(dev, &camera_set_info, &camera_set);
    if (res != VK_SUCCESS) {
        printf("allocating descriptor sets failed with %i\n", res);
        exit(1);
    }

    ecs_entity_t camera_set_e = ecs_set_ptr(world, 0, VkDescriptorSet, &camera_set);
    ecs_set_name(world, camera_set_e, "camera_set");

    VkDescriptorSetLayoutBinding framebuffer_layout_bindings[] = {
        { 0, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 6, VK_SHADER_STAGE_FRAGMENT_BIT, NULL },
    };

    VkDescriptorSetLayoutCreateInfo framebuffer_layout_info = {0};
    framebuffer_layout_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    framebuffer_layout_info.bindingCount = 1;
    framebuffer_layout_info.pBindings = framebuffer_layout_bindings;

    VkDescriptorSetLayout framebuffer_layout;
    vkCreateDescriptorSetLayout(dev, &framebuffer_layout_info, NULL, &framebuffer_layout); 

    VkDescriptorSetAllocateInfo framebuffer_set_info = {0};
    framebuffer_set_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    framebuffer_set_info.descriptorPool = descriptor_pool;
    framebuffer_set_info.descriptorSetCount = 1;
    framebuffer_set_info.pSetLayouts = &framebuffer_layout;

    VkDescriptorSet framebuffer_set;
    res = vkAllocateDescriptorSets(dev, &framebuffer_set_info, &framebuffer_set);
    if (res != VK_SUCCESS) {
        printf("allocating descriptor sets failed with %i\n", res);
        exit(1);
    }

    ecs_entity_t framebuffer_set_e = ecs_set_ptr(world, 0, VkDescriptorSet, &framebuffer_set);
    ecs_set_name(world, framebuffer_set_e, "framebuffer_set");

    MainSets mainsets = {
        .global = ecs_id(VkDescriptorSet),
        .frame = frame_set_e,
        .frame_buf = {
            objects_buffer,
            objects_allocation,
            objects_info,
            objects_info.pMappedData, 
            100 
        },
        .cam = camera_set_e,
        .fb = framebuffer_set_e,
        .draws_buf = {
            draws_buffer,
            draws_allocation,
            draws_info,
            draws_info.pMappedData,
            100
        },
        .meshes = info,
        .mesh_ind = 1,
        .mat_ind = 0,
        .tex_ind = 128 - 32 + 5,
        .lights_buf = {
            lights_buffer,
            lights_allocation,
            lights_info,
            lights_info.pMappedData,
            100
        },
        .materials_buf = {
            materials_buffer,
            materials_allocation,
            materials_info,
            materials_info.pMappedData,
            100
        },
        // .scene_buf = {
        //     scene_buffer,
        //     scene_allocation,
        //     scene_info,
        //     scene_info.pMappedData,
        //     100
        // },
    };
    ecs_entity_t msets = ecs_singleton_set_ptr(world, MainSets, &mainsets);

    VkSamplerCreateInfo sampler_info = {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .magFilter = VK_FILTER_LINEAR,
        .minFilter = VK_FILTER_LINEAR,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .mipLodBias = 0.0,
        .anisotropyEnable = VK_FALSE,
        .maxAnisotropy = 0.0,
        .compareEnable = VK_FALSE,
        .compareOp = VK_COMPARE_OP_ALWAYS,
        .minLod = 0.0,
        .maxLod = VK_LOD_CLAMP_NONE,
        .borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE,
        .unnormalizedCoordinates = VK_FALSE,
    };

    VkSampler sampler;
    vkCreateSampler(dev, &sampler_info, NULL, &sampler);

    VkSamplerCreateInfo sampler_info2 = {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .magFilter = VK_FILTER_LINEAR,
        .minFilter = VK_FILTER_LINEAR,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .mipLodBias = 0.0,
        .anisotropyEnable = VK_TRUE,
        .maxAnisotropy = 16.0,
        .compareEnable = VK_FALSE,
        .compareOp = VK_COMPARE_OP_ALWAYS,
        .minLod = 0.0,
        .maxLod = VK_LOD_CLAMP_NONE,
        .borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK,
        .unnormalizedCoordinates = VK_FALSE,
    };

    VkSampler sampler2;
    vkCreateSampler(dev, &sampler_info2, NULL, &sampler2);

    VkSamplerCreateInfo sampler_info3 = {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .magFilter = VK_FILTER_NEAREST,
        .minFilter = VK_FILTER_NEAREST,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .mipLodBias = 0.0,
        .anisotropyEnable = VK_FALSE,
        .maxAnisotropy = 0.0,
        .compareEnable = VK_FALSE,
        .compareOp = VK_COMPARE_OP_ALWAYS,
        .minLod = 0.0,
        .maxLod = VK_LOD_CLAMP_NONE,
        .borderColor = VK_BORDER_COLOR_FLOAT_OPAQUE_BLACK,
        .unnormalizedCoordinates = VK_FALSE,
    };

    VkSampler nearest_sampler;
    vkCreateSampler(dev, &sampler_info3, NULL, &nearest_sampler);

    ecs_singleton_set_ptr(world, VkSampler, &sampler2);

    ECS_IMPORT(world, Ui);

    ecs_entity_t image  = load_texture(world, "textures/stone.png");
    ecs_entity_t image2 = load_texture(world, "textures/grass.png");

    u8 fort[16] = { 
        255, 0, 255, 255,
        0,   0,   0, 255,
        0,   0,   0, 255,
        255, 0, 255, 255,
    };
    ecs_entity_t empty_e = load_texture2d_from_data(world, &fort, 2, 2, 4);
    u32 forted = -1;

    ecs_entity_t white_tex = load_texture2d_from_data(world, &forted, 1, 1, 4);
    ecs_set_name(world, white_tex, "white_tex");
    
    descriptor_load_image(world, white_tex, nearest_sampler, global_set, 0);
    descriptor_load_image(world, empty_e, sampler, global_set, 1);

    ecs_entity_t tonemap = load_dds_texture(world, "textures/tony_mc_mapface.dds");

    descriptor_load_image(world, tonemap, sampler2, global_set, 2);
    descriptor_load_image(world, image, sampler, global_set, 3);
    descriptor_load_image(world, image2, sampler, global_set, 4);

    const Glyphs *glyphs = ecs_singleton_get(world, Glyphs);
    for (int i = 32; i < 128; i++) {
        if (glyphs->glyphs[i - 32].texture) {
            descriptor_load_image(world, glyphs->glyphs[i - 32].texture, sampler, global_set, i - 32 + 5);
        } else {
            descriptor_load_image(world, empty_e, sampler, global_set, i - 32 + 5);
        }
    }

    ecs_entity_t mat0 = new_material(world, (vec3s){{ 1, 1, 1 }},   empty_e,   white_tex,   0, 0, 1, "not_found");
    ecs_entity_t mat3 = new_material(world, (vec3s){{ 1, 1, 1 }},   white_tex, white_tex, 1, 0, 1, "white");
                        new_material(world, (vec3s){{ .5, .7, 1 }}, white_tex, white_tex, 0, 0, 1, "sky");
    ecs_entity_t mat  = new_material(world, (vec3s){{ 1, 1, 1 }},   image,     white_tex,     1, 0, 1, "stone");
    ecs_entity_t mat2 = new_material(world, (vec3s){{ 1, 1, 1 }},   image2,    white_tex,    1, 0, 1, "grass");
    ecs_entity_t mat4 = new_material(world, (vec3s){{ 1, 0, 0 }},   white_tex, white_tex, 0, 0, 1, "red");

    vmaFlushAllocation(allocator, materials_allocation, 0, 0);

    
    // write_ds.pBufferInfo = &_buffer_info;

    VkDescriptorBufferInfo _buffer_info = {0};
    _buffer_info.buffer = buffer;
    _buffer_info.offset = 0;
    _buffer_info.range = VK_WHOLE_SIZE;

    VkWriteDescriptorSet write_ds1 = {0};
    write_ds1.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write_ds1.dstSet = camera_set;
    write_ds1.dstBinding = 0;
    write_ds1.dstArrayElement = 0;
    write_ds1.descriptorCount = 1;
    write_ds1.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    write_ds1.pBufferInfo = &_buffer_info;

    vkUpdateDescriptorSets(dev, 1, &write_ds1, 0, NULL);

    VkDescriptorBufferInfo buf_info = {0};
    buf_info.buffer = objects_buffer;
    buf_info.offset = 0;
    buf_info.range = VK_WHOLE_SIZE;

    VkWriteDescriptorSet write_ds = {0};
    write_ds.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write_ds.dstSet = frame_set;
    write_ds.dstBinding = 0;
    write_ds.dstArrayElement = 0;
    write_ds.descriptorCount = 1;
    write_ds.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    write_ds.pBufferInfo = &buf_info;

    vkUpdateDescriptorSets(dev, 1, &write_ds, 0, NULL);

    _buffer_info.buffer = lights_buffer;
    _buffer_info.offset = 0;
    _buffer_info.range = VK_WHOLE_SIZE;

    write_ds1.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write_ds1.dstSet = frame_set;
    write_ds1.dstBinding = 1;
    write_ds1.dstArrayElement = 0;
    write_ds1.descriptorCount = 1;
    write_ds1.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    write_ds1.pBufferInfo = &_buffer_info;

    vkUpdateDescriptorSets(dev, 1, &write_ds1, 0, NULL);

    VkDescriptorBufferInfo bufin = {0};
    bufin.buffer = meshes_buffer;
    bufin.offset = 0;
    bufin.range = VK_WHOLE_SIZE;

    VkWriteDescriptorSet write = {0};
    write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write.dstSet = global_set;
    write.dstBinding = 1;
    write.dstArrayElement = 0;
    write.descriptorCount = 1;
    write.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    write.pBufferInfo = &bufin;

    vkUpdateDescriptorSets(dev, 1, &write, 0, NULL);

    _buffer_info.buffer = materials_buffer;
    _buffer_info.offset = 0;
    _buffer_info.range = VK_WHOLE_SIZE;

    write_ds1.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write_ds1.dstSet = global_set;
    write_ds1.dstBinding = 2;
    write_ds1.dstArrayElement = 0;
    write_ds1.descriptorCount = 1;
    write_ds1.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    write_ds1.pBufferInfo = &_buffer_info;

    vkUpdateDescriptorSets(dev, 1, &write_ds1, 0, NULL);

    VkPushConstantRange push_range;
    push_range.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    push_range.offset = 0;
    push_range.size = sizeof(mat4);

    VkPushConstantRange push_range1;
    push_range1.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    push_range1.offset = sizeof(mat4);
    push_range1.size = 28;

    VkPipelineLayoutCreateInfo pipeline_layout_info = {0};
    pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipeline_layout_info.setLayoutCount = 4;
    pipeline_layout_info.pSetLayouts = (VkDescriptorSetLayout[]){ global_set_layout, frame_set_layout, camera_layout, framebuffer_layout };
    pipeline_layout_info.pushConstantRangeCount = 2;
    pipeline_layout_info.pPushConstantRanges = (VkPushConstantRange[]){push_range, push_range1};

    VkPipelineLayout pipeline_layout;
    vkCreatePipelineLayout(dev, &pipeline_layout_info, NULL, &pipeline_layout);

    // pipeline_layout_info.pSetLayouts = &tlayout;

    pipeline_layout_info.pushConstantRangeCount = 1;
    pipeline_layout_info.pPushConstantRanges = &(VkPushConstantRange) { VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT, 0, 132 };

    VkPipelineLayout text_layout;
    vkCreatePipelineLayout(dev, &pipeline_layout_info, NULL, &text_layout);

    // pipeline_layout_info.pSetLayouts = &olayout;
    pipeline_layout_info.pushConstantRangeCount = 1;
    pipeline_layout_info.pPushConstantRanges = &(VkPushConstantRange) { VK_SHADER_STAGE_FRAGMENT_BIT, 0, 36 };

    VkPipelineLayout outline_layout;
    vkCreatePipelineLayout(dev, &pipeline_layout_info, NULL, &outline_layout);

    ///

    // struct TextureFormat msdepth = FBTEX_DEPTH;
    // msdepth.sample_count = VK_SAMPLE_COUNT_4_BIT;

    // struct TextureFormat mscolor = FBTEX_COLOR;
    // mscolor.sample_count = VK_SAMPLE_COUNT_4_BIT;

    VkAttachmentDescription background_color_desc = {
        .flags = 0,
        .format = FBTEX_COLOR.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    };

    VkAttachmentDescription background_normal_desc = {
        .flags = 0,
        .format = FBTEX_NORM.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    };

    VkAttachmentDescription background_mat_desc = {
        .flags = 0,
        .format = FBTEX_MAT.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    };

    VkAttachmentDescription background_depth_desc = {
        .flags = 0,
        .format = FBTEX_DEPTH.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    };

    VkSubpassDescription background_subpass_desc = {
        .flags = 0,
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .inputAttachmentCount = 0,
        .pInputAttachments = NULL,
        .colorAttachmentCount = 4,
        .pColorAttachments = (VkAttachmentReference[]){ { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL }, { 1, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL}, { 2, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL}, { 3, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL } },
        .pResolveAttachments = NULL,
        .pDepthStencilAttachment = &(VkAttachmentReference){ 4, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL },
        .preserveAttachmentCount = 0,
        .pPreserveAttachments = NULL
    };

    VkRenderPassCreateInfo background_pass_info = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .attachmentCount = 5,
        .pAttachments = (VkAttachmentDescription[]){ background_color_desc, background_normal_desc, background_color_desc, background_mat_desc, background_depth_desc },
        .subpassCount = 1,
        .pSubpasses = &background_subpass_desc,
        .dependencyCount = 0,
        .pDependencies = NULL,
    };

    VkRenderPass background_render_pass; 
    vkCreateRenderPass(dev, &background_pass_info, NULL, &background_render_pass);

    VkAttachmentDescription main_uv_desc = {
        .flags = 0,
        .format = FBTEX_UV.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
    };

    VkAttachmentDescription main_normal_desc = {
        .flags = 0,
        .format = FBTEX_NORM.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
    };

    VkAttachmentDescription main_mat_desc = {
        .flags = 0,
        .format = FBTEX_MAT.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
    };

    VkAttachmentDescription main_depth_desc = {
        .flags = 0,
        .format = FBTEX_DEPTH.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_STORE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL
    };

    VkSubpassDescription main_subpass_desc = {
        .flags = 0,
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .inputAttachmentCount = 0,
        .pInputAttachments = NULL,
        .colorAttachmentCount = 4,
        .pColorAttachments = (VkAttachmentReference[]){ { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL }, { 1, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL}, { 2, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL }, { 3, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL } },
        .pResolveAttachments = NULL,
        .pDepthStencilAttachment = &(VkAttachmentReference){ 4, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL },
        .preserveAttachmentCount = 0,
        .pPreserveAttachments = NULL
    };

    VkRenderPassCreateInfo main_pass_info = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .attachmentCount = 5,
        .pAttachments = (VkAttachmentDescription[]){ main_uv_desc, main_normal_desc, main_normal_desc, main_mat_desc, main_depth_desc },
        .subpassCount = 1,
        .pSubpasses = &main_subpass_desc,
        .dependencyCount = 0,
        .pDependencies = NULL,
    };

    VkRenderPass main_render_pass; 
    vkCreateRenderPass(dev, &main_pass_info, NULL, &main_render_pass);

    VkAttachmentDescription framebuffer_color_desc = {
        .flags = 0,
        .format = format.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
    };

    VkSubpassDescription framebuffer_subpass_desc = {
        .flags = 0,
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .inputAttachmentCount = 0,
        .pInputAttachments = NULL,
        .colorAttachmentCount = 1,
        .pColorAttachments = &(VkAttachmentReference){ 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL },
        .pResolveAttachments = NULL,
        .pDepthStencilAttachment = 0,
        .preserveAttachmentCount = 0,
        .pPreserveAttachments = NULL
    };

    VkRenderPassCreateInfo framebuffer_pass_info = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .attachmentCount = 1,
        .pAttachments = (VkAttachmentDescription[]){ framebuffer_color_desc },
        .subpassCount = 1,
        .pSubpasses = &framebuffer_subpass_desc,
        .dependencyCount = 0,
        .pDependencies = NULL,
    };

    VkRenderPass framebuffer_render_pass; 
    vkCreateRenderPass(dev, &framebuffer_pass_info, NULL, &framebuffer_render_pass);

    VkAttachmentDescription outline_color_desc = {
        .flags = 0,
        .format = format.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_LOAD,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        .finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    };

    VkAttachmentDescription outline_depth_desc = {
        .flags = 0,
        .format = FBTEX_DEPTH.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_LOAD,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    };

    VkSubpassDescription outline_subpass_desc = {
        .flags = 0,
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .inputAttachmentCount = 0,
        .pInputAttachments = NULL,
        .colorAttachmentCount = 1,
        .pColorAttachments = &(VkAttachmentReference){ 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL },
        .pResolveAttachments = NULL,
        .pDepthStencilAttachment = &(VkAttachmentReference){ 1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL },
        .preserveAttachmentCount = 0,
        .pPreserveAttachments = NULL
    };

    VkRenderPassCreateInfo outline_pass_info = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .attachmentCount = 2,
        .pAttachments = (VkAttachmentDescription[]){ outline_color_desc, outline_depth_desc },
        .subpassCount = 1,
        .pSubpasses = &outline_subpass_desc,
        .dependencyCount = 0,
        .pDependencies = NULL,
    };

    VkRenderPass outline_render_pass; 
    vkCreateRenderPass(dev, &outline_pass_info, NULL, &outline_render_pass);


    VkAttachmentDescription text_color_desc = {
        .flags = 0,
        .format = format.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_LOAD,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        .finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
    };

    VkAttachmentDescription text_depth_desc = {
        .flags = 0,
        .format = FBTEX_DEPTH.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_LOAD,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        .finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
    };

    VkSubpassDescription text_subpass_desc = {
        .flags = 0,
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .inputAttachmentCount = 0,
        .pInputAttachments = NULL,
        .colorAttachmentCount = 1,
        .pColorAttachments = &(VkAttachmentReference){ 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL },
        .pResolveAttachments = NULL,
        .pDepthStencilAttachment = &(VkAttachmentReference){ 1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL },
        .preserveAttachmentCount = 0,
        .pPreserveAttachments = NULL
    };

    VkRenderPassCreateInfo text_pass_info = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .attachmentCount = 2,
        .pAttachments = (VkAttachmentDescription[]){ text_color_desc, text_depth_desc },
        .subpassCount = 1,
        .pSubpasses = &text_subpass_desc,
        .dependencyCount = 0,
        .pDependencies = NULL,
    };

    VkRenderPass text_render_pass; 
    vkCreateRenderPass(dev, &text_pass_info, NULL, &text_render_pass);

    VkAttachmentDescription blit_color_desc = {
        .flags = 0,
        .format = format.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
    };

    VkAttachmentDescription blit_depth_desc = {
        .flags = 0,
        .format = FBTEX_DEPTH.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    };

    VkSubpassDescription blit_subpass_desc = {
        .flags = 0,
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .inputAttachmentCount = 0,
        .pInputAttachments = NULL,
        .colorAttachmentCount = 1,
        .pColorAttachments = &(VkAttachmentReference){ 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL },
        .pResolveAttachments = NULL,
        .pDepthStencilAttachment = &(VkAttachmentReference){ 1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL },
        .preserveAttachmentCount = 0,
        .pPreserveAttachments = NULL
    };

    VkRenderPassCreateInfo blit_pass_info = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .attachmentCount = 2,
        .pAttachments = (VkAttachmentDescription[]){ blit_color_desc, blit_depth_desc },
        .subpassCount = 1,
        .pSubpasses = &blit_subpass_desc,
        .dependencyCount = 0,
        .pDependencies = NULL,
    };

    VkRenderPass blit_render_pass; 
    vkCreateRenderPass(dev, &blit_pass_info, NULL, &blit_render_pass);

    VkAttachmentDescription fxaa_color_desc = {
        .flags = 0,
        .format = format.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    };

    VkAttachmentDescription fxaa_depth_desc = {
        .flags = 0,
        .format = FBTEX_DEPTH.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    };

    VkSubpassDescription fxaa_subpass_desc = {
        .flags = 0,
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .inputAttachmentCount = 0,
        .pInputAttachments = NULL,
        .colorAttachmentCount = 1,
        .pColorAttachments = &(VkAttachmentReference){ 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL },
        .pResolveAttachments = NULL,
        .pDepthStencilAttachment = &(VkAttachmentReference){ 1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL },
        .preserveAttachmentCount = 0,
        .pPreserveAttachments = NULL
    };

    VkRenderPassCreateInfo fxaa_pass_info = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .attachmentCount = 2,
        .pAttachments = (VkAttachmentDescription[]){ fxaa_color_desc, fxaa_depth_desc },
        .subpassCount = 1,
        .pSubpasses = &fxaa_subpass_desc,
        .dependencyCount = 0,
        .pDependencies = NULL,
    };

    VkRenderPass fxaa_render_pass; 
    vkCreateRenderPass(dev, &fxaa_pass_info, NULL, &fxaa_render_pass);

    const Texture *depth = ecs_get(world, chudchain->depth, Texture);
    VkFramebuffer *framebuffers = malloc(sizeof(VkFramebuffer) * chudchain->count);
    for (int i = 0; i < chudchain->count; i++) {
        VkFramebufferCreateInfo framebuffer_info = {
            .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
            .pNext = NULL,
            .flags = 0,
            .renderPass = blit_render_pass,
            .attachmentCount = 2,
            .pAttachments = (VkImageView[]){ chudchain->views[i], depth->view },
            .width = w,
            .height = h,
            .layers = 1,
        };

        vkCreateFramebuffer(dev, &framebuffer_info, NULL, &framebuffers[i]);
    }
   
    ecs_entity_t swap_framebuffer_e = ecs_set(world, 0, Framebuffer, { 0, 0, 0, NULL, framebuffers, blit_render_pass, w, h });

    chudchain->framebuffer = swap_framebuffer_e;

    ecs_entity_t color_t = create_fb_texture(world, w, h, FBTEX_UV);
    ecs_entity_t normal_t = create_fb_texture(world, w, h, FBTEX_NORM);
    ecs_entity_t pos_t = create_fb_texture(world, w, h, FBTEX_NORM);
    ecs_entity_t mat_t = create_fb_texture(world, w, h, FBTEX_MAT);
    ecs_entity_t depth_t = create_fb_texture(world, w, h, FBTEX_DEPTH);

    // ecs_entity_t res_color_t = create_fb_texture(world, w, h, FBTEX_COLOR);
    // ecs_entity_t res_depth_t = create_fb_texture(world, w, h, FBTEX_DEPTH);
    
    ecs_entity_t gbuffer_e = ecs_set(world, 0, Framebuffer, { 4, (ecs_entity_t[]){ color_t, normal_t, pos_t, mat_t }, depth_t, NULL, NULL, main_render_pass, w, h });
    init_framebuffer(world, gbuffer_e);

    ecs_entity_t lighting_color_t = create_fb_texture(world, w, h, FBTEX_COLOR);
    ecs_entity_t lighting_e = ecs_set(world, 0, Framebuffer, { 1, (ecs_entity_t[]){ lighting_color_t }, 0, NULL, NULL, framebuffer_render_pass, w, h });
    init_framebuffer(world, lighting_e);

    ecs_entity_t fcolor_t = create_fb_texture(world, w, h, FBTEX_COLOR);
    ecs_entity_t fdepth_t = create_fb_texture(world, w, h, FBTEX_DEPTH);
    
    ecs_entity_t framebuffer_e = ecs_set(world, 0, Framebuffer, { 1, (ecs_entity_t[]){ fcolor_t }, fdepth_t, NULL, NULL, fxaa_render_pass, w, h });
    init_framebuffer(world, framebuffer_e);
    
    descriptor_load_image(world, color_t,  nearest_sampler, framebuffer_set, 0);
    descriptor_load_image(world, pos_t,    nearest_sampler, framebuffer_set, 1);
    descriptor_load_image(world, normal_t, nearest_sampler, framebuffer_set, 2);
    descriptor_load_image(world, mat_t,    nearest_sampler, framebuffer_set, 3);

    descriptor_load_image(world, lighting_color_t,  sampler, framebuffer_set, 4);

    descriptor_load_image(world, fcolor_t,  nearest_sampler, framebuffer_set, 5);

    Window *window_ = ecs_singleton_get_mut(world, Window);
    window_->framebuffer = swap_framebuffer_e;
    ecs_singleton_modified(world, Window);

    char *fart_night = malloc(200);
    fart_night[0] = 0;

    VkPipeline pipeline  = create_main_pipeline(world, pipeline_layout, main_render_pass);
    ecs_entity_t pipeline_e = ecs_set(world, 0, Pipeline, { &uniform_buffer, pipeline_layout, pipeline, global_set });

    VkPipeline bpipeline = create_background_pipeline(world, pipeline_layout, background_render_pass);
    ecs_entity_t bpipeline_e = ecs_set(world, 0, Pipeline, { &uniform_buffer, pipeline_layout, bpipeline, global_set });

    VkPipeline opipeline = create_outline_pipeline(world, outline_layout, outline_render_pass);
    ecs_entity_t opipeline_e = ecs_set(world, 0, Pipeline, { &uniform_buffer, outline_layout, opipeline, global_set });

    VkPipeline fpipeline = create_framebuffer_pipeline(world, outline_layout, framebuffer_render_pass);
    ecs_entity_t fpipeline_e = ecs_set(world, 0, Pipeline, { &uniform_buffer, outline_layout, fpipeline, global_set });

    VkPipeline tpipeline = create_text_pipeline(world, text_layout, text_render_pass);
    ecs_entity_t tpipeline_e = ecs_set(world, 0, Pipeline, { &uniform_buffer, text_layout, tpipeline, camera_set });

    VkPipeline blit_pipeline = create_blit_pipeline(world, pipeline_layout, blit_render_pass);
    ecs_entity_t blit_pipeline_e = ecs_set(world, 0, Pipeline, { &uniform_buffer, pipeline_layout, blit_pipeline, global_set});

    VkPipeline fxaa_pipeline = create_fxaa_pipeline(world, outline_layout, fxaa_render_pass);
    ecs_entity_t fxaa_pipeline_e = ecs_set(world, 0, Pipeline, { &uniform_buffer, outline_layout, fxaa_pipeline, global_set});


    ecs_entity_t render_pass_e = ecs_entity(world, {.name = "renderpasses" });   
    ecs_set(world, render_pass_e, RenderPass, {0});

    ECS_TAG(world, Background);

    ecs_query_t *background_pass_query = ecs_query(world, {
        .filter.terms = {
            { ecs_id(Matrix4) },
            { Background },
        }
    });
    
    ecs_query_t *main_pass_query = ecs_query(world, {
        .filter.terms = {
            { ecs_id(Matrix4) },
            { ecs_id(Render3D) },
        }
    });
    
    ecs_query_t *outline_pass_query = ecs_query(world, {
        .filter.terms = {
            { ecs_id(Matrix4) },
            { Background },
        }
    });
    
    ecs_query_t *text_pass_query = ecs_query(world, {
        .filter.terms = {
            { ecs_id(Transform2D) },
            { ecs_id(AnchorBits) },
            { ecs_id(Label) },
        }
    });
    
    VkClearValue ccol = {
        .color = { .int32 = { 0, 0, 0, 0 }},
    };

    VkClearValue cdepth = {
        .depthStencil = { 1.0, 0x00 }
    };

    ecs_entity_t main_pass = ecs_new_w_pair(world, EcsDependsOn, render_pass_e);
    ecs_set(world, main_pass, RenderPass, { main_render_pass, gbuffer_e, pipeline_e, main_pass_query, &render_main, 1, (VkClearValue[]){ ccol, ccol, ccol, { .color = { .int32 = { 2, 0, 0, 0 } } }, cdepth } });
    ecs_set_name(world, main_pass, "main_pass");
        
    ecs_entity_t framebuffer_pass = ecs_new_w_pair(world, EcsDependsOn, main_pass);
    ecs_set(world, framebuffer_pass, RenderPass, { framebuffer_render_pass, lighting_e, fpipeline_e, background_pass_query, &render_framebuffer, 0 });
    ecs_set_name(world, framebuffer_pass, "framebuffer_pass");
        
    // ecs_entity_t outline_pass = ecs_new_w_pair(world, EcsDependsOn, framebuffer_pass);
    // ecs_set(world, outline_pass, RenderPass, { outline_render_pass, 0, opipeline_e, background_pass_query, &render_outline, 0 });
    // ecs_set_name(world, outline_pass, "outline_pass");
        
    ecs_entity_t fxaa_pass = ecs_new_w_pair(world, EcsDependsOn, framebuffer_pass);
    ecs_set(world, fxaa_pass, RenderPass, { fxaa_render_pass, framebuffer_e, fxaa_pipeline_e, background_pass_query, &render_fxaa, 0, (VkClearValue[]){ ccol, cdepth } });
    ecs_set_name(world, fxaa_pass, "fxaa_pass");
        
    ecs_entity_t text_pass = ecs_new_w_pair(world, EcsDependsOn, fxaa_pass);
    ecs_set(world, text_pass, RenderPass, { text_render_pass, framebuffer_e, tpipeline_e, text_pass_query, &render_text, 0 });
    ecs_set_name(world, text_pass, "text_pass");

    ecs_entity_t blit_pass = ecs_new_w_pair(world, EcsDependsOn, text_pass);
    ecs_set(world, blit_pass, RenderPass, { blit_render_pass, 0, blit_pipeline_e, background_pass_query, &render_blit, 0 });
    ecs_set_name(world, blit_pass, "blit_pass");
        
    // render_passes(world);

    // struct Camera cam;

    // ecs_entity_t cam_e = ecs_new_id(world);
    // ecs_set_ptr(world, cam_e, Camera, cam);

    // add_convar(world, "player", &(ConVar){ convar_int, .i = 0 });
    // Transform3D default_transform = {0};
    // default_transform.scale.x = 1;
    // default_transform.scale.y = 1;
    // default_transform.scale.z = 1;
    // default_transform.pos.x = 6;
    // default_transform.pos.z = -1;

    // default_transform.rotation = glms_quat_identity();

    // struct MaterialInfo *mat_info = materials_info.pMappedData;


    local_player_init(world);

    ecs_entity_t fps_counter = ecs_set(world, ecs_set(world, ecs_set(world, 0, Label, { 48, fart_night }), Transform2D, { {{ 0.5, 0 }}, {{ 1, 1 }}, 0 }), AnchorBits, { anchor_left | anchor_right | anchor_top });
    // ecs_entity_t forntite = ecs_set_name(world, ecs_set(world, 0, Textbox, { fps_counter, NULL, 200 }), "baller");

      ecs_system(world, {
        .entity = ecs_entity(world, {
          .name = "set_text",
          .add = { ecs_dependson(PInput) }
        }),
        .callback = set_text,
        .ctx = (void*)fps_counter,
        .interval = 1.0,
      });


    mat4s t = glms_mat4_identity();
    ecs_entity_t background = ecs_new_id(world);
    ecs_add(world, background, Background);
    ecs_set(world, background, Mesh, { 6, fullscreen_buf_e, 0 });
    ecs_set_ptr(world, background, Matrix4, &t);

    ecs_set_entity_range(world, 1000, 9000);

    ecs_entity_t teleportnite_a = ecs_set_name(world, new_action(world, { teleportnite }), "teleportnite");
    ecs_entity_t select_object_a = new_action(world, { select_object });
    ecs_entity_t teleportnite_ppu_a = new_action(world, { teleportnite_ppu });

    Quaternion quat = glms_quat_identity();

    PhysicsSystem *system = ecs_singleton_get_mut(world, PhysicsSystem);
    
    rapier_PhysicsState_set_delta(system->state, 1.0 / 64.0);
    Collider *collider = rapier_Collider_create_cuboid((RVector3) { 0.5, 0.5, 0.5 }, 1);

    ecs_entity_t block = ecs_new_id(world);

    RigidBodyOptions options = {
        .type = RigidBodyTypeDynamic,
        // .pos = { 0, 10, -1 },
        .rotation = { 1, 0, 0, 0 },
        .linvel = { 0, 0, 0 },
        .gravity_scale = 1.0,
        .user_data = block,
    };

    RigidBody *body = rapier_RigidBody_create(&options);
    RigidBodyHandle handle = rapier_RigidBodySet_insert(system->rset, body);
    rapier_ColliderSet_insert_with_parent(system->cset, collider, handle, system->rset);

    ecs_set(world, block, Render3D, { mat, cube_mesh, 1 });
    ecs_add(world, block, RenderOutline);
    ecs_set(world, block, Velocity, {});
    ecs_add(world, block, SoundPlayer);
    ecs_set_name(world, block, "chud");
    ecs_set_ptr(world, block, CollisionMesh, &mesh);
    ecs_add(world, block, Matrix4);
    Transform block_transform = default_transform();
    block_transform.pos = (Vector3){ { 6, 0, -1 } };
    ecs_set_ptr(world, block, Transform, &block_transform);
    ecs_set(world, block, PhysicsBody, { handle });
    ecs_set(world, block, Selectable, { teleportnite_a });

    ecs_entity_t light = ecs_new_id(world);
    Light fart = {
        light_type_sun,
        { -0.5, 1.5, 0.5 },
        { 1, 1, 1 },
        9.44, 0
    };
    glm_vec3_normalize(fart.dir);
    printf("%f, %f, %f\n", fart.dir[0], fart.dir[1], fart.dir[2]);
    ecs_set_ptr(world, light, Light, &fart);
    block_transform.pos = (Vector3){ { 0, 5, 0 } };
    ecs_set_ptr(world, light, Transform, &block_transform);

    ecs_set(world, light, Render3D, { mat3, cube_mesh, 1 });
    ecs_add(world, light, Matrix4);

    // ecs_add(world, light, Velocity);
    // ecs_set(world, light, PhysicsBody, { handle });
    // ecs_set(world, light, Rotation, { { 1, 0, 0, 0 }});

    // ecs_entity_t light2 = ecs_new_id(world);
    // ecs_set(world, light2, Light, { light_type_point, { 0, 0, 0 }, { 0, 0, 1 }, 5.0, 0});
    // ecs_set(world, light2, Position, {{ 0, 5, 1 }});
    // ecs_set(world, light2, Render3D, { mat3, cube_mesh, 1 });
    // ecs_add(world, light2, Matrix4);
    // ecs_entity_t light3 = ecs_new_id(world);
    // ecs_set(world, light3, Light, { light_type_point, { 0, 0, 0 }, { 1, 0, 0 }, 5.0, 0});
    // ecs_set(world, light3, Position, {{ 0, 5, -1 }});
    // ecs_set(world, light3, Render3D, { mat3, cube_mesh, 1 });
    // ecs_add(world, light3, Matrix4);
    // ecs_add_pair(world, block, Networked, ecs_id(Position));
    // ecs_add_pair(world, block, Networked, ecs_id(Velocity));
    // play_sound(world, block, sres);

    Collider *floor = rapier_Collider_create_cuboid((RVector3) { 8, 0.25, 8 }, 1);

    ecs_entity_t block2 = ecs_new_id(world);

    options.pos.y = -1;
    options.pos.z =  0;
    options.type = RigidBodyTypeFixed;
    options.user_data = block2;

    body = rapier_RigidBody_create(&options);
    handle = rapier_RigidBodySet_insert(system->rset, body);
    rapier_ColliderSet_insert_with_parent(system->cset, floor, handle, system->rset);

    ecs_set(world, block2, Render3D, { mat, cube_mesh, 1 });
    ecs_add(world, block2, RenderOutline);
    ecs_set_name(world, block2, "chud2");
    ecs_set_ptr(world, block2, CollisionMesh, &mesh);
    ecs_add(world, block2, Matrix4);
    block_transform.pos = (Vector3){ { 6, -1, 0 } };
    block_transform.scale = (Vector3){ { 16, 0.5, 16 } };
    ecs_set_ptr(world, block2, Transform, &block_transform);
    // ecs_set(world, block2, Scale, { { 16, 0.5, 16 } });
    // ecs_set(world, block2, Rotation, { { 1, 0, 0, 0 }});
    ecs_set(world, block2, PhysicsBody, { handle });

    ecs_entity_t gizmo = ecs_new_id(world);
    // load_gltf_model(world, gizmo, "models/ak47.glb");
    load_gltf_model(world, gizmo, "models/glTF/Sponza.gltf");
    vmaFlushAllocation(allocator, meshes_allocation, 0, 0);
    // ecs_iter_t it = ecs_children(world, gizmo);
    // while (ecs_children_next(&it)) {
    //     for (int i = 0; i < it.count; i++) {
    //         ecs_set(world, it.entities[i], Selectable, { teleportnite_ppu_a });
    //     }
    // }
    block_transform = default_transform();
    block_transform.scale = (Vector3){ { 0.5, 0.5, 0.5 } };
    ecs_set(world, gizmo, Velocity, {});
    ecs_set_name(world, gizmo, "gizmo");
    ecs_add(world, gizmo, Matrix4);
    // ecs_add(world, gizmo, SoundPlayer);
    // ecs_set_ptr(world, gizmo, CollisionMesh, &mesh);
    // ecs_set(world, gizmo, Position, { { 0, 0, 0 } } );
    // ecs_set(world, gizmo, Scale, { { 0.5, 0.5, 0.5 } });

    // ecs_set_pair(world, gizmo, Networked, ecs_id(Position));
    // ecs_set_pair(world, gizmo, Networked, ecs_id(Velocity));

    // default_transform.pos.z = 1;

    // ecs_entity_t block3 = ecs_new_id(world);
    // options.pos.y = 10;
    // options.pos.z = 1;
    // options.type = RigidBodyTypeDynamic;
    // options.user_data = block3;

    // collider = rapier_Collider_create_cuboid((RVector3) { 0.5, 0.5, 0.5 }, 1);
    // ecs_set(world, block3, Render3D, { mat, cube_mesh, 1 });
    // ecs_set(world, block3, Velocity, {});
    // ecs_add(world, block3, SoundPlayer);
    // ecs_set_name(world, block3, "chud3");
    // ecs_set_ptr(world, block3, CollisionMesh, &mesh);
    // ecs_add(world, block3, Matrix4);
    // ecs_set(world, block3, Position, { { 6, 0, 1 } } );
    // ecs_set(world, block3, Selectable, { teleportnite_a });
    // ecs_set(world, block3, Rotation, { { 1, 0, 0, 0 }});
    // body = rapier_RigidBody_create(&options);
    // handle = rapier_RigidBodySet_insert(system->rset, body);
    // rapier_ColliderSet_insert_with_parent(system->cset, collider, handle, system->rset);
    // ecs_set(world, block3, PhysicsBody, { handle });

    // ecs_set_pair(world, block3, Networked, ecs_id(Position));
    // ecs_set_pair(world, block3, Networked, ecs_id(Velocity));

    ecs_entity_t game_actions = new_action_set(world, "game_actions");
    ecs_entity_t text_actions = new_action_set(world, "text_actions");

    // new_kb_action(world,    action_down, select_textbox,           (void*)game_actions,             SDL_SCANCODE_G);
    new_kb_action(world, text_actions, action_down,    deselect_textbox,      (void*)game_actions,             SDL_SCANCODE_ESCAPE);
    new_kb_action(world, text_actions, action_down,    backspace_textbox,     (void*)game_actions,             SDL_SCANCODE_BACKSPACE);

    new_kb_action(world, game_actions, action_pressed, move_forward,          NULL,             SDL_SCANCODE_W                  );
    new_kb_action(world, game_actions, action_pressed, move_backward,         NULL,             SDL_SCANCODE_S                  );
    new_kb_action(world, game_actions, action_pressed, move_left,             NULL,             SDL_SCANCODE_A                  );
    new_kb_action(world, game_actions, action_pressed, move_right,            NULL,             SDL_SCANCODE_D                  );
    new_kb_action(world, game_actions, action_pressed, move_up,               NULL,             SDL_SCANCODE_SPACE              );
    new_kb_action(world, game_actions, action_pressed, move_down,             NULL,             SDL_SCANCODE_LSHIFT             );
    struct SetConvarFloat {
        char *name;
        float value;
    };
    new_kb_action(world, game_actions, action_down,    convar_float_minusone, "timescale",      SDL_SCANCODE_Q                  );
    new_kb_action(world, game_actions, action_down,    convar_float_plusone,  "timescale",      SDL_SCANCODE_E                  );
    // new_kb_action(world, game_actions, action_down,    convar_int_minusone, "player",      SDL_SCANCODE_Q                  );
    // new_kb_action(world, game_actions, action_down,    convar_int_plusone,  "player",      SDL_SCANCODE_E                  );
    new_kb_action(world, game_actions, action_down,    toggle_convar,         "among",          SDL_SCANCODE_G                  );
    new_kb_action(world, game_actions, action_down,    toggle_convar,         "vsync",          SDL_SCANCODE_O                  );
    new_kb_action(world, game_actions, action_down,    toggle_convar,         "teleport",       SDL_SCANCODE_F                  );
    new_kb_action(world, game_actions, action_down,    toggle_convar,         "mouse_captured", SDL_SCANCODE_ESCAPE             );
    // new_kb_action(world, game_actions, action_down,    set_convar_float,      &(struct SetConvarFloat) { "speed", 20.0 },   SDL_SCANCODE_LSHIFT );
    // new_kb_action(world, game_actions, action_up,      set_convar_float,      &(struct SetConvarFloat) { "speed", 10.0 },   SDL_SCANCODE_LSHIFT );
    // new_kb_action(world, game_actions, action_down,    teleport_chud,         "chud",           SDL_SCANCODE_LEFT               );
    // new_kb_action(world, game_actions, action_down,    teleport_chud,         "chud2",          SDL_SCANCODE_DOWN               );
    // new_kb_action(world, game_actions, action_down,    teleport_chud,         "chud3",          SDL_SCANCODE_RIGHT              );
    new_mouse_axis_action(world, game_actions, bully_light_val, (void*)light, axis_mouse_wheel);
    new_mouse_action(world, game_actions, action_down, deselect_all,          NULL,             SDL_BUTTON_LEFT);
    new_mouse_action(world, game_actions, action_down, toggle_convar,         "mouse_captured", SDL_BUTTON_RIGHT                );
    new_mouse_action(world, game_actions, action_up,   toggle_convar,         "mouse_captured", SDL_BUTTON_RIGHT                );
    ecs_set(world, new_mouse_action(world, game_actions, action_pressed, throw_block,        (void*)(u64)10, SDL_BUTTON_LEFT                 ), InputInterval, { 0.04, 0 });
    new_mouse_action(world, game_actions, action_down, throw_block,                          (void*)(u64)100, SDL_BUTTON_X2);
    ecs_set(world, new_gamepad_action(world, game_actions, action_pressed, throw_block,  (void*)(u64)10, SDL_CONTROLLER_BUTTON_RIGHTSHOULDER), InputInterval, { 0.04, 0 });
    new_gamepad_action(world, game_actions, action_down, throw_block,      (void*)(u64)100,             SDL_CONTROLLER_BUTTON_LEFTSHOULDER);
    // new_gamepad_action(world, game_actions, action_down, teleport_chud,       "chud2",          SDL_CONTROLLER_BUTTON_DPAD_UP   );
    new_gamepad_action(world, game_actions, action_pressed, move_up,          NULL,             SDL_CONTROLLER_BUTTON_A         );
    new_gamepad_action(world, game_actions, action_down,    toggle_convar,    "teleport",       SDL_CONTROLLER_BUTTON_DPAD_RIGHT);
    // new_gamepad_action(world, game_actions, action_pressed, set_rumble,    0,                   SDL_CONTROLLER_BUTTON_A);
    new_gamepad_action(world, game_actions, action_pressed, move_down,        NULL,             SDL_CONTROLLER_BUTTON_B         );
    new_gamepad_axis_action(world, game_actions, move_scaled,                 NULL,             axis_left_stick                 );
    new_gamepad_axis_action(world, game_actions, look_scaled,                 NULL,             axis_right_stick                );
    new_gamepad_axis_action(world, game_actions, look_scaled_gyro,            NULL,             axis_gyroscope                  );
    // new_gamepad_axis_action(world, game_actions, make_chud,                   "chud",           axis_left_trigger               );
    // new_gamepad_axis_action(world, game_actions, make_chud,                   "chud3",          axis_right_trigger              );


    // ecs_set_entity_range(world, 10000, 0);
    // new_kb_action(world, action_down,    play_sound_a,    (ecs_entity_t[2]){block, sres},       SDL_SCANCODE_E      );

    // int mouse = 0;

    // const ConVar *quit = ecs_get(world, quit_e, ConVar);
    // const ConVar *timescale = ecs_get(world, timescale_e, ConVar);

    // ecs_set_target_fps(world, 5);
    
    // ecs_set_threads(world, 6);
    // ecs_set_target_fps(world, 144);
    while (ecs_progress(world, 0)) { 
    }

    // ma_engine_uninit(ecs_singleton_get_mut(world, ma_engine));
    vkQueueWaitIdle(queue);

    vmaUnmapMemory(allocator, uniform_allocation);

    for (int i = 0; i < chudchain->count; i++) {
        vkDestroyImageView(dev, chudchain->views[i], NULL);
    }

    destroy_buffer(world, &vert_buf);
    destroy_buffer(world, &fullscreen_buf);
    vmaDestroyBuffer(allocator, buffer, uniform_allocation);
    // vmaDestroyImage(allocator, image.image, image.allocation);
    // vmaDestroyImage(allocator, image2.image, image2.allocation);
    // vmaDestroyImage(allocator, chudchain->depth_image, chudchain->depth_allocation);
    ecs_run(world, ecs_id(delete_textures), 0, NULL);
    // vkDestroyImageView(dev, image.view, NULL);
    // vkDestroyImageView(dev, image2.view, NULL);
    // vkDestroyImageView(dev, chudchain->depth_view, NULL);
    vmaDestroyAllocator(allocator);
    // vkDestroyShaderModule(dev, vmod, NULL);
    // vkDestroyShaderModule(dev, fmod, NULL);
    // vkDestroyPipeline(dev, pipeline, NULL);
    vkDestroyPipelineLayout(dev, pipeline_layout, NULL);
    vkDestroyPipelineLayout(dev, outline_layout, NULL);
    vkDestroyDescriptorSetLayout(dev, global_set_layout, NULL);
    vkDestroySampler(dev, sampler, NULL);
    vkDestroyDescriptorPool(dev, descriptor_pool, NULL);
    vkDestroySemaphore(dev, chudchain->image_ready, NULL);
    vkDestroySemaphore(dev, chudchain->render_done_s, NULL);
    vkWaitForFences(dev, 1, &chudchain->render_done, VK_TRUE, (i64)-1);
    vkResetFences(dev, 1, &chudchain->render_done);
    vkDestroyFence(dev, chudchain->render_done, NULL);
    vkDestroySwapchainKHR(dev, chudchain->swapchain, NULL);
    vkDestroySurfaceKHR(instance, surf, NULL);
    vkFreeCommandBuffers(dev, pool, 1, &cmd);
    vkDestroyCommandPool(dev, pool, NULL);
    vkDestroyDevice(dev, NULL);
    vkDestroyInstance(instance, NULL);
    ecs_fini(world);
}
