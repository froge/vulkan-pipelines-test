#pragma once

#include <enet.h>
#include "flecs.h"
#include "common.h"

#ifdef NETWORKING_H_DECLARE
#define NETWORKING_EXTERN
#else
#define NETWORKING_EXTERN extern
#endif

typedef struct {
  ENetHost *host;
  int server;
}Host;

typedef struct {
  ENetPeer *peer;
  ecs_entity_t player;
}Peer;

typedef struct {
  u16 sequence;
  ecs_id_t component;
  const void *value;
}ComponentChange;

typedef struct {
  u16 offset;
}PlayerNetworked;

enum NetworkEventType {
  network_event_player_connected,
  network_event_player_identify,
  network_event_component_change,
  network_event_player_component_change,
  network_event_player_disconnected,
  network_event_action,
  network_event_input_action,
  network_event_add_entity,
  network_event_delete_entity,
};

typedef struct {
  enum NetworkEventType type;
  ecs_entity_t entity;
  union {
    ecs_entity_t action;
    struct {
      u16 player;
      u16 generation;
      u16 entity;
    } player;
  };
}NetworkEvent;

typedef struct {
  u16 offset;
  u16 player;
}PlayerTag;

struct Player {
  u32 max;
  u32 len;
  ecs_entity_t *entities;
  ENetPeer *peer;
  u16 generation;
  bool alive;
};

typedef struct {
  u32 max;
  u32 len;
  struct Player *players;
  u16 me;
}Players;

void networking_init(ecs_world_t *world);
void networking_init_client(ecs_world_t *world, char *addr);
void networking_init_server(ecs_world_t *world);

void NetworkingImport(ecs_world_t *world);

NETWORKING_EXTERN ECS_COMPONENT_DECLARE(Host);
NETWORKING_EXTERN ECS_COMPONENT_DECLARE(Peer);
NETWORKING_EXTERN ECS_COMPONENT_DECLARE(NetworkEvent);
NETWORKING_EXTERN ECS_COMPONENT_DECLARE(PlayerNetworked);
NETWORKING_EXTERN ECS_COMPONENT_DECLARE(PlayerTag);
NETWORKING_EXTERN ECS_COMPONENT_DECLARE(Players);

NETWORKING_EXTERN ECS_TAG_DECLARE(Networked);
