#pragma once

#include "flecs.h"
#include "SDL2/SDL.h"
#include "cglm/call.h"
#include "common.h"

#ifdef INPUT_H_DECLARE
#define INPUT_EXTERN
#else
#define INPUT_EXTERN extern
#endif

typedef struct AxisValue {
    f32 a0;
    f32 a1;
    f32 a2;
}AxisValue;

typedef void InputActionFunc(ecs_world_t *world, void *data);
typedef void AxisActionFunc(ecs_world_t *world, struct AxisValue value, void *data);

typedef struct InputAction {
    InputActionFunc *callback;
    void *data;
}InputAction;

typedef struct AxisAction {
    AxisActionFunc *callback;
    void *data;
}AxisAction;

typedef struct {
    SDL_Scancode key;
}KeyboardAction;

typedef struct {
    u32 button;
}MouseButtonAction;

typedef struct {
    SDL_GameController *controller;
    // SDL_Haptic *haptic;
}Gamepad;

typedef struct {
    SDL_GameControllerButton button;
}GamepadAction;

typedef enum {
    axis_gyroscope,
    axis_left_stick,
    axis_right_stick,
    axis_left_trigger,
    axis_right_trigger,
}GamepadAxis;

typedef struct {
    GamepadAxis axis;
}GamepadAxisAction;

typedef enum {
    axis_mouse_wheel,
    axis_cursor,
}MouseAxis;

typedef struct {
    MouseAxis axis;
}MouseAxisAction;

typedef enum {
    mouse_left,
    mouse_middle,
    mouse_right,
    mouse_4,
    mouse_5,
    mouse_w_up,
    mouse_w_down,
}MouseButton;

typedef struct {
    f32 interval;
    u32 last_tick;
}InputInterval;

void handle_input(ecs_iter_t *iter);

ecs_entity_t new_action_set(ecs_world_t *world, char *name);
void action_set_enable(ecs_world_t *world, ecs_entity_t set);
void action_set_disable(ecs_world_t *world, ecs_entity_t set);
ecs_entity_t new_kb_action(ecs_world_t *world, ecs_entity_t set, ecs_id_t event_type, InputActionFunc *callback, void *data, SDL_Scancode key);
ecs_entity_t new_mouse_action(ecs_world_t *world, ecs_entity_t set, ecs_id_t event_type, InputActionFunc *callback, void *data, u32 button);
ecs_entity_t new_mouse_axis_action(ecs_world_t *world, ecs_entity_t set, AxisActionFunc *callback, void *data, MouseAxis axis);
ecs_entity_t new_gamepad_action(ecs_world_t *world, ecs_entity_t set, ecs_id_t event_type, InputActionFunc *callback, void *data, SDL_GameControllerButton button);
ecs_entity_t new_gamepad_axis_action(ecs_world_t *world, ecs_entity_t set, AxisActionFunc *callback, void *data, GamepadAxis axis);

void InputImport(ecs_world_t *world);

INPUT_EXTERN ECS_COMPONENT_DECLARE(InputAction);
INPUT_EXTERN ECS_COMPONENT_DECLARE(AxisAction);
INPUT_EXTERN ECS_COMPONENT_DECLARE(KeyboardAction);
INPUT_EXTERN ECS_COMPONENT_DECLARE(MouseButtonAction);
INPUT_EXTERN ECS_COMPONENT_DECLARE(MouseAxisAction);
INPUT_EXTERN ECS_COMPONENT_DECLARE(Gamepad);
INPUT_EXTERN ECS_COMPONENT_DECLARE(GamepadAction);
INPUT_EXTERN ECS_COMPONENT_DECLARE(GamepadAxisAction);
INPUT_EXTERN ECS_COMPONENT_DECLARE(InputInterval);

INPUT_EXTERN ECS_TAG_DECLARE(action_up);
INPUT_EXTERN ECS_TAG_DECLARE(action_down);
INPUT_EXTERN ECS_TAG_DECLARE(action_pressed);

INPUT_EXTERN ECS_SYSTEM_DECLARE(handle_input);
