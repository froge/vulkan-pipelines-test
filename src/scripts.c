#include "SDL_keyboard.h"
#include "SDL_scancode.h"
#include "cglm/struct/vec4.h"
#include "common.h"
#include "flecs.h"
#include "SDL2/SDL.h"
#include "components.h"
#include "cglm/call.h"
#include "physics.h"
#include "convars.h"

typedef struct {
    int test;
}Selected;

ECS_COMPONENT_DECLARE(Selected);

void deselect_all(ecs_world_t *world, void *data) {
    u8 *fortnite = SDL_GetKeyboardState(NULL);
    if (!fortnite[SDL_SCANCODE_LSHIFT]) {
        ecs_filter_t *filter = ecs_filter(world, {
            .terms = {
                { ecs_id(RenderOutline) },
            },
        });
        ecs_iter_t iter = ecs_filter_iter(world, filter);
        while (ecs_filter_next(&iter)) {
            for (int i = 0; i < iter.count; i++) {
                ecs_remove(world, iter.entities[i], RenderOutline);
            }
        }
    }
}

void select_object(ecs_world_t *world, ecs_entity_t ent, void *data) {
    // ecs_set(world, ent, RenderOutline, {});
    ecs_add(world, ent, RenderOutline);
}

void move_forward(ecs_world_t *world, void *data) {
    // Player *cam = ecs_singleton_get_mut(world, Player);
    // Players *clients = ecs_singleton_get_mut(world, Players);
    // const ConVar *player_i = ecs_get(world, ecs_lookup(world, "player"), ConVar);
    // ecs_entity_t player = clients->players[player_i->i].entities[0];
    ecs_entity_t player = ecs_id(Player);
    Player *cam = ecs_get_mut(world, player, Player);
    // Position *pos = ecs_get_mut(world, player, Position);
    DeltaTime delta = *ecs_singleton_get(world, DeltaTime);
    const ConVar *speed = ecs_get(world, ecs_lookup(world, "speed"), ConVar);

    cam->desired_dir.z += 1.0;
}

void move_backward(ecs_world_t *world, void *data) {
    // Player *cam = ecs_singleton_get_mut(world, Player);
    // Position *pos = ecs_get_mut(world, ecs_id(Player), Position);
    // Players *clients = ecs_singleton_get_mut(world, Players);
    // const ConVar *player_i = ecs_get(world, ecs_lookup(world, "player"), ConVar);
    ecs_entity_t player = ecs_id(Player);
    // ecs_entity_t player = clients->players[player_i->i].entities[0];
    Player *cam = ecs_get_mut(world, player, Player);
    // Position *pos = ecs_get_mut(world, player, Position);
    DeltaTime delta = *ecs_singleton_get(world, DeltaTime);
    const ConVar *speed = ecs_get(world, ecs_lookup(world, "speed"), ConVar);

    cam->desired_dir.z -= 1.0;
}

void move_left(ecs_world_t *world, void *data) {
    // Player *cam = ecs_singleton_get_mut(world, Player);
    // Position *pos = ecs_get_mut(world, ecs_id(Player), Position);
    // Players *clients = ecs_singleton_get_mut(world, Players);
    // const ConVar *player_i = ecs_get(world, ecs_lookup(world, "player"), ConVar);
    ecs_entity_t player = ecs_id(Player);
    // ecs_entity_t player = clients->players[player_i->i].entities[0];
    Player *cam = ecs_get_mut(world, player, Player);
    // Position *pos = ecs_get_mut(world, player, Position);
    DeltaTime delta = *ecs_singleton_get(world, DeltaTime);
    const ConVar *speed = ecs_get(world, ecs_lookup(world, "speed"), ConVar);

    cam->desired_dir.x -= 1.0;
}

void move_right(ecs_world_t *world, void *data) {
    // Player *cam = ecs_singleton_get_mut(world, Player);
    // Position *pos = ecs_get_mut(world, ecs_id(Player), Position);
    // Players *clients = ecs_singleton_get_mut(world, Players);
    // const ConVar *player_i = ecs_get(world, ecs_lookup(world, "player"), ConVar);
    ecs_entity_t player = ecs_id(Player);
    // ecs_entity_t player = clients->players[player_i->i].entities[0];
    Player *cam = ecs_get_mut(world, player, Player);
    // Position *pos = ecs_get_mut(world, player, Position);
    DeltaTime delta = *ecs_singleton_get(world, DeltaTime);
    const ConVar *speed = ecs_get(world, ecs_lookup(world, "speed"), ConVar);

    cam->desired_dir.x += 1.0;
}

void move_up(ecs_world_t *world, void *data) {
    // Player *cam = ecs_singleton_get_mut(world, Player);
    // Position *pos = ecs_get_mut(world, ecs_id(Player), Position);
    // Players *clients = ecs_singleton_get_mut(world, Players);
    // const ConVar *player_i = ecs_get(world, ecs_lookup(world, "player"), ConVar);
    ecs_entity_t player = ecs_id(Player);
    // ecs_entity_t player = clients->players[player_i->i].entities[0];
    Player *cam = ecs_get_mut(world, player, Player);
    // Position *pos = ecs_get_mut(world, player, Position);
    DeltaTime delta = *ecs_singleton_get(world, DeltaTime);
    const ConVar *speed = ecs_get(world, ecs_lookup(world, "speed"), ConVar);

    cam->desired_dir.y += 1.0;
}

void move_down(ecs_world_t *world, void *data) {
    // Player *cam = ecs_singleton_get_mut(world, Player);
    // Position *pos = ecs_get_mut(world, ecs_id(Player), Position);
    // Players *clients = ecs_singleton_get_mut(world, Players);
    // const ConVar *player_i = ecs_get(world, ecs_lookup(world, "player"), ConVar);
    ecs_entity_t player = ecs_id(Player);
    // ecs_entity_t player = clients->players[player_i->i].entities[0];
    Player *cam = ecs_get_mut(world, player, Player);
    // Position *pos = ecs_get_mut(world, player, Position);
    DeltaTime delta = *ecs_singleton_get(world, DeltaTime);
    const ConVar *speed = ecs_get(world, ecs_lookup(world, "speed"), ConVar);

    cam->desired_dir.y -= 1.0;
}
