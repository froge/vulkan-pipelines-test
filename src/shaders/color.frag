#version 460

layout (location = 0) out vec4 color;

layout (push_constant) uniform pc {
    layout (offset = 64) vec4 col;
};

void main () {
    vec3 tex_col;
    // if (source != 0) {
    // } else {
        // color.rgb = albedo;
    // }
    color = col;
    // color.a = 1.0;
}
