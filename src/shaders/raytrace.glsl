#version 460

void main () {}

bool tri_hit(vec3 orig, vec3 dir, in vec3 tri[3], out float t) {
    vec3 v0v1 = tri[1] - tri[0];
    vec3 v0v2 = tri[2] - tri[0];

    vec3 pvec = cross(dir, v0v2);
    float det = dot(v0v1, pvec);

    float kEpsilon = 0.001;
    if (det < kEpsilon) return false;

    float inv_det = 1.0 / det;

    vec3 tvec = orig - tri[0];
    float u = dot(tvec, pvec) * inv_det;
    if (u < 0.0 || u > 1.0) return false;

    vec3 qvec = cross(tvec, v0v1);
    float v = dot(dir, qvec) * inv_det;
    if (v < 0.0 || u + v > 1.0) return false;

    t = dot(v0v2, qvec) * inv_det;

    return true;
}

bool object_hit(vec3 orig, vec3 dir, uint object, in float tmax, out float t) {
    MeshInfo mesh = meshes[objects[object].mesh];

    // debugPrintfEXT("mesh has %i verts\n", mesh.verts);
    for (int i = 0; i < mesh.verts;) {
        vec3 tri[3];
        for (int v = 0; v < 3; v++) {
            tri[v] = vec3(objects[object].model * vec4(mesh.vertices.vertices[mesh.indices.indices[i]].pos, 1.0));
            i++;
        }
        if (tri_hit(orig, dir, tri, t)) {
            // if (t < tmax)
                return true;
            // debugPrintfEXT("mesh %i\n", i);
        }
    }
    return false;
}

bool in_shadow(vec3 frag_pos, vec3 dir) {
    float t;
    // debugPrintfEXT("frag has %i objects\n", object_count);
    // vec3 dir = normalize(light_pos - frag_pos);
    // float tmax = length(frag_pos - light_pos);
    float tmax = 4.0;
    for (uint i = 0; i < object_count; i++) {
        // debugPrintfEXT("mesh %i\n", i);
        if (object_hit(frag_pos, dir, i, tmax, t)) {
                return true;
        }
    }
    return false;
}
