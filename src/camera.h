#pragma once

#include "flecs.h"

typedef struct Camera {
	vec3 pos;
	vec3 front;
	vec3 up;
	float pitch;
	float yaw;
}Camera;

void cam_calc_front(struct Camera *cam);
void init_camera(struct Camera *cam);
void move_z(struct Camera *cam, vec3 *target, float amount);
void move_x(struct Camera *cam, vec3 *target, float amount);
void move_y(struct Camera *cam, vec3 *target, float amount);
void cam_rot_pitch(struct Camera *cam, float degrees);
void cam_rot_yaw(struct Camera *cam, float degrees);
void cam_view(ecs_world_t *world, mat4 *view);

RENDERER_EXTERN ECS_COMPONENT_DECLARE(Camera);
