#version 460

#extension GL_EXT_nonuniform_qualifier : require

layout (location = 0) out vec4 color;

layout (location = 0) in vec2 tex_coord;

layout (set = 3, binding = 0) uniform sampler2D framebuffer[];

void main () {
    vec3 pixel = texture(framebuffer[5], tex_coord).rgb;

    color.rgb = pixel;
    color.a = 1.0;
}
