#version 460

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 tex_coord;

layout (location = 0) out vec2 tex_coord_out;
layout (location = 1) out vec3 normal_out;

layout (set = 0, binding = 1) uniform ubo0 {
    mat4 proj;
    mat4 view;
};

layout (push_constant) uniform pc {
    mat4 model;
};

void main () {
    gl_Position = proj * view * model * vec4(pos, 1.0);
    gl_Position.xyz += normalize(normal) * 0.01;
    tex_coord_out = tex_coord;
    normal_out = normal;
}
