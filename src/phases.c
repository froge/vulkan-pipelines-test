#define PHASES_H_DECLARE
#include "phases.h"

void PhasesImport(ecs_world_t *world) {
  ECS_MODULE(world, Phases);

  PInput = ecs_new_w_id(world, EcsPhase);
  ecs_add_pair(world, PInput, EcsDependsOn, EcsOnUpdate);
  PUser = ecs_new_w_id(world, EcsPhase);
  ecs_add_pair(world, PUser, EcsDependsOn, PInput);
  PPhysics = ecs_new_w_id(world, EcsPhase);
  ecs_add_pair(world, PPhysics, EcsDependsOn, PUser);
  PPostPhysics = ecs_new_w_id(world, EcsPhase);
  ecs_add_pair(world, PPostPhysics, EcsDependsOn, PPhysics);
  POutputs = ecs_new_w_id(world, EcsPhase);
  ecs_add_pair(world, POutputs, EcsDependsOn, PPostPhysics);
  PPresent = ecs_new_w_id(world, EcsPhase);
  ecs_add_pair(world, PPresent, EcsDependsOn, POutputs);
}
