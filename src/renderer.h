#pragma once

#include "common.h"
#include "flecs.h"
#include "cglm/call.h"
#include "physics.h"

#ifdef RENDERER_H_DECLARE
#define RENDERER_EXTERN
#else
#define RENDERER_EXTERN extern
#endif

#include "camera.h"
typedef float DeltaTime;

enum ColorSource {
    csource_color,
    csource_texture,
    csource_shader,
};

typedef struct Material {
    enum ColorSource color_source;
    u32 index;
    u8 lit;
    Vector3 albedo;
    ecs_entity_t texture;
    ecs_entity_t metallic_roughness_texture;
    float metallic;
    float roughness;
}Material;

typedef struct {
    Vector3 min;
    Vector3 max;
}AABB;

typedef struct Render3D {
    ecs_entity_t mat;
    ecs_entity_t mesh;
    int visible;
}Render3D;

typedef enum {
    light_type_sun,
    light_type_point,
    light_type_spot,
}LightType;

typedef struct {
    LightType type;
    vec3 dir;
    vec3 color;
    float strength;
    u32 shadowed;
}Light;

void RendererImport(ecs_world_t *world);

RENDERER_EXTERN ECS_COMPONENT_DECLARE(Material);
RENDERER_EXTERN ECS_COMPONENT_DECLARE(DeltaTime);
RENDERER_EXTERN ECS_COMPONENT_DECLARE(Render3D);
RENDERER_EXTERN ECS_COMPONENT_DECLARE(AABB);
RENDERER_EXTERN ECS_COMPONENT_DECLARE(Light);

RENDERER_EXTERN ECS_TAG_DECLARE(RenderOutline);
