#include "cglm/struct/quat.h"
#include "physics.h"
#include "rapier.h"
#include "renderer.h"
#include "networking.h"
#include "phases.h"
#include "common.h"

typedef struct {
  Vector3 desired_dir;
  ecs_entity_t camera;
}Player;

ECS_COMPONENT_DECLARE(Player);

void move_player(ecs_iter_t *it) {
  Player *players = ecs_field(it, Player, 1);
  Transform *positions = ecs_field(it, Transform, 2);
  ecs_world_t *world = it->world;
  // Players *players = ecs_singleton_get_mut(world, Players);
  float speed = 5.0;
  for (int i = 0; i < it->count; i++) {
    // if (i == players->me) {
    //   continue;
    // }
    Player *player = &players[i];
    Transform *position = &positions[i];
    // struct Player *p = &players->players[i];
    // Player *player = ecs_get_mut(world, p->entities[0], Player);
    // Position *position = ecs_get_mut(world, p->entities[0], Position);
    // if (i == 0) {
      // printf("%f %f %f\n", player->desired_dir.x, player->desired_dir.y, player->desired_dir.z);
    // }
    // printf("%f %f %f\n", position->x, position->y, position->z);
    Camera *cam = ecs_get_mut(it->world, player->camera, Camera);
    // Camera *cam = ecs_get_mut(it->world, p->entities[1], Camera);
    // Camera *cam = ecs_singleton_get_mut(it->world, Camera);
    Vector3 speeded = glms_vec3_scale((player->desired_dir), it->delta_time * speed);
    // RVector3 speeded = { player->desired_dir.x, -player->desired_dir.y, -player->desired_dir.z };
    move_z(cam, &position->pos.raw, speeded.z);
    move_x(cam, &position->pos.raw, speeded.x);
    move_y(cam, &position->pos.raw, speeded.y);
    // glm_vec3_rotate(&speeded, glm_rad(-cam->yaw), (vec3){ 0, 1, 0});
    // PhysicsSystem *system = ecs_singleton_get_mut(it->world, PhysicsSystem);
    // PhysicsBody *_player = ecs_get_mut(it->world, ecs_id(Player), PhysicsBody);
    // RigidBody *body = rapier_RigidBodySet_index(system->rset, _player->handle);

    // glm_vec3_scale(&speeded, 3, &speeded);

    // rapier_RigidBody_set_angvel(body, &speeded);
    // rapier_RigidBody_set_angvel(body, &speeded);
    // RVector3 test;
    // test.x = player->desired_dir.x;
    // test.y = player->desired_dir.y;
    // test.z = player->desired_dir.z;
    // rapier_RigidBody_set_linvel(body, &test);
    // position->modified = true;
  }
}

void reset_main_player_pos(ecs_iter_t *it) {
  // Host *host = ecs_singleton_get_mut(it->world, Host);
  Player *player = ecs_singleton_get_mut(it->world, Player);
  player->desired_dir = glms_vec3_zero();
  // PhysicsSystem *system = ecs_singleton_get_mut(it->world, PhysicsSystem);
  // PhysicsBody *_player = ecs_get_mut(it->world, ecs_id(Player), PhysicsBody);
  // RigidBody *body = rapier_RigidBodySet_index(system->rset, _player->handle);
  // RVector3 vel = { 0, 0, 0 };
  // rapier_RigidBody_translation(body, &vel);
}

ecs_entity_t player_init(ecs_world_t *world, ecs_entity_t player, int singleton) {

  struct Camera cam;
  init_camera(&cam);
  ecs_entity_t cam_e;
  if (singleton) {
    cam_e = ecs_singleton_set_ptr(world, Camera, &cam);
  } else {
    cam_e = ecs_set_ptr(world, 0, Camera, &cam);
  }
  Transform ball = default_transform();
  ball.pos.z = 0;
  Transform fart = default_transform();
  
  ecs_set_ptr(world, cam_e, Transform, &ball);
  ecs_add(world, cam_e, WorldTransform);

  ecs_set(world, player, Player, { {{ 0, 0, 0 }}, cam_e });
  // ecs_add(world, player, PlayerTag);
  ecs_add_pair(world, cam_e, EcsChildOf, player);
  ecs_set_ptr(world, player, Transform, &fart);

  // PhysicsSystem *system = ecs_singleton_get_mut(world, PhysicsSystem);
  // Collider *collider = rapier_Collider_create_cuboid((RVector3) { 0.5, 0.5, 0.5 }, 1);
  // RigidBodyOptions options = {
  //     .type = RigidBodyTypeDynamic,
  //     .pos = { 0, 10, 0 },
  //     .rotation = {1, 0, 0, 0},
  //     .linvel = { 0, 0, 0 },
  //     .gravity_scale = 0.0,
  //     .user_data = player,
  // };

  // RigidBody *body = rapier_RigidBody_create(&options);
  // RigidBodyHandle handle = rapier_RigidBodySet_insert(system->rset, body);
  // rapier_ColliderSet_insert_with_parent(system->cset, collider, handle,
  //                                       system->rset);
  ecs_entity_t cube_mesh = ecs_lookup(world, "cube_mesh");

  ecs_entity_t mat;
  mat = ecs_lookup(world, "red");
  // ecs_set(world, player, PhysicsBody, { handle });
  // ecs_add(world, player, Matrix4);
  // ecs_set(world, player, Render3D, {mat, cube_mesh, 1});
  // ecs_set(world, player, AABB, { { { -0.5, -0.5, -0.5 } }, { { 0.5, 0.5, 0.5 } } });
  // ecs_set(world, player, PlayerTag, { 0 });
  // ecs_set(world, cam_e,  PlayerTag,  { 1 });
  // ecs_add_pair(world, cam_e, Networked, ecs_id(Camera));
  // ecs_add_pair(world, player, Networked, ecs_id(Position));
  // ecs_add_pair(world, player, Networked, ecs_id(Player));
  return cam_e;
}

void local_player_init(ecs_world_t *world) {
  ecs_entity_t cam_e = player_init(world, ecs_id(Player), 1);

  // Players *clients = ecs_singleton_get_mut(world, Players);
  // struct Player *me = &clients->players[0];
  // me->max = 10;
  // me->len = 2;
  // me->entities = malloc(sizeof(ecs_entity_t) * 10);
  // me->generation = 0;
  // me->alive = 1;

  // me->entities[0] = ecs_id(Player);
  // me->entities[1] = cam_e;

  ECS_SYSTEM(world, move_player, PUser, Player, Transform);
  ECS_SYSTEM(world, reset_main_player_pos, EcsOnUpdate);
}
