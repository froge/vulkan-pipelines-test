#include "SDL_timer.h"
#include "common.h"

#include "cglm/call.h"
#include "rapier.h"

#define PHYSICS_H_DECLARE
#include "physics.h"
#include "input.h"

#include "phases.h"
#include "convars.h"
#include "sound.h"
#include "window.h"
#include "vulkan/vulkan.h"
#include "vk_mem_alloc.h"
#include "renderer.h"
#include "vulkan.h"
#include "networking.h"
#include "actions.h"

Transform default_transform() {
    return (Transform){
        { { 0, 0, 0 } },
        { { 1, 1, 1 } },
        { { 1, 0, 0, 0 } },
        true
    };
};
void handle_physics(ecs_iter_t *it) {
    PhysicsBody *bodies = ecs_field(it, PhysicsBody, 1);
    Transform *poss = ecs_field(it, Transform, 2);
    Velocity *vels = ecs_field(it, Velocity, 3);
    PhysicsSystem *system = ecs_singleton_get_mut(it->world, PhysicsSystem);
    rapier_PhysicsPipeline_step(system->pipeline, system->state, system->rset, system->cset);
    for (int i = 0; i < it->count; i++) {
        PhysicsBody *bodyh = &bodies[i];
        Transform *transform = &poss[i];
        RigidBody *body = rapier_RigidBodySet_index(system->rset, bodyh->handle);
        if (rapier_RigidBody_is_sleeping(body)) {
            continue;
        }
        if (vels) {
            Velocity *vel = &vels[i];
            rapier_RigidBody_linvel(body, (RVector3*)vel->raw);
        }
        rapier_RigidBody_translation(body, (RVector3*)transform->pos.raw);
        if (transform->pos.y < -100) {
            ecs_delete(it->world, it->entities[i]);
        }
        RVector4 rott;
        rapier_RigidBody_rotation(body, &rott);
        transform->rotation.x = rott.x;
        transform->rotation.y = rott.y;
        transform->rotation.z = rott.z;
        transform->rotation.w = rott.w;
        transform->modified = true;
    }

    system->last_update = SDL_GetPerformanceCounter();
}

void interpolate_physics(ecs_iter_t *it) {
    PhysicsBody *bodies = ecs_field(it, PhysicsBody, 1);
    Transform *poss = ecs_field(it, Transform, 2);
    PhysicsSystem *system = ecs_singleton_get_mut(it->world, PhysicsSystem);
    float delta = (float)(SDL_GetPerformanceCounter() - system->last_update) / SDL_GetPerformanceFrequency();
    // printf("%f\n", delta);
    for (int i = 0; i < it->count; i++) {
        PhysicsBody *bodyh = &bodies[i];
        Transform *transform = &poss[i];
        RigidBody *body = rapier_RigidBodySet_index(system->rset, bodyh->handle);
        if (rapier_RigidBody_is_sleeping(body)) {
            continue;
        }
        rapier_RigidBody_predict_position(body, delta, (RVector3*)transform->pos.raw, (RVector4*)transform->rotation.raw);
        transform->modified = true;
        // pos->y -= 9.81 * it->delta_time;
        // *pos = glms_vec3_add(glms_vec3_scale(*vel, it->delta_time), *pos);
    }
}

Ray ray_from_mouse_pos(ecs_world_t *world, vec2 mouse_pos) {
    const Window *window = ecs_singleton_get(world, Window);
    const Framebuffer *framebuffer = ecs_get(world, window->framebuffer, Framebuffer);
    const Camera *cam = ecs_singleton_get(world, Camera);
    Transform *pos = ecs_get_mut(world, ecs_id(Camera), WorldTransform);

    
    mat4s proj;
    mat4s view;
    proj = glms_perspective(glm_rad(90.0), (float)framebuffer->x / (float)framebuffer->y, 0.01, 100.0);
    // glm_lookat((vec3){-0.1, -0.1, 1}, (vec3){0, 0, 0}, (vec3){0, 1, 0}, uniform.view);
    cam_view(world, &view.raw);

    vec2 clipcoord = { 2.0f * mouse_pos[0] / (f32)framebuffer->x - 1.0f, 1.0f - 2.0f * mouse_pos[1] / (f32)framebuffer->y };
    mat4s inv_p;
    mat4s inv_v;
    inv_p = glms_mat4_inv(proj);
    vec3s transform;
    transform = glms_mat4_mulv3(inv_p, (vec3s){ { clipcoord[0], clipcoord[1], 1.0 } }, 1.0);
    inv_v = glms_mat4_inv(view);
    Ray chud;
    chud.direction = glms_mat4_mulv3(inv_v, transform, 0.0);
    glm_vec3_copy(pos->pos.raw, chud.origin.raw);
    // glm_vec3_mul(transform, (float*)cam->front, chud.direction);
    chud.direction = glms_vec3_normalize(chud.direction);
    return chud;
}

int intersect_mesh(Ray ray, CollisionMesh *mesh, mat4s model, float *_dist) {
    float best_distance = INFINITY;
    float dist;
    int col = 0;
    for (int i = 0; i < mesh->verts; i += 3) {
        vec3s tri[3];
        tri[0] = glms_mat4_mulv3(model, mesh->data[i    ], 1.0);
        tri[1] = glms_mat4_mulv3(model, mesh->data[i + 1], 1.0);
        tri[2] = glms_mat4_mulv3(model, mesh->data[i + 2], 1.0);
        if (glm_ray_triangle(ray.origin.raw, ray.direction.raw, tri[0].raw, tri[1].raw, tri[2].raw, &dist)) {
            if (dist < best_distance) {
                best_distance = dist;
            }
            col = 1;
        }
    }
    *_dist = best_distance;
    return col;
}

Vector3 get_world_pos(ecs_world_t *world, ecs_entity_t ent) {
    ecs_entity_t parent;
    Vector3 world_pos;
}

void contact_force_handler(void *user_data, RigidBodyHandle col1, RigidBodyHandle col2) {
    ecs_world_t *world = user_data;
    PhysicsSystem *system = ecs_singleton_get_mut(world, PhysicsSystem);
    RigidBody *body = rapier_RigidBodySet_index(system->rset, col2);
    ecs_entity_t col1e = rapier_RigidBody_user_data(body);
    Gamepad *gamepad = ecs_singleton_get(world, Gamepad);
    // SDL_GameControllerRumble(gamepad->controller, 0xffff, 10, 100);
    // printf("chud connect\n");
    ecs_entity_t soundr = ecs_lookup(world, "sounds/test.mp3");
    play_sound(world, col1e, soundr);
}

void remove_physics(ecs_iter_t *it) {
    PhysicsBody *bodies = ecs_field(it, PhysicsBody, 1);    
    PhysicsSystem *system = ecs_singleton_get_mut(it->world, PhysicsSystem);
    for (int i = 0; i < it->count; i++) {
        PhysicsBody *body = &bodies[i];
        rapier_RigidBodySet_remove(system->rset, system->cset, system->state, body->handle);
        // printf("removed 1 physics body\n");
    }
}

void physics_set_timescale(ecs_world_t *world, f32 scale) {
    PhysicsSystem *system = ecs_singleton_get_mut(world, PhysicsSystem);
    rapier_PhysicsState_set_delta(system->state, 1.0 / 64.0 * scale);
}

void PhysicsImport(ecs_world_t *world) {
    ECS_MODULE(world, Physics);

    ECS_COMPONENT_DEFINE(world, Transform);
    // ECS_COMPONENT_DEFINE(world, Scale);
    // ECS_COMPONENT_DEFINE(world, Rotation);
    ECS_COMPONENT_DEFINE(world, Matrix4);

    ECS_COMPONENT_DEFINE(world, WorldTransform);
    // ECS_COMPONENT_DEFINE(world, WorldScale);
    // ECS_COMPONENT_DEFINE(world, WorldRotation);

    ECS_COMPONENT_DEFINE(world, Velocity);
    ECS_COMPONENT_DEFINE(world, CollisionMesh);
    ECS_COMPONENT_DEFINE(world, Selectable);

    ECS_COMPONENT_DEFINE(world, PhysicsSystem);
    ECS_COMPONENT_DEFINE(world, PhysicsBody);

    ECS_TAG_DEFINE(world, Static);
    ECS_TAG_DEFINE(world, Dynamic);
    ECS_TAG_DEFINE(world, Kinematic);

    // ECS_SYSTEM(world, handle_physics, PPhysics, [inout] Velocity, [inout] Position);

    ECS_SYSTEM(world, interpolate_physics, PPhysics, PhysicsBody, Transform);
    ECS_OBSERVER(world, remove_physics, EcsOnRemove, PhysicsBody);

    ecs_system(world, {
        .entity = ecs_entity(world, {
            .name = "handle_physics",
            .add = { ecs_dependson(PPhysics) }
        }),
        .query.filter.terms = {
            { .id = ecs_id(PhysicsBody), .inout = EcsInOut },
            { .id = ecs_id(Transform),    .inout = EcsInOut },
            // { .id = ecs_id(Rotation),    .inout = EcsInOut },
            { .id = ecs_id(Velocity),    .oper = EcsOptional, .inout = EcsInOut },
        },
        .callback = handle_physics,
        .interval = 1.0 / 64.0,
    });

    RigidBodySet *rset = rapier_RigidBodySet_new();
    ColliderSet *cset = rapier_ColliderSet_new();
    PhysicsState *state = rapier_PhysicsState_new();
    PhysicsPipeline *pipeline = rapier_PhysicsPipeline_new();

    rapier_PhysicsState_set_user_data(state, world);
    // rapier_PhysicsState_set_contact_force_handler(state, contact_force_handler);
    ecs_singleton_set(world, PhysicsSystem, { state, pipeline, rset, cset, 0 });
}
