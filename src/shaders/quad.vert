#version 460

#extension GL_EXT_scalar_block_layout : require

layout (location = 0) out vec2 tex_coord_out;

layout (push_constant, scalar) uniform pc {
    mat4 view;
    mat4 model;
};

vec2 fart[] = {
    vec2( 1.0,  1.0),
    vec2(-1.0,  1.0),
    vec2(-1.0, -1.0),
    vec2( 1.0,  1.0),
    vec2(-1.0, -1.0),
    vec2( 1.0, -1.0),
};

vec2 tex[] = {
    vec2(1.0, 0.0),
    vec2(0.0, 0.0),
    vec2(0.0, 1.0),
    vec2(1.0, 0.0),
    vec2(0.0, 1.0),
    vec2(1.0, 1.0),
};

void main() {
    gl_Position = model * vec4(fart[gl_VertexIndex], 0.0, 1.0);
    tex_coord_out = tex[gl_VertexIndex];
}
