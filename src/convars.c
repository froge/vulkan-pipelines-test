#define CONVARS_H_DECLARE
#include "convars.h"

#include <stdio.h>

ecs_entity_t add_convar(ecs_world_t *world, char *name, ConVar *convar) {
    return ecs_set_name(world, ecs_set_ptr(world, 0, ConVar, convar), name);
}

void toggle_convar(ecs_world_t *world, void *data) {
    ecs_entity_t mousee = ecs_lookup(world, (char*)data);
    ConVar *mouse = ecs_get_mut(world, mousee, ConVar);
    mouse->i = !mouse->i;
    if (mouse->set_callback) {
        mouse->set_callback(world, mouse);
    }
    // printf("convar %s toggled to %i\n", (char*)data, mouse->i);
    ecs_modified(world, mousee, ConVar);
}

void ConvarsImport(ecs_world_t *world) {
  ECS_MODULE(world, Convars);

  ECS_COMPONENT_DEFINE(world, ConVar);
}
