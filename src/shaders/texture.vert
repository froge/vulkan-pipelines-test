#version 460

layout (location = 0) in vec2 pos;
layout (location = 1) in vec2 tex_coord;

layout (location = 0) out vec2 tex_coord_out;

// layout (push_constant) uniform pc {
//     vec2 offset;
// };

void main () {
    gl_Position = vec4(pos, 0.0, 1.0);
    tex_coord_out = tex_coord;
}
