
#version 460

#extension GL_EXT_nonuniform_qualifier : require

layout (location = 0) out vec4 color;

layout (location = 0) in vec2 tex_coord;

layout (set = 3, binding = 0) uniform sampler2D framebuffer[];

float fxaa_luma(vec3 rgb) {
    return rgb.g * (0.587/0.299) + rgb.r;
}

vec3 pixel_offset(float offset_x, float offset_y) {
    ivec2 pixel_pos = ivec2(gl_FragCoord.xy);
    return pow(texelFetch(framebuffer[4], pixel_pos - ivec2(offset_x, offset_y), 0).rgb, vec3(1.0/2.2));
}

vec3 pixel_offsetf(vec2 offset) {
    vec2 pixel_pos = gl_FragCoord.xy;
    return pow(texture(framebuffer[4], (pixel_pos + offset) / textureSize(framebuffer[4], 0)).rgb, vec3(1.0/2.2));
}

void main () {
    color.a = 1.0;

    vec3 rgbN = pixel_offset( 0, -1);
    vec3 rgbW = pixel_offset(-1,  0);
    vec3 rgbM = pixel_offset( 0,  0);
    vec3 rgbE = pixel_offset( 1,  0);
    vec3 rgbS = pixel_offset( 0,  1);

    float lumaN = fxaa_luma(rgbN);
    float lumaW = fxaa_luma(rgbW);
    float lumaM = fxaa_luma(rgbM);
    float lumaE = fxaa_luma(rgbE);
    float lumaS = fxaa_luma(rgbS);

    float range_min = min(lumaM, min(min(lumaN, lumaW), min(lumaS, lumaE)));
    float range_max = max(lumaM, max(max(lumaN, lumaW), max(lumaS, lumaE)));

    // fxaa edge detection
    float FXAA_EDGE_THRESHOLD_MIN = 1.0/8.0;
    float FXAA_EDGE_THRESHOLD = 1.0/16.0;
    float range = range_max - range_min;
    if (range < max(FXAA_EDGE_THRESHOLD_MIN, range_max * FXAA_EDGE_THRESHOLD)) {
        color.rgb = pow(rgbM, vec3(2.2));
        // color.rgb = vec3(0.0);
        return;
    }

    // subpixel aliasing test
    float blend_factor = 1.0;
    
    vec3 rgbNW = pixel_offset(-1, -1);
    vec3 rgbNE = pixel_offset( 1, -1);
    vec3 rgbSW = pixel_offset(-1,  1);
    vec3 rgbSE = pixel_offset( 1,  1);

    float lumaNW = fxaa_luma(rgbNW);
    float lumaNE = fxaa_luma(rgbNE);
    float lumaSW = fxaa_luma(rgbSW);
    float lumaSE = fxaa_luma(rgbSE);

    float blendL = 2.0 * (lumaN + lumaE + lumaS + lumaW);
    blendL += lumaNE + lumaNW + lumaSE + lumaSW;
    blendL *= 1.0 / 12.0;
    blendL = abs(blendL - lumaM);
    blendL = clamp(0.0, 1.0, blendL / range);
    blendL = smoothstep(0.0, 1.0, blendL);
    blendL *= blendL * blend_factor;
    // float rangeL = abs(lumaL - lumaM);
    // float blendL = max(0.0, (rangeL / range) - FXAA_SUBPIX_TRIM) * FXAA_SUBPIX_TRIM_SCALE;
    // blendL = min(FXAA_SUBPIX_CAP, blendL);

    // vert/horiz edge test

    float edgeVert =
        abs((0.25 * lumaNW) + (-0.5 * lumaN) + (0.25 * lumaNE)) +
        abs((0.50 * lumaW ) + (-1.0 * lumaM) + (0.50 * lumaE )) +
        abs((0.25 * lumaSW) + (-0.5 * lumaS) + (0.25 * lumaSE));
    float edgeHoriz =
        abs((0.25 * lumaNW) + (-0.5 * lumaW) + (0.25 * lumaSW)) +
        abs((0.50 * lumaN ) + (-1.0 * lumaM) + (0.50 * lumaS )) +
        abs((0.25 * lumaNE) + (-0.5 * lumaE) + (0.25 * lumaSE));
    bool horizSpan = edgeHoriz >= edgeVert;

    // end of edge search
    uint FXAA_SEARCH_STEPS = 10;
    bool doneN = false;
    bool doneP = false;
    float lumaNeg = horizSpan ? lumaS : lumaW;
    float lumaPos = horizSpan ? lumaN : lumaE;
    float gradientNeg = abs(lumaNeg - lumaM);
    float gradientPos = abs(lumaPos - lumaM);
    ivec2 offNP = horizSpan ? ivec2(1, 0) : ivec2(0, 1);

    vec2 init_off = horizSpan ? vec2(0.0, 0.5) : vec2(0.5, 0.0);
    float lumaEndN = 0.0;
    float lumaEndP = 0.0;

    float otherLuma;
    float lumaGradient;

    vec2 off;
    if (gradientPos < gradientNeg) {
        off = -vec2(offNP.y, offNP.x);
        lumaGradient = gradientNeg;
        otherLuma = lumaNeg;
        init_off = -init_off;
    } else {
        off = vec2(offNP.y, offNP.x);
        lumaGradient = gradientPos;
        otherLuma = lumaPos;
    }
    vec2 posN = init_off;
    vec2 posP = init_off;

    float edgeLuma = 0.5 * (lumaM + otherLuma);
    float gradientThreshold = 0.25 * lumaGradient;
    
    for (uint i = 0; i < FXAA_SEARCH_STEPS; i++) {
        if (!doneN) {
            lumaEndN = fxaa_luma(pixel_offsetf(posN));
        }
        if (!doneP) {
            lumaEndP = fxaa_luma(pixel_offsetf(posP));
        }
        if (abs(lumaEndN - lumaM) >= gradientThreshold) {
            doneN = true;
        }
        if (abs(lumaEndP - lumaM) >= gradientThreshold) {
            doneP = true;
        }
        if (doneN && doneP) {
            break;
        }
        if (!doneN) {
            posN -= offNP;
        }
        if (!doneP) {
            posP += offNP;
        }
    }

    float distP = max(posP.x, posP.y);
    float distN = abs(min(posN.x, posN.y));

    float shortest_dist = min(distP, distN); 
    float factor = shortest_dist / (distP + distN);
    if (factor == 0.5) {
        color.rgb = pow(rgbM, vec3(2.2));
        return;
    }

    // color.rgb = pow(pixel_offsetf(off * blendL), vec3(2.2));
    // color.rgb = vec3(distP, distN, shortest_dist);
    // color.rgb = vec3(max(blendL, factor.x));
    color.rgb = pow(pixel_offsetf(off * max(factor, blendL)), vec3(2.2));
    // color.rg = off;
    // color.b = factor.x;
    // color.r = factor.x;
    // color.rgb = vec3();
    // color.rgb = horizSpan ? vec3(0.0, 1.0, 0.0) : vec3(1.0, 0.0, 0.0);
    // color.gb = textureSize(framebuffer[4], 0);

    // color.rgb = pow(mix(rgbL, rgbM, blendL), vec3(2.2));
    // color.rg = offNP;
    // color.b = 1.0;
}
